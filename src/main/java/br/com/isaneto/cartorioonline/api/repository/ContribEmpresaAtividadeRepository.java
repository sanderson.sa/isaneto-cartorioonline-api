package br.com.isaneto.cartorioonline.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.ContribEmpresaAtividade;
import br.com.isaneto.cartorioonline.api.modelo.ContribuinteEmpresa;

@Repository
public interface ContribEmpresaAtividadeRepository extends JpaRepository<ContribEmpresaAtividade, Long> {
	List<ContribEmpresaAtividade> findByContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa);
}
