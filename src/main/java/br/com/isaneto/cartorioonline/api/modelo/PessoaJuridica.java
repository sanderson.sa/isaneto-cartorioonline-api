/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import br.com.isaneto.cartorioonline.api.modelo.dto.PessoaJuridicaDTO;
import br.com.isaneto.cartorioonline.api.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * 
 * @author Bruno
 */
@Entity
@DiscriminatorValue(value="J")
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaJuridica extends Pessoa implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7813367282412842138L;
    
        
    @Size(max = 100) 
    @Column(name = "tx_pessoa_jur_fantasia", length = 100)
    private String txPessoaJurFantasia;
    @Column(name = "vl_pessoa_jur_capital", precision = 30, scale = 2)
    private BigDecimal vlPessoaJurCapital;
     
	@Size(max = 15)
    @Column(name = "tx_pessoa_jur_nire", length = 15)
    private String txPessoaJurNire;
    @Size(max = 15)
    @Column(name = "tx_pessoa_jur_insc_estadual", length = 15)
    private String txPessoaJurInscEstadual;
    @Size(max = 600)
    @Column(name = "tx_pessoa_jur_objeto_social", length = 2500)
    private String txPessoaJurObjetoSocial;
    @Column(name = "dt_pessoa_jur_constituicao")
    @Temporal(TemporalType.DATE)
    private Date dtPessoaJurConstituicao;
    @ManyToOne
    @JoinColumn(name = "id_nat_juridica")
    private NaturezaJuridica idNatJuridica;
    
    
  /*  @Column(name = "dt_pessoa_ult_atualizacao")
    @Temporal(TemporalType.DATE)
    private Date dtPessoaUltAtualizacao;*/
//    @OneToMany(mappedBy = "pessoaJuridica", cascade={CascadeType.MERGE, CascadeType.PERSIST})
 //   private List<PessoaJurRegimeTributacao> pessoaPjRegimeTributacoes;
  //  @OneToMany(mappedBy = "pessoaJuridica", cascade={CascadeType.MERGE, CascadeType.PERSIST})
   // private List<PessoaJuridicaAtividade> pessoaJurAtividades;
    @Column(name = "bo_pessoa_jur_incluio")
    private Boolean incluiuPeloSistema = false;
    
    @JoinColumn(name = "id_representante_legal", referencedColumnName = "id_pessoa")
    @ManyToOne(fetch=FetchType.EAGER)
    private PessoaFisica representLegal; 
             
	@Transient
	private String cnpj;
	
	@Transient
	private BigInteger nuCpfRepresentateLegal;
	
    public PessoaJuridica() {
    }
    
    public PessoaJuridica(PessoaJuridicaDTO dto) {
        this.txPessoaJurFantasia = dto.getNomeFantasia();
        this.vlPessoaJurCapital = new BigDecimal(dto.getCapitalSocial());
//        this.txPessoaJurNire;
//        this.txPessoaJurInscEstadual;
//        this.txPessoaJurObjetoSocial;
        this.dtPessoaJurConstituicao = Util.strToDateConvert(dto.getDataConstituicao());
        this.cnpj = dto.getCnpj();
        this.setNuPessoaCpfCnpj(new BigInteger(dto.getCnpj()));
		this.setNoPessoa(dto.getRazaoSocial());
		this.setTxPessoaJurFantasia(dto.getNomeFantasia());
//		this.inscricao = pj.getin;
		this.setDtPessoaCad(Util.strToDateConvert(dto.getDataCadastro()));
		this.setVlPessoaJurCapital(new BigDecimal(dto.getCapitalSocial()));
		this.setNuPessoaEndCep(new BigInteger(dto.getCep()));
		this.setTxPessoaEndeLogradouro(dto.getEnderecoLogradouro());
		this.setTxPessoaEndNumero(dto.getEnderecoNumero());
		this.setTxPessoaEndeComplemento(dto.getEnderecoComplemento());
		this.setTxPessoaEndBairro(dto.getEnderecoBairro());
		this.setTxPessoaDdd(dto.getDdd());
		this.setTxPessoaTelefone(dto.getTelefone());
		this.setTxPessoaCelDdd(dto.getDddCelular());
		this.setTxCelularComDdd(dto.getCelular());
		this.setTxPessoaEmail(dto.getCelular());
    }
    
    public PessoaFisica getIdRepresentanteLegal() {
        return representLegal;
    }

    public void setIdRepresentanteLegal(PessoaFisica representLegal) {
        this.representLegal = representLegal;
    }

    public String getTxPessoaJurFantasia() {
        return txPessoaJurFantasia;
    }

    public void setTxPessoaJurFantasia(String txPessoaJurFantasia) {
        this.txPessoaJurFantasia = txPessoaJurFantasia;
    }

    public BigDecimal getVlPessoaJurCapital() {
        return vlPessoaJurCapital;
    }

    public void setVlPessoaJurCapital(BigDecimal vlPessoaJurCapital) {
        this.vlPessoaJurCapital = vlPessoaJurCapital;
    }
    
    public NaturezaJuridica getIdNatJuridica() {
        return idNatJuridica;
    }

    public void setIdNatJuridica(NaturezaJuridica idNatJuridica) {
        this.idNatJuridica = idNatJuridica;
    }

   

    public String getTxPessoaJurNire() {
        return txPessoaJurNire;
    }

    public void setTxPessoaJurNire(String txPessoaJurNire) {
        this.txPessoaJurNire = txPessoaJurNire;
    }

    /*public Date getDtPessoaUltAtualizacao() {
        return dtPessoaUltAtualizacao;
    }

    public void setDtPessoaUltAtualizacao(Date dtPessoaUltAtualizacao) {
        this.dtPessoaUltAtualizacao = dtPessoaUltAtualizacao;
    }
    
    @PreUpdate
    public void preUpdate(){
    	dtPessoaUltAtualizacao = new Date();
    }*/
    
    @PrePersist
    public void prePersist(){
    	setDtPessoaCad(new Date());
    	setTpPessoa("J");
    }

    public String getTxPessoaJurInscEstadual() {
        return txPessoaJurInscEstadual;
    }

    public void setTxPessoaJurInscEstadual(String txPessoaJurInscEstadual) {
        this.txPessoaJurInscEstadual = txPessoaJurInscEstadual;
    }

    public String getTxPessoaJurObjetoSocial() {
        return txPessoaJurObjetoSocial;
    }

    public void setTxPessoaJurObjetoSocial(String txPessoaJurObjetoSocial) {
        this.txPessoaJurObjetoSocial = txPessoaJurObjetoSocial;
    }
   
/*    public void setPessoaPjRegimeTributacoes(
            List<PessoaJurRegimeTributacao> pessoaPjRegimeTributacoes) {
        this.pessoaPjRegimeTributacoes = pessoaPjRegimeTributacoes;
    }

    public List<PessoaJurRegimeTributacao> getPessoaPjRegimeTributacoes() {
        return pessoaPjRegimeTributacoes;
    }
    public void setPessoaJurAtividades(List<PessoaJuridicaAtividade> pessoaJurAtividades) {
        this.pessoaJurAtividades = pessoaJurAtividades;
    }

    public List<PessoaJuridicaAtividade> getPessoaJurAtividades() {
        return pessoaJurAtividades;
    }
*/

    

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setIncluiuPeloSistema(Boolean incluiuPeloSistema) {
		this.incluiuPeloSistema = incluiuPeloSistema;
	}

	public Boolean getIncluiuPeloSistema() {
		return incluiuPeloSistema;
	}


	public void setNuCpfRepresentateLegal(BigInteger nuCpfRepresentateLegal) {
		this.nuCpfRepresentateLegal = nuCpfRepresentateLegal;
	}

	public Date getDtPessoaJurConstituicao() {
		return dtPessoaJurConstituicao;
	}

	public void setDtPessoaJurConstituicao(Date dtPessoaJurConstituicao) {
		this.dtPessoaJurConstituicao = dtPessoaJurConstituicao;
	}

}
