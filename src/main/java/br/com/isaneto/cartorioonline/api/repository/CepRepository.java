package br.com.isaneto.cartorioonline.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Cep;

@Repository
public interface CepRepository extends JpaRepository<Cep, Long> {
	Optional<Cep> findByCep(String cep);
}
