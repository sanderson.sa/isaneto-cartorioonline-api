package br.com.isaneto.cartorioonline.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ComponentScan({"br.com.isaneto"})
@SpringBootApplication
@EnableJpaRepositories ( basePackages = {"br.com.isaneto"})
@EntityScan(value = "br.com.isaneto")
@EnableTransactionManagement
public class CartorioonlineApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartorioonlineApiApplication.class, args);
	}
}