/*
] * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "tipo_estabelecimento", schema = "mobiliario")
public class TipoEstabelecimento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @SequenceGenerator(name = "tipo_estabelecimento_id_tp_estabelecimento_seq", sequenceName = "mobiliario.tipo_estabelecimento_id_tp_estabelecimento_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_estabelecimento_id_tp_estabelecimento_seq")
    @Column(name = "id_tp_estabelecimento", nullable = false)
    private Integer idTpEstabelecimento;
    
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "tx_tp_estabelecimento", nullable = false, length = 40)
    private String txTpEstabelecimento;
    
    @Column(name = "bo_tp_estabelecimento_des_if", nullable = false)
    private Boolean boTpEstabelecimentoDesIf;
    
    @Column(name = "nu_tp_estabelecimento_des_if_codigo", nullable = true)
    private Integer nuTpEstabelecimentoDesIfCodigo;
    
    @Column(name = "bo_tp_estabelecimento_obriga_cnpj", nullable = true)
    private Boolean boTpEstabelecimentoObrigaCnpj;
    
    @OneToMany(mappedBy = "tipoEstabelecimento")
    private List<ContribuinteEmpresa> contribuinteEmpresas;

    public TipoEstabelecimento() {
    }

    public TipoEstabelecimento(Integer idTpEstabelecimento) {
        this.idTpEstabelecimento = idTpEstabelecimento;
    }

    public TipoEstabelecimento(Integer idTpEstabelecimento, String txTpEstabelecimento) {
        this.idTpEstabelecimento = idTpEstabelecimento;
        this.txTpEstabelecimento = txTpEstabelecimento;
    }

    public Integer getIdTpEstabelecimento() {
        return idTpEstabelecimento;
    }

    public void setIdTpEstabelecimento(Integer idTpEstabelecimento) {
        this.idTpEstabelecimento = idTpEstabelecimento;
    }

    public String getTxTpEstabelecimento() {
        return txTpEstabelecimento;
    }

    public void setTxTpEstabelecimento(String txTpEstabelecimento) {
        this.txTpEstabelecimento = txTpEstabelecimento;
    }

    public List<ContribuinteEmpresa> getContribuintes() {
        return contribuinteEmpresas;
    }

    public void setContribuintes(List<ContribuinteEmpresa> contribuinteEmpresas) {
        this.contribuinteEmpresas = contribuinteEmpresas;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTpEstabelecimento != null ? idTpEstabelecimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TipoEstabelecimento)) {
            return false;
        }
        TipoEstabelecimento other = (TipoEstabelecimento) object;
        if ((this.idTpEstabelecimento == null && other.idTpEstabelecimento != null) || (this.idTpEstabelecimento != null && !this.idTpEstabelecimento.equals(other.idTpEstabelecimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
    	return this.idTpEstabelecimento + " - " + this.txTpEstabelecimento;    	
    }

	public Boolean getBoTpEstabelecimentoDesIf() {
		return boTpEstabelecimentoDesIf;
	}

	public void setBoTpEstabelecimentoDesIf(Boolean boTpEstabelecimentoDesIf) {
		this.boTpEstabelecimentoDesIf = boTpEstabelecimentoDesIf;
	}

	public Boolean getBoTpEstabelecimentoObrigaCnpj() {
		return boTpEstabelecimentoObrigaCnpj;
	}

	public void setBoTpEstabelecimentoObrigaCnpj(Boolean boTpEstabelecimentoObrigaCnpj) {
		this.boTpEstabelecimentoObrigaCnpj = boTpEstabelecimentoObrigaCnpj;
	}

	public List<ContribuinteEmpresa> getContribuinteEmpresas() {
		return contribuinteEmpresas;
	}

	public void setContribuinteEmpresas(List<ContribuinteEmpresa> contribuinteEmpresas) {
		this.contribuinteEmpresas = contribuinteEmpresas;
	}

	public Integer getNuTpEstabelecimentoDesIfCodigo() {
		return nuTpEstabelecimentoDesIfCodigo;
	}

	public void setNuTpEstabelecimentoDesIfCodigo(Integer nuTpEstabelecimentoDesIfCodigo) {
		this.nuTpEstabelecimentoDesIfCodigo = nuTpEstabelecimentoDesIfCodigo;
	}
}
