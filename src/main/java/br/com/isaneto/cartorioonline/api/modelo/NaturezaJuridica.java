/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "natureza_juridica", schema = "mobiliario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nu_nat_juridica_cod"})})
public class NaturezaJuridica implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "nat_juridica_id_nat_juridica_seq", sequenceName = "mobiliario.nat_juridica_id_nat_juridica_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nat_juridica_id_nat_juridica_seq")
    @Column(name = "id_nat_juridica", nullable = false)
    private Integer idNatJuridica;
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "tx_nat_juridica_desc", nullable = false, length = 80)
    private String txNatJuridicaDesc;
    @Size(max = 5)
    @Column(name = "nu_nat_juridica_cod", length = 5)
    private String nuNatJuridicaCod;
    @NotNull
    @Column(name = "ch_nat_juridica_pf_pj", nullable = false)
    private char chNatJuridicaPfPj;

    @NotNull
    @Column(name = "bo_nat_juridica_publico", nullable = false)
    private Boolean boNatJuridicaPublico;
    
    @Column(name="dt_nat_jur_ault_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date dataUltimaAtualizacao;
    
    
    @NotNull
    @Column(name = "bo_nat_juridica_ativa", nullable = false)
    private Boolean boNatJuridicaAtiva = true;
  
    public NaturezaJuridica() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public NaturezaJuridica(Integer idNatJuridica) {
        this.idNatJuridica = idNatJuridica;
    }

    public NaturezaJuridica(Integer idNatJuridica, String txNatJuridicaDesc, char chNatJuridicaPfPj) {
        this.idNatJuridica = idNatJuridica;
        this.txNatJuridicaDesc = txNatJuridicaDesc;
        this.chNatJuridicaPfPj = chNatJuridicaPfPj;
    }

    public Integer getIdNatJuridica() {
        return idNatJuridica;
    }

    public void setIdNatJuridica(Integer idNatJuridica) {
        this.idNatJuridica = idNatJuridica;
    }

    public String getTxNatJuridicaDesc() {
        return txNatJuridicaDesc;
    }

    public void setTxNatJuridicaDesc(String txNatJuridicaDesc) {
        this.txNatJuridicaDesc = txNatJuridicaDesc;
    }

    public String getNuNatJuridicaCod() {
        return nuNatJuridicaCod;
    }

    public void setNuNatJuridicaCod(String nuNatJuridicaCod) {
        this.nuNatJuridicaCod = nuNatJuridicaCod;
    }

    public char getChNatJuridicaPfPj() {
        return chNatJuridicaPfPj;
    }

    public void setChNatJuridicaPfPj(char chNatJuridicaPfPj) {
        this.chNatJuridicaPfPj = chNatJuridicaPfPj;
    }

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setBoNatJuridicaPublico(Boolean boNatJuridicaPublico) {
		this.boNatJuridicaPublico = boNatJuridicaPublico;
	}

	public Boolean getBoNatJuridicaPublico() {
		return boNatJuridicaPublico;
	}
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNatJuridica != null ? idNatJuridica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof NaturezaJuridica)) {
            return false;
        }
        NaturezaJuridica other = (NaturezaJuridica) object;
        if ((this.idNatJuridica == null && other.idNatJuridica != null) || (this.idNatJuridica != null && !this.idNatJuridica.equals(other.idNatJuridica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "teste.NaturezaJuridica[ idNatJuridica=" + idNatJuridica + " ]";
    }

	public Boolean getBoNatJuridicaAtiva() {
		return boNatJuridicaAtiva;
	}

	public void setBoNatJuridicaAtiva(Boolean boNatJuridicaAtiva) {
		this.boNatJuridicaAtiva = boNatJuridicaAtiva;
	}
}

