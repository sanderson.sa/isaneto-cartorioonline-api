package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "situacao_contribuinte", schema = "mobiliario")
public class SituacaoContribuinte implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5189856644377435680L;
    @Id
    @Column(name = "id_sit_contribuinte", nullable = false)
    private Integer idSitContribuinte;
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tx_sit_contribuinte", nullable = false, length = 30)
    private String txSitContribuinte;
    @Column(name = "bo_sit_contribuinte_ativo", nullable = false)
    private Boolean ativo;
	@Column(name="dt_sit_contrib_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date dataUltimaAtualizacao;
    
    @OneToMany(mappedBy = "situacaoContribuinte")
    private List<ContribEmpresaSituacao> contribuinteEmpresaSituacoes;
    
    private static String SITUACAO_ATIVA = "ATIVA";
    private static String SITUACAO_INATIVA = "INATIVA";
    private static String SITUACAO_SUSPENSO = "SUSPENSA";
    private static String SITUACAO_BAIXADA = "BAIXADA";
    

    public static Integer getIdSituacaoContribuinte(String situacaoReceita) {
    	if(situacaoReceita.equals(SITUACAO_ATIVA)) {
    		return 1;
    	}else if(situacaoReceita.equals(SITUACAO_INATIVA)) {
    		return 2;
    	}else if(situacaoReceita.equals(SITUACAO_SUSPENSO)) {
    		return 3;
    	}else if(situacaoReceita.equals(SITUACAO_BAIXADA)) {
    		return 4;
    	}
		return null;
    }
    
    public SituacaoContribuinte() {
    	
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public SituacaoContribuinte(Integer idSitContribuinte) {
        this.idSitContribuinte = idSitContribuinte;
    }

    public SituacaoContribuinte(Integer idSitContribuinte, String txSitContribuinte) {
        this.idSitContribuinte = idSitContribuinte;
        this.txSitContribuinte = txSitContribuinte;
    }

    public Integer getIdSitContribuinte() {
        return idSitContribuinte;
    }

    public void setIdSitContribuinte(Integer idSitContribuinte) {
        this.idSitContribuinte = idSitContribuinte;
    }

    public String getTxSitContribuinte() {
        return txSitContribuinte;
    }

    public void setTxSitContribuinte(String txSitContribuinte) {
        this.txSitContribuinte = txSitContribuinte;
    }

    public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSitContribuinte != null ? idSitContribuinte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SituacaoContribuinte)) {
            return false;
        }
        SituacaoContribuinte other = (SituacaoContribuinte) object;
        if ((this.idSitContribuinte == null && other.idSitContribuinte != null) || (this.idSitContribuinte != null && !this.idSitContribuinte.equals(other.idSitContribuinte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.isaneto.prefeitura.modelo.nfe.SituacaoContribuinte[ idSitContribuinte=" + idSitContribuinte + " ]";
    }

}
