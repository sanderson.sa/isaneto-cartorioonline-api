package br.com.isaneto.cartorioonline.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Contribuinte;
import br.com.isaneto.cartorioonline.api.modelo.ContribuinteEmpresa;
import br.com.isaneto.cartorioonline.api.modelo.Pessoa;

@Repository
public interface ContribuinteEmpresaRepository extends JpaRepository<ContribuinteEmpresa, Long> {
	Optional<ContribuinteEmpresa> findByInscricaoMunicipal(Long inscricaoMunicipal);
	Optional<ContribuinteEmpresa> findByContribuinte(Contribuinte contribuinte);
	Optional<ContribuinteEmpresa> findByPessoaAndInscricaoMunicipal(Pessoa pessoa, Long inscricao);
}
