package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "regime_tributacao", schema = "mobiliario")    
public class RegimeTributacao implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4688230218944403702L;
    @Id
    @SequenceGenerator(name = "regime_tributacao_id_regime_tributacao_seq", sequenceName = "nfe.regime_tributacao_id_regime_tributacao_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "regime_tributacao_id_regime_tributacao_seq")
    @Column(name = "id_regime_tributacao", nullable = false)
    private Integer idRegimeTributacao;
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "tx_regime_tributacao", nullable = false, length = 40)
    private String txRegimeTributacao;
    @Column(name = "bo_regime_trib_pj")
    private Boolean boRegimeTribPj;
    @Column(name = "bo_regime_trib_e_nfe")
    private Boolean boRegimeTribENfe;
    @Column(name = "bo_regime_trib_e_guia_nfe")
    private Boolean boRegimeTribEGuiaNfe;
    @Column(name = "bo_regime_trib_especial")
    private Boolean boRegimeTribEspecial;
    @Column(name = "bo_regime_trib_status")
    private Boolean boRegimeTribStatus;
    @Column(name = "ch_regime_trib_eh_retido")
    private String regimeTribPodeSerRetido;
    @Column(name = "bo_regime_trib_util_serv_tom")
    private Boolean regimeTribUtilizadoDeclaServTomados;
    @Column(name="dt_regime_trib_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
	private Date dataUltimaAtualizacao;
    @Column(name="nu_regime_trib_cod_wswl")    
    private Integer nuRegimeTribCodSswl;
   
    @Column(name="bo_regime_trib_inf_aliquota")
    private Boolean boInformaAliquota; 
    @Column(name="bo_regime_trib_iss_fixo")
    private Boolean boRegimeTribIssFixo;
    
    @Column(name="bo_regime_trib_isento_tlpl")
    private Boolean boRegimeTribIsentoTlpl = false;
    
    @Column(name="bo_regime_trib_isento_tlpl_prim_ano")
    private Boolean boRegimeTribIsentoTlplPrimAno = false;
    
    @Column(name="vl_regime_trib_perc_desconto", precision=2, scale=4)
    private BigDecimal vlRegimeTribPercDesconto;
    
    @Column(name="vl_regime_trib_perc_desc_prim_ano", precision=2, scale=4)
    private BigDecimal vlRegimeTribPercDescontoPrimAno;
    
    @OneToMany(mappedBy = "regimeTributacao")
    private List<ContribEmpresaRegimeTributacao> contribuinteRegimeTributacoes;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="regime_trib_sit_tributaria", schema = "mobiliario", 
			   joinColumns= @JoinColumn(name="id_regime_tributacao", referencedColumnName="id_regime_tributacao"), 
			   inverseJoinColumns = @JoinColumn(name="id_sit_tributacao", referencedColumnName="id_sit_tributacao"))
	private List<SituacaoTributaria> situacaoTributaria;    
    
   
    
    public RegimeTributacao() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
		//situacaoTributaria.
	}
    
    public RegimeTributacao(Integer idRegimeTributacao) {
        this.idRegimeTributacao = idRegimeTributacao;
    }

    public RegimeTributacao(Integer idRegimeTributacao, String txRegimeTributacao) {
        this.idRegimeTributacao = idRegimeTributacao;
        this.txRegimeTributacao = txRegimeTributacao;
    }

    public Integer getIdRegimeTributacao() {
        return idRegimeTributacao;
    }

    public void setIdRegimeTributacao(Integer idRegimeTributacao) {
        this.idRegimeTributacao = idRegimeTributacao;
    }

    public String getTxRegimeTributacao() {
        return txRegimeTributacao;
    }

    public void setTxRegimeTributacao(String txRegimeTributacao) {
        this.txRegimeTributacao = txRegimeTributacao;
    }

    public Boolean getBoRegimeTribPj() {
        return boRegimeTribPj;
    }

    public void setBoRegimeTribPj(Boolean boRegimeTribPj) {
        this.boRegimeTribPj = boRegimeTribPj;
    }

    public Boolean getBoRegimeTribENfe() {
        return boRegimeTribENfe;
    }

    public void setBoRegimeTribENfe(Boolean boRegimeTribENfe) {
        this.boRegimeTribENfe = boRegimeTribENfe;
    }

    public Boolean getBoRegimeTribEGuiaNfe() {
        return boRegimeTribEGuiaNfe;
    }

    public void setBoRegimeTribEGuiaNfe(Boolean boRegimeTribEGuiaNfe) {
        this.boRegimeTribEGuiaNfe = boRegimeTribEGuiaNfe;
    }

    public Boolean getBoRegimeTribEspecial() {
        return boRegimeTribEspecial;
    }

    public void setBoRegimeTribEspecial(Boolean boRegimeTribEspecial) {
        this.boRegimeTribEspecial = boRegimeTribEspecial;
    }

    public Boolean getBoRegimeTribStatus() {
        return boRegimeTribStatus;
    }

    public void setBoRegimeTribStatus(Boolean boRegimeTribStatus) {
        this.boRegimeTribStatus = boRegimeTribStatus;
    }
    
    @Override
    public String toString() {
    	if (this.idRegimeTributacao == null){
    		return "";
    	}
    	else {
    		return this.idRegimeTributacao + " - " + this.txRegimeTributacao;
    	}
    }

	public void setContribuinteRegimeTributacoes(
			List<ContribEmpresaRegimeTributacao> contribuinteRegimeTributacoes) {
		this.contribuinteRegimeTributacoes = contribuinteRegimeTributacoes;
	}

	public List<ContribEmpresaRegimeTributacao> getContribuinteRegimeTributacoes() {
		return contribuinteRegimeTributacoes;
	}

	public void setRegimeTribPodeSerRetido(String regimeTribPodeSerRetido) {
		this.regimeTribPodeSerRetido = regimeTribPodeSerRetido;
	}

	public String getRegimeTribPodeSerRetido() {
		return regimeTribPodeSerRetido;
	}

	public void setRegimeTribUtilizadoDeclaServTomados(
			Boolean regimeTribUtilizadoDeclaServTomados) {
		this.regimeTribUtilizadoDeclaServTomados = regimeTribUtilizadoDeclaServTomados;
	}

	public Boolean getRegimeTribUtilizadoDeclaServTomados() {
		return regimeTribUtilizadoDeclaServTomados;
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setSituacaoTributaria(List<SituacaoTributaria> situacaoTributaria) {
		this.situacaoTributaria = situacaoTributaria;
	}

	public List<SituacaoTributaria> getSituacaoTributaria() {
		return situacaoTributaria;
	}

	public void setBoInformaAliquota(Boolean boInformaAliquota) {
		this.boInformaAliquota = boInformaAliquota;
	}

	public Boolean getBoInformaAliquota() {
		return boInformaAliquota;
	}

	public void setNuRegimeTribCodSswl(Integer nuRegimeTribCodSswl) {
		this.nuRegimeTribCodSswl = nuRegimeTribCodSswl;
	}

	public Integer getNuRegimeTribCodSswl() {
		return nuRegimeTribCodSswl;
	}

	public void setBoRegimeTribIssFixo(Boolean boRegimeTribIssFixo) {
		this.boRegimeTribIssFixo = boRegimeTribIssFixo;
	}

	public Boolean getBoRegimeTribIssFixo() {
		return boRegimeTribIssFixo;
	}

	public Boolean getBoRegimeTribIsentoTlplPrimAno() {
		return boRegimeTribIsentoTlplPrimAno;
	}

	public void setBoRegimeTribIsentoTlplPrimAno(
			Boolean boRegimeTribIsentoTlplPrimAno) {
		this.boRegimeTribIsentoTlplPrimAno = boRegimeTribIsentoTlplPrimAno;
	}

	public BigDecimal getVlRegimeTribPercDesconto() {
		return vlRegimeTribPercDesconto;
	}

	public void setVlRegimeTribPercDesconto(BigDecimal vlRegimeTribPercDesconto) {
		this.vlRegimeTribPercDesconto = vlRegimeTribPercDesconto;
	}

	public BigDecimal getVlRegimeTribPercDescontoPrimAno() {
		return vlRegimeTribPercDescontoPrimAno;
	}

	public void setVlRegimeTribPercDescontoPrimAno(
			BigDecimal vlRegimeTribPercDescontoPrimAno) {
		this.vlRegimeTribPercDescontoPrimAno = vlRegimeTribPercDescontoPrimAno;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public Boolean getBoRegimeTribIsentoTlpl() {
		return boRegimeTribIsentoTlpl;
	}

	public void setBoRegimeTribIsentoTlpl(Boolean boRegimeTribIsentoTlpl) {
		this.boRegimeTribIsentoTlpl = boRegimeTribIsentoTlpl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRegimeTributacao == null) ? 0 : idRegimeTributacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegimeTributacao other = (RegimeTributacao) obj;
		if (idRegimeTributacao == null) {
			if (other.idRegimeTributacao != null)
				return false;
			else if(this == other)
                return true;
			 else 
	             return false;
		} else if (!idRegimeTributacao.equals(other.idRegimeTributacao))
			return false;
		return true;
	}
}
