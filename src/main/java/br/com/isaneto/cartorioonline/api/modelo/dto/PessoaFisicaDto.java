package br.com.isaneto.cartorioonline.api.modelo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.isaneto.cartorioonline.api.modelo.Pessoa;
import br.com.isaneto.cartorioonline.api.modelo.PessoaFisica;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class PessoaFisicaDto {

	private String cpf;
	private String nome;
	private String dataNascimento;
	private String sexo;
	private String tipoIdentificacaoDoc;
	private String numeroDocumento;
	private String dataExpedicao;
	private String ufDoc;
	private String orgaoExpeditor;
	private String cep;
	private String uf;
	private String cidade;
	private String enderecoLogradouro;
	private String enderecoNumero;
	private String enderecoComplemento;
	private String enderecoBairro;
	
	
	public PessoaFisicaDto(Pessoa pessoa) {
		PessoaFisica pf = (PessoaFisica) pessoa;
		this.setCpf(pessoa.getNuPessoaCpfCnpj().toString());
		this.setNome(pessoa.getNoPessoa());
		this.setDataNascimento(pf.getDtPessoaFisNasc() != null ? pf.getDtPessoaFisNasc().toString() : null);
		this.setSexo(pf.getTxPessoaFisSexo());
		this.setTipoIdentificacaoDoc(pf.getTipoIdentificacaoDoc() != null ? pf.getTipoIdentificacaoDoc().getIdTpIdentificacao().toString() : null);
		this.setNumeroDocumento(pf.getTxPessoaFisDoc());
		this.setDataExpedicao(pf.getDtPessoaFisExpeDoc() != null ? pf.getDtPessoaFisExpeDoc().toString() : null);
		this.setUfDoc(pf.getEstadoDoc() != null ? pf.getEstadoDoc().getTxEstadoSigla() : null);
		this.setOrgaoExpeditor(pf.getTxPessoaFisOrgExpDoc() != null ? pf.getTxPessoaFisOrgExpDoc().toString() : null);
		this.setCep(pf.getNuPessoaEndCep() != null ? pf.getNuPessoaEndCep().toString() : null);
		this.setUf(pf.getEstado() != null ? pf.getEstado().getTxEstadoSigla() : null);
		this.setCidade(pf.getCidade() != null ? pf.getCidade().getTxCidadeCodIbge() : null);
		this.setEnderecoLogradouro(pf.getTxPessoaEndeLogradouro());
		this.setEnderecoNumero(pf.getTxPessoaEndNumero());
		this.setEnderecoComplemento(pf.getTxPessoaEndeComplemento());
		this.setEnderecoBairro(pf.getTxPessoaEndBairro());
	}
	
	public PessoaFisicaDto() {
		
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTipoIdentificacaoDoc() {
		return tipoIdentificacaoDoc;
	}
	public void setTipoIdentificacaoDoc(String tipoIdentificacaoDoc) {
		this.tipoIdentificacaoDoc = tipoIdentificacaoDoc;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getDataExpedicao() {
		return dataExpedicao;
	}
	public void setDataExpedicao(String dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}
	public String getUfDoc() {
		return ufDoc;
	}
	public void setUfDoc(String ufDoc) {
		this.ufDoc = ufDoc;
	}
	public String getOrgaoExpeditor() {
		return orgaoExpeditor;
	}
	public void setOrgaoExpeditor(String orgaoExpeditor) {
		this.orgaoExpeditor = orgaoExpeditor;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEnderecoLogradouro() {
		return enderecoLogradouro;
	}
	public void setEnderecoLogradouro(String enderecoLogradouro) {
		this.enderecoLogradouro = enderecoLogradouro;
	}
	public String getEnderecoNumero() {
		return enderecoNumero;
	}
	public void setEnderecoNumero(String enderecoNumero) {
		this.enderecoNumero = enderecoNumero;
	}
	public String getEnderecoComplemento() {
		return enderecoComplemento;
	}
	public void setEnderecoComplemento(String enderecoComplemento) {
		this.enderecoComplemento = enderecoComplemento;
	}
	public String getEnderecoBairro() {
		return enderecoBairro;
	}
	public void setEnderecoBairro(String enderecoBairro) {
		this.enderecoBairro = enderecoBairro;
	}
	
	
	
	
}
