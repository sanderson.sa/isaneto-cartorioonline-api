/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity
@Table(name = "contribuinte_empresa", schema = "mobiliario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nu_contrib_empresa_inscricao"})})    
    
public class ContribuinteEmpresa implements Serializable { 

    private static final long serialVersionUID = -8992406298830342988L;
    
    @Id  
    @Column(name = "id_contribuinte")  
    private Long idContribuinte;
    
    @OneToOne(mappedBy = "contribuinteEmpresa")  
    private Contribuinte contribuinte; 
    
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id_pessoa")
    @ManyToOne(optional = false)
    private Pessoa pessoa;
    
    @Column(name = "nu_contrib_empresa_inscricao")
    private Long inscricaoMunicipal;
    
    @NotNull 
    @Column(name = "tp_contrib_empresa")
    private Character tipoEmpresa;
    
    @JoinColumn(name = "id_tp_contrib_empresa", referencedColumnName = "id_tp_contrib_empresa")
    @ManyToOne
    private TipoContribuinteEmpresa tipoContribuinte;
    
    @Size(max = 16)
    @Column(name = "tx_contrib_empresa_inscricao_hist", length = 16)
    private String inscricaoHistorica;
    
    
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado estado;
    
    
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade cidade;
    
    
    @JoinColumn(name = "id_tp_logradouro", referencedColumnName = "id_tp_logradouro")
    @ManyToOne
    private TipoLogradouro tipoLogradouro;
   
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_ende_logradouro", length = 100)
    private String txContribuinteLogradouro;  
    
    @Size(max = 12)
    @Column(name = "tx_contrib_empresa_end_numero", length = 12)
    private String txContribuinteEnderecoNumero;
   
    @Size(max = 70)
    @Column(name = "tx_contrib_empresa_ende_complemento", length = 70)
    private String txContribuinteEnderecoComplemento;    
   
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_end_bairro", length = 100)
    private String txContribuinteEnderecoBairro;
    
    @Column(name = "nu_contrib_empresa_end_cep")
    private BigInteger nuContribuinteEnderecoCep; 
    
    
    @JoinColumn(name = "id_estado_corresp", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado estadoCorresp;
    
    
    @JoinColumn(name = "id_cidade_corresp", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade cidadeCorresp;    
    
    
    @JoinColumn(name = "id_tp_logradouro_corresp", referencedColumnName = "id_tp_logradouro")
    @ManyToOne
    private TipoLogradouro tipoLogradouroCorresp;
    
    @Column(name = "tx_contrib_empresa_end_logra_corresp", length = 100)
    private String txContribuinteEnderecoLogradouroCorresp;
    
    @Size(max = 12)
    @Column(name = "tx_contrib_empresa_end_numero_corresp", length = 12)
    private String txContribuinteEnderecoNumeroCorresp;
    
    @Size(max = 70)
    @Column(name = "tx_contrib_empresa_ende_compl_corresp", length = 70)
    private String txContribuinteEnderecoComplCorresp;
    
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_end_bairro_corresp", length = 100)
    private String txContribuinteEnderecoBairroCorresp;
    
    @Column(name = "nu_contrib_empresa_end_cep_corresp")
    private BigInteger nuContribuinteEnderecoCepCorresp;
    
    @Column(name = "tx_contrib_empresa_contato", length = 60)
    private String txContribuinteContato;
    
    @Column(name = "tx_contrib_empresa_tel_ddd", length = 3)
    private String txContribuinteDdd;
   
    @Column(name = "tx_contrib_empresa_telefone", length = 20)
    private String txContribuinteTelefone;
    
    @Column(name = "tx_contrib_empresa_fax_ddd", length = 3)
    private String txContribuinteFaxDdd;
    
    @Column(name = "tx_contrib_empresa_fax_telefone", length = 9)
    private String txContribuinteFaxTelefone;    
    
    @Column(name = "tx_contrib_empresa_cel_ddd", length = 3)
    private String txContribuinteCelDdd;
    
    @Column(name = "tx_contrib_empresa_cel_telefone", length = 9)
    private String txContribuinteCelTelefone;
    
    @Email
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_email", length = 100)
    private String txContribuinteEmail;
    
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_endereco_site", length = 100)
    private String txContribuinteSite;
   
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_facebook", length = 100)
    private String txContribuinteFaceBook;
   
    @Size(max = 100)
    @Column(name = "tx_contrib_empresa_twitter", length = 100)
    private String txContribuinteTwitter;
    
    @Column(name = "nu_contrib_empresa_qtd_empregados")
    private Integer qtdEmpregados;
    
    @Column(name = "nu_contrib_empresa_area", precision = 15, scale = 2)
    private BigDecimal areaEmpresa;
    
    @Column(name = "nu_contrib_qtd_dias_trabalha_excedente_semana")
    private Integer nuQtdDiasTrabalhaExcedenteSemana = 0;
    
    @Column(name = "nu_contrib_qtd_horas_trabalha_excedente")
    private Integer nuQtdHorasTrabalhaExcedente = 0;
   
    @Temporal(TemporalType.DATE)    
    @Column(name = "dt_contrib_empresa_inicio_lib_emissao_nfsd")
    private Date dataInicioLiberacaoEmissaoNfse;
    
    @Temporal(TemporalType.DATE)    
    @Column(name = "dt_contrib_empresa_fim_lib_emissao_nfsd")
    private Date dataFimLiberacaoEmissaoNfse;
    
    @Column(name = "bo_contrib_empresa_usa_web_service")
    private Boolean isLiberadoUsoWebService = false;
    
    @Column(name = "bo_contrib_empresa_iss")
    private Boolean boContribEmpresaIss = false;
    
    @Column(name = "bo_contrib_empresa_permite_deducao_iss")
    private Boolean permiteDeducaoIss = false;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_contrib_empresa_cad")    
    private Date dataContribEmpresaCadastro;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_contrib_registro")    
    private Date dtContribRegistro;
    
	
    @JoinColumn(name = "id_operador_incluiu", referencedColumnName = "id_operador")
    @ManyToOne       
    private Operador operadorIncluiu;
    
	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "id_class_prof_autonomo", referencedColumnName = "id_class_prof_autonomo")
    private ClassificacaoProfissionalAutonomo classifacaoProfAutonomo;*/
    
    
    @JoinColumn(name = "id_operador_alterou", referencedColumnName = "id_operador")
    @ManyToOne
    private Operador idOperadorAlterou;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_contrib_empresa_alterou")    
    private Date dataContribEmpresaAlterou;
        
 
    @Column(name="tx_contrib_empresa_justif_bloqueio_emite_nfe")
    private String txContribEmpresaJustifBloqueioEmiteNfe; 
    
    @Column(name="tx_contrib_empresa_observacao")
    private String txContribEmpresaObservacao; 
    
	@Column(name="ho_contrib_empresa_horario_funcionamento_inicio")
	private Time hoHorarioFuncionamentoInicio;
	
	@Column(name="ho_contrib_empresa_horario_funcionamento_termino")
	private Time hoHorarioFuncionamentoTermino;
	
	@Column(name="tx_contrib_empresa_nome_alternativo")
	private String nomeAlternativo;
	
	@JoinColumn(name = "id_tp_estabelecimento", referencedColumnName = "id_tp_estabelecimento")
    @ManyToOne
	private TipoEstabelecimento tipoEstabelecimento;
 
    
    public ContribuinteEmpresa() {
    	Calendar c = new GregorianCalendar();
    	c.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, 8, 0, 0);
    	hoHorarioFuncionamentoInicio = new Time(c.getTimeInMillis());
    	
    	c.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, 17, 0, 0);
    	hoHorarioFuncionamentoTermino = new Time(c.getTimeInMillis());
    }
    
    public ContribuinteEmpresa(Contribuinte contribuinte, Pessoa pessoa,
            Long inscridoMunicipal, char tipoEmpresa) {
        this.contribuinte = contribuinte;
        this.pessoa = pessoa;
        this.inscricaoMunicipal = inscricaoMunicipal;
        this.tipoEmpresa = tipoEmpresa;
    }

		
	public Contribuinte getContribuinte() {
		return contribuinte;
	}

	public void setContribuinte(Contribuinte contribuinte) {
		this.contribuinte = contribuinte;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public Long getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(Long inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public Character getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(Character tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	public TipoContribuinteEmpresa getTipoContribuinte() {
		return tipoContribuinte;
	}

	public void setTipoContribuinte(TipoContribuinteEmpresa tipoContribuinte) {
		this.tipoContribuinte = tipoContribuinte;
	}

	public String getInscricaoHistorica() {
		return inscricaoHistorica;
	}

	public void setInscricaoHistorica(String inscricaoHistorica) {
		this.inscricaoHistorica = inscricaoHistorica;
	}

	public Integer getQtdEmpregados() {
		return qtdEmpregados;
	}

	public void setQtdEmpregados(Integer qtdEmpregados) {
		this.qtdEmpregados = qtdEmpregados;
	}

	public BigDecimal getAreaEmpresa() {
		return areaEmpresa;
	}

	public void setAreaEmpresa(BigDecimal areaEmpresa) {
		this.areaEmpresa = areaEmpresa;
	}



	

	public Date getDataInicioLiberacaoEmissaoNfse() {
		return dataInicioLiberacaoEmissaoNfse;
	}

	public void setDataInicioLiberacaoEmissaoNfse(
			Date dataInicioLiberacaoEmissaoNfse) {
		this.dataInicioLiberacaoEmissaoNfse = dataInicioLiberacaoEmissaoNfse;
	}

	public Date getDataFimLiberacaoEmissaoNfse() {
		return dataFimLiberacaoEmissaoNfse;
	}

	public void setDataFimLiberacaoEmissaoNfse(Date dataFimLiberacaoEmissaoNfse) {
		this.dataFimLiberacaoEmissaoNfse = dataFimLiberacaoEmissaoNfse;
	}

	public Boolean getIsLiberadoUsoWebService() {
		return isLiberadoUsoWebService;
	}

	public void setIsLiberadoUsoWebService(Boolean isLiberadoUsoWebService) {
		this.isLiberadoUsoWebService = isLiberadoUsoWebService;
	}

	public Date getDataContribEmpresaCadastro() {
		return dataContribEmpresaCadastro;
	}

	public void setDataContribEmpresaCadastro(Date dataContribEmpresaCadastro) {
		this.dataContribEmpresaCadastro = dataContribEmpresaCadastro;
	}

	public Operador getOperadorIncluiu() {
		return operadorIncluiu;
	}

	public void setOperadorIncluiu(Operador operadorIncluiu) {
		this.operadorIncluiu = operadorIncluiu;
	}

	public Operador getIdOperadorAlterou() {
		return idOperadorAlterou;
	}

	public void setIdOperadorAlterou(Operador idOperadorAlterou) {
		this.idOperadorAlterou = idOperadorAlterou;
	}

	public Date getDataContribEmpresaAlterou() {
		return dataContribEmpresaAlterou;
	}

	public void setDataContribEmpresaAlterou(Date dataContribEmpresaAlterou) {
		this.dataContribEmpresaAlterou = dataContribEmpresaAlterou;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getContribuinte() == null) ? 0 : getContribuinte().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ContribuinteEmpresa)){
			return false;
		}
		ContribuinteEmpresa other = (ContribuinteEmpresa) obj;
		if (getContribuinte() == null) {
			if (other.getContribuinte() != null)
				return false;
		} else if (!getContribuinte().equals(other.getContribuinte()))
			return false;
		return true;
	}

  
	public Long getIdContribuinte() {
		return idContribuinte;
	}

	public void setIdContribuinte(Long idContribuinte) {
		this.idContribuinte = idContribuinte;
	}

	/*public ClassificacaoProfissionalAutonomo getClassifacaoProfAutonomo() {
		return classifacaoProfAutonomo;
	}

	public void setClassifacaoProfAutonomo(ClassificacaoProfissionalAutonomo classifacaoProfAutonomo) {
		this.classifacaoProfAutonomo = classifacaoProfAutonomo;
	}*/

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getTxContribuinteLogradouro() {
		return txContribuinteLogradouro;
	}

	public void setTxContribuinteLogradouro(String txContribuinteLogradouro) {
		this.txContribuinteLogradouro = txContribuinteLogradouro;
	}

	public String getTxContribuinteEnderecoNumero() {
		return txContribuinteEnderecoNumero;
	}

	public void setTxContribuinteEnderecoNumero(String txContribuinteEnderecoNumero) {
		this.txContribuinteEnderecoNumero = txContribuinteEnderecoNumero;
	}

	public String getTxContribuinteEnderecoComplemento() {
		return txContribuinteEnderecoComplemento;
	}

	public void setTxContribuinteEnderecoComplemento(
			String txContribuinteEnderecoComplemento) {
		this.txContribuinteEnderecoComplemento = txContribuinteEnderecoComplemento;
	}

	public String getTxContribuinteEnderecoBairro() {
		return txContribuinteEnderecoBairro;
	}

	public void setTxContribuinteEnderecoBairro(String txContribuinteEnderecoBairro) {
		this.txContribuinteEnderecoBairro = txContribuinteEnderecoBairro;
	}

	public BigInteger getNuContribuinteEnderecoCep() {
		return nuContribuinteEnderecoCep;
	}

	public void setNuContribuinteEnderecoCep(BigInteger nuContribuinteEnderecoCep) {
		this.nuContribuinteEnderecoCep = nuContribuinteEnderecoCep;
	}

	public Estado getEstadoCorresp() {
		return estadoCorresp;
	}

	public void setEstadoCorresp(Estado estadoCorresp) {
		this.estadoCorresp = estadoCorresp;
	}

	public Cidade getCidadeCorresp() {
		return cidadeCorresp;
	}

	public void setCidadeCorresp(Cidade cidadeCorresp) {
		this.cidadeCorresp = cidadeCorresp;
	}

	public TipoLogradouro getTipoLogradouroCorresp() {
		return tipoLogradouroCorresp;
	}

	public void setTipoLogradouroCorresp(TipoLogradouro tipoLogradouroCorresp) {
		this.tipoLogradouroCorresp = tipoLogradouroCorresp;
	}

	public String getTxContribuinteEnderecoLogradouroCorresp() {
		return txContribuinteEnderecoLogradouroCorresp;
	}

	public void setTxContribuinteEnderecoLogradouroCorresp(
			String txContribuinteEnderecoLogradouroCorresp) {
		this.txContribuinteEnderecoLogradouroCorresp = txContribuinteEnderecoLogradouroCorresp;
	}

	public String getTxContribuinteEnderecoNumeroCorresp() {
		return txContribuinteEnderecoNumeroCorresp;
	}

	public void setTxContribuinteEnderecoNumeroCorresp(
			String txContribuinteEnderecoNumeroCorresp) {
		this.txContribuinteEnderecoNumeroCorresp = txContribuinteEnderecoNumeroCorresp;
	}

	public String getTxContribuinteEnderecoComplCorresp() {
		return txContribuinteEnderecoComplCorresp;
	}

	public void setTxContribuinteEnderecoComplCorresp(
			String txContribuinteEnderecoComplCorresp) {
		this.txContribuinteEnderecoComplCorresp = txContribuinteEnderecoComplCorresp;
	}

	public String getTxContribuinteEnderecoBairroCorresp() {
		return txContribuinteEnderecoBairroCorresp;
	}

	public void setTxContribuinteEnderecoBairroCorresp(
			String txContribuinteEnderecoBairroCorresp) {
		this.txContribuinteEnderecoBairroCorresp = txContribuinteEnderecoBairroCorresp;
	}

	public BigInteger getNuContribuinteEnderecoCepCorresp() {
		return nuContribuinteEnderecoCepCorresp;
	}

	public void setNuContribuinteEnderecoCepCorresp(
			BigInteger nuContribuinteEnderecoCepCorresp) {
		this.nuContribuinteEnderecoCepCorresp = nuContribuinteEnderecoCepCorresp;
	}

	public String getTxContribuinteContato() {
		return txContribuinteContato;
	}

	public void setTxContribuinteContato(String txContribuinteContato) {
		this.txContribuinteContato = txContribuinteContato;
	}

	public String getTxContribuinteDdd() {
		return txContribuinteDdd;
	}

	public void setTxContribuinteDdd(String txContribuinteDdd) {
		this.txContribuinteDdd = txContribuinteDdd;
	}

	public String getTxContribuinteTelefone() {
		return txContribuinteTelefone;
	}

	public void setTxContribuinteTelefone(String txContribuinteTelefone) {
		this.txContribuinteTelefone = txContribuinteTelefone;
	}

	public String getTxContribuinteFaxDdd() {
		return txContribuinteFaxDdd;
	}

	public void setTxContribuinteFaxDdd(String txContribuinteFaxDdd) {
		this.txContribuinteFaxDdd = txContribuinteFaxDdd;
	}

	public String getTxContribuinteFaxTelefone() {
		return txContribuinteFaxTelefone;
	}

	public void setTxContribuinteFaxTelefone(String txContribuinteFaxTelefone) {
		this.txContribuinteFaxTelefone = txContribuinteFaxTelefone;
	}

	public String getTxContribuinteCelDdd() {
		return txContribuinteCelDdd;
	}

	public void setTxContribuinteCelDdd(String txContribuinteCelDdd) {
		this.txContribuinteCelDdd = txContribuinteCelDdd;
	}

	public String getTxContribuinteCelTelefone() {
		return txContribuinteCelTelefone;
	}

	public void setTxContribuinteCelTelefone(String txContribuinteCelTelefone) {
		this.txContribuinteCelTelefone = txContribuinteCelTelefone;
	}

	public String getTxContribuinteEmail() {
		return txContribuinteEmail;
	}

	public void setTxContribuinteEmail(String txContribuinteEmail) {
		this.txContribuinteEmail = txContribuinteEmail;
	}

	public String getTxContribuinteSite() {
		return txContribuinteSite;
	}

	public void setTxContribuinteSite(String txContribuinteSite) {
		this.txContribuinteSite = txContribuinteSite;
	}

	public String getTxContribuinteFaceBook() {
		return txContribuinteFaceBook;
	}

	public void setTxContribuinteFaceBook(String txContribuinteFaceBook) {
		this.txContribuinteFaceBook = txContribuinteFaceBook;
	}

	public String getTxContribuinteTwitter() {
		return txContribuinteTwitter;
	}

	public void setTxContribuinteTwitter(String txContribuinteTwitter) {
		this.txContribuinteTwitter = txContribuinteTwitter;
	}

	

	/*public SubTipoContribuinteEmpresa getSubTipoContribuinteEmpresa() {
		return subTipoContribuinteEmpresa;
	}

	public void setSubTipoContribuinteEmpresa(
			SubTipoContribuinteEmpresa subTipoContribuinteEmpresa) {
		this.subTipoContribuinteEmpresa = subTipoContribuinteEmpresa;
	}*/

	public Time getHoHorarioFuncionamentoInicio() {
		return hoHorarioFuncionamentoInicio;
	}

	public void setHoHorarioFuncionamentoInicio(Time hoHorarioFuncionamentoInicio) {
		this.hoHorarioFuncionamentoInicio = hoHorarioFuncionamentoInicio;
	}

	public Time getHoHorarioFuncionamentoTermino() {
		return hoHorarioFuncionamentoTermino;
	}

	public void setHoHorarioFuncionamentoTermino(Time hoHorarioFuncionamentoTermino) {
		this.hoHorarioFuncionamentoTermino = hoHorarioFuncionamentoTermino;
	}

	
	public Date getDtContribRegistro() {
		return dtContribRegistro;
	}

	public void setDtContribRegistro(Date dtContribRegistro) {
		this.dtContribRegistro = dtContribRegistro;
	}

	public Boolean getBoContribEmpresaIss() {
		return boContribEmpresaIss;
	}

	public void setBoContribEmpresaIss(Boolean boContribEmpresaIss) {
		this.boContribEmpresaIss = boContribEmpresaIss;
	}

	public Boolean getPermiteDeducaoIss() {
		return permiteDeducaoIss;
	}

	public void setPermiteDeducaoIss(Boolean permiteDeducaoIss) {
		this.permiteDeducaoIss = permiteDeducaoIss;
	}

	public String getTxContribEmpresaJustifBloqueioEmiteNfe() {
		return txContribEmpresaJustifBloqueioEmiteNfe;
	}

	public void setTxContribEmpresaJustifBloqueioEmiteNfe(String txContribEmpresaJustifBloqueioEmiteNfe) {
		this.txContribEmpresaJustifBloqueioEmiteNfe = txContribEmpresaJustifBloqueioEmiteNfe;
	}

	public String getNomeAlternativo() {
		return nomeAlternativo;
	}

	public void setNomeAlternativo(String nomeAlternativo) {
		this.nomeAlternativo = nomeAlternativo;
	}

	
	public String getTxContribEmpresaObservacao() {
		return txContribEmpresaObservacao;
	}

	public void setTxContribEmpresaObservacao(String txContribEmpresaObservacao) {
		this.txContribEmpresaObservacao = txContribEmpresaObservacao;
	}

	

	public Integer getNuQtdDiasTrabalhaExcedenteSemana() {
		return nuQtdDiasTrabalhaExcedenteSemana;
	}

	public void setNuQtdDiasTrabalhaExcedenteSemana(Integer nuQtdDiasTrabalhaExcedenteSemana) {
		this.nuQtdDiasTrabalhaExcedenteSemana = nuQtdDiasTrabalhaExcedenteSemana;
	}

	public Integer getNuQtdHorasTrabalhaExcedente() {
		return nuQtdHorasTrabalhaExcedente;
	}

	public void setNuQtdHorasTrabalhaExcedente(Integer nuQtdHorasTrabalhaExcedente) {
		this.nuQtdHorasTrabalhaExcedente = nuQtdHorasTrabalhaExcedente;
	}



	public TipoEstabelecimento getTipoEstabelecimento() {
		return tipoEstabelecimento;
	}



	public void setTipoEstabelecimento(TipoEstabelecimento tipoEstabelecimento) {
		this.tipoEstabelecimento = tipoEstabelecimento;
	}


}