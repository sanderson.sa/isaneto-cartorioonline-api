package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Short> {
	Estado findByTxEstadoSigla(String uf);
}
