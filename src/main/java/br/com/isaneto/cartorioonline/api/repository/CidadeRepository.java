package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer> {
	Cidade findByTxCidadeCodIbge(String codIbge);
}
