package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Bruno
 */
@Entity
@Table(name = "tipo_identificacao", schema = "public")

public class TipoIdentificacao implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6512461020893217800L;
    @Id
    @SequenceGenerator(name = "tipo_identificacao_id_tp_identificacao_seq", sequenceName = "tipo_identificacao_id_tp_identificacao_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_identificacao_id_tp_identificacao_seq")
    @Column(name = "id_tp_identificacao", nullable = false)
    private Short idTpIdentificacao;
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tx_tp_identificacao", nullable = false, length = 30)
    private String txTpIdentificacao;

    public TipoIdentificacao() {
    }

    public TipoIdentificacao(Short idTpIdentificacao) {
        this.idTpIdentificacao = idTpIdentificacao;
    }

    public TipoIdentificacao(Short idTpIdentificacao, String txTpIdentificacao) {
        this.idTpIdentificacao = idTpIdentificacao;
        this.txTpIdentificacao = txTpIdentificacao;
    }

    public Short getIdTpIdentificacao() {
        return idTpIdentificacao;
    }

    public void setIdTpIdentificacao(Short idTpIdentificacao) {
        this.idTpIdentificacao = idTpIdentificacao;
    }

    public String getTxTpIdentificacao() {
        return txTpIdentificacao;
    }

    public void setTxTpIdentificacao(String txTpIdentificacao) {
        this.txTpIdentificacao = txTpIdentificacao;
    }   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTpIdentificacao != null ? idTpIdentificacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TipoIdentificacao)) {
            return false;
        }
        TipoIdentificacao other = (TipoIdentificacao) object;
        if ((this.idTpIdentificacao == null && other.idTpIdentificacao != null)
                || (this.idTpIdentificacao != null && !this.idTpIdentificacao.equals(other.idTpIdentificacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoIdentificacao - " + txTpIdentificacao;
    }

}
