package br.com.isaneto.cartorioonline.api.modelo.dto;

import java.io.Serializable;

public class RepresentanteLegalCpfDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cpf;
	private String nome;
	private String dataNascimento;
	private String sexo;
	private String tipoIdentificacaoDoc;
	private String numeroDocumento;
	private String dataExpedicao;
	private String ufExpedicaoDocumento;
	private String orgaoExpeditor;
	private String cep;
	private String ufResidencia;
	private String cidade;
	private String enderecoLogradouro;
	private String enderecoNumero;
	private String enderecoComplemento;
	private String enderecoBairro;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTipoIdentificacaoDoc() {
		return tipoIdentificacaoDoc;
	}
	public void setTipoIdentificacaoDoc(String tipoIdentificacaoDoc) {
		this.tipoIdentificacaoDoc = tipoIdentificacaoDoc;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getDataExpedicao() {
		return dataExpedicao;
	}
	public void setDataExpedicao(String dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}
	public String getUfExpedicaoDocumento() {
		return ufExpedicaoDocumento;
	}
	public void setUfExpedicaoDocumento(String ufExpedicaoDocumento) {
		this.ufExpedicaoDocumento = ufExpedicaoDocumento;
	}
	public String getOrgaoExpeditor() {
		return orgaoExpeditor;
	}
	public void setOrgaoExpeditor(String orgaoExpeditor) {
		this.orgaoExpeditor = orgaoExpeditor;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getUfResidencia() {
		return ufResidencia;
	}
	public void setUfResidencia(String ufResidencia) {
		this.ufResidencia = ufResidencia;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEnderecoLogradouro() {
		return enderecoLogradouro;
	}
	public void setEnderecoLogradouro(String enderecoLogradouro) {
		this.enderecoLogradouro = enderecoLogradouro;
	}
	public String getEnderecoNumero() {
		return enderecoNumero;
	}
	public void setEnderecoNumero(String enderecoNumero) {
		this.enderecoNumero = enderecoNumero;
	}
	public String getEnderecoComplemento() {
		return enderecoComplemento;
	}
	public void setEnderecoComplemento(String enderecoComplemento) {
		this.enderecoComplemento = enderecoComplemento;
	}
	public String getEnderecoBairro() {
		return enderecoBairro;
	}
	public void setEnderecoBairro(String enderecoBairro) {
		this.enderecoBairro = enderecoBairro;
	}
	
	

}
