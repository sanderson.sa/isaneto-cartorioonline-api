package br.com.isaneto.cartorioonline.api.modelo.dto;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.isaneto.cartorioonline.api.modelo.PessoaJuridica;
import br.com.isaneto.cartorioonline.api.util.Util;
import lombok.Data;

@Data
@JsonInclude(NON_NULL)
public class PessoaJuridicaDTO {

	@NotNull(message = "CNPJ - Não pode ser Nullo")
	@Size(max = 14, message = "CNPJ - Tamanho máximo 14 caracteres")
	private String cnpj;
	
	@NotNull(message = "RAZAOSOCIAL - Não pode ser nullo")
	@Size(max = 120, message = "RAZAOSOCIAL - Tamanho máximo 120 caracteres")
	private String razaoSocial;
	
	@NotNull(message = "NOMEFANTASIA - Não pode ser Nullo")
	@Size(max = 120, message = "NOMEFANTASIA - Tamanho máximo de 120 caracteres")
	private String nomeFantasia;
	
	private String inscricaoEstadual;
	
	@NotNull(message = "NATUREZAJURIDICA - Não pode ser Nullo")
	@Size(max = 5, message = "NATUREZAJURIDICA - Tamanho máximo 5 caracteres")
	private String naturezaJuridica;
	
	@NotNull(message = "DATACONSTITUICAO - Não pode ser Nullo")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", locale = "pt-BR", timezone = "Brazil/East")
	private String dataConstituicao;
	
	//Capital Social da empresa, sendo o valor informados sem separador decimal. Os dois últimos digitos serão considerados o valor decimal Máx. 18 digitos
	@NotNull(message = "CAPITALSOCIAL - Não pode ser Nullo")
	private String capitalSocial;

	//: Tipo de Estabelecimento da empresa conforme Anexo I
	@NotNull(message = "TIPOESTABELECIMENTO - Não pode ser Nullo")
	@Size(max = 2, message = "TIPOESTABELECIMENTO - Tamanho máximo 2 caracteres")
	private String tipoEstabelecimento;

	@NotNull(message = "DATACADASTRO - Não pode ser Nullo")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", locale = "pt-BR", timezone = "Brazil/East")
	private String dataCadastro;
	
	@NotNull(message = "UF -Não pode ser Nullo")
	private String uf;
	
	//: Código do IBGE da Cidade onde a empresa está estabelecidaI
	@NotNull(message = "CIDADE - Não pode ser Nullo")
	private String cidade;
	
	//: Cep do endereço da empresa sem mascara de formatação
	@NotNull(message = "CEP - Não pode ser Nullo")
	@Size(max = 8, message = "CEP - Tamanho máximo 8 caracteres")
	private String cep;
	
	@NotNull(message = "ENDERECOLOGRADOURO - Não pode ser Nullo")
	@Size(max = 100, message = "ENDERECOLOGRADOURO - Tamanho máximo 100 caracteres")
	private String enderecoLogradouro;
	
	@NotNull(message = "ENDERECONUMERO - Não pode ser Nullo")
	private String enderecoNumero;
	
	private String enderecoComplemento;
	
	@NotNull(message = "ENDERECOBAIRRO - Não pode ser Nullo")
	private String enderecoBairro;
	private String ddd;
	private String telefone;
	private String dddCelular;
	private String celular;
	private String email;
	private String representanteLegalCpf; 
	private String representanteLegalCnpj;
	
	public PessoaJuridicaDTO() {
		
	}
	
	public PessoaJuridicaDTO(PessoaJuridica pj) {
		this.cnpj = pj.getNuPessoaCpfCnpj().toString();
		this.razaoSocial = pj.getNoPessoa();
		this.nomeFantasia = pj.getTxPessoaJurFantasia();
//		this.inscricao = pj.getin;
		this.naturezaJuridica = pj.getIdNatJuridica() != null ? pj.getIdNatJuridica().getNuNatJuridicaCod() : null;
		this.dataConstituicao = Util.dateToStr(pj.getDtPessoaCad());
		this.capitalSocial = pj.getVlPessoaJurCapital() != null ? pj.getVlPessoaJurCapital().toString() : null;
//		this.tipoEstabelecimento = pj.get;
//		this.dataCadastro = dataContribEmpresaCadastro;
		this.uf = pj.getEstado().getTxEstadoSigla();
		this.cidade = pj.getCidade().getTxCidadeCodIbge();
		this.cep = pj.getNuPessoaEndCep().toString();
		this.enderecoLogradouro = pj.getTxPessoaEndeLogradouro();
		this.enderecoNumero = pj.getTxPessoaEndNumero();
		this.enderecoComplemento = pj.getTxPessoaEndeComplemento();
		this.enderecoBairro = pj.getTxPessoaEndBairro();
		this.ddd = pj.getTxPessoaDdd();
		this.telefone = pj.getTxPessoaTelefone();
		this.dddCelular = pj.getTxPessoaCelDdd();
		this.celular = pj.getTxCelularComDdd();
		this.email = pj.getTxPessoaEmail();
		if(pj.getIdRepresentanteLegal() != null) {
			if(pj.getIdRepresentanteLegal().getTpPessoa().equals("F"))
				this.representanteLegalCpf = pj.getIdRepresentanteLegal().getNuPessoaCpfCnpj().toString();
			else
				this.representanteLegalCnpj = pj.getIdRepresentanteLegal().getNuPessoaCpfCnpj().toString();
		}
	}
	
}
