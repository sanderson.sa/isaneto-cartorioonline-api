package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.RegimeTributacao;

@Repository
public interface RegimeTributacaoRepository extends JpaRepository<RegimeTributacao, Integer> {
	
}
