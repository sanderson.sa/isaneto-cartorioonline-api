package br.com.isaneto.cartorioonline.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.NaturezaJuridica;

@Repository
public interface NaturezaJuridicaRepository extends JpaRepository<NaturezaJuridica, Integer> {
	Optional<NaturezaJuridica> findByNuNatJuridicaCod(String nuNatJuridicaCod);
}
