/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * 
 * @author Bruno
 */


@Entity
@Table(name = "pessoa", schema = "public")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tp_pessoa", discriminatorType = DiscriminatorType.STRING, length = 1)
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pessoa implements Serializable {

 
    private static final long serialVersionUID = 5105038223197139006L;
   
    @Id
    @SequenceGenerator(name = "pessoa_id_pessoa_seq", sequenceName = "pessoa_id_pessoa_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_id_pessoa_seq")
    @Column(name = "id_pessoa", nullable = false)
    private Long idPessoa;
    
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "no_pessoa", nullable = false, length = 120)
    private String noPessoa;
    @NotNull
    @Column(name = "tp_pessoa", nullable = false, insertable = false, updatable = false)
    private String tpPessoa;
    @Column(name = "nu_pessoa_cpf_cnpj")
    private BigInteger nuPessoaCpfCnpj;
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado estado;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade cidade;
    @JoinColumn(name = "id_tp_logradouro", referencedColumnName = "id_tp_logradouro")
    @ManyToOne
    private TipoLogradouro tipoLogradouro;
    @Size(max = 100)
    @Column(name = "tx_pessoa_ende_logradouro", length = 100)
    private String txPessoaEndeLogradouro;       
    @Size(max = 12)
    @Column(name = "tx_pessoa_end_numero", length = 12)
    private String txPessoaEndNumero;
    @Size(max = 70)
    @Column(name = "tx_pessoa_ende_complemento", length = 70)
    private String txPessoaEndeComplemento;    
    @Size(max = 100)
    @Column(name = "tx_pessoa_end_bairro", length = 100)
    private String txPessoaEndBairro;
    @Column(name = "nu_pessoa_end_cep")
    private BigInteger nuPessoaEndCep;    
    @JoinColumn(name = "id_estado_corresp", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado estadoCorresp;
    @JoinColumn(name = "id_cidade_corresp", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade cidadeCorresp;  
    @JoinColumn(name = "id_tp_logradouro_corresp", referencedColumnName = "id_tp_logradouro")
    @ManyToOne
    private TipoLogradouro tipoLogradouroCorresp;
    @Column(name = "tx_pessoa_end_logra_corresp", length = 100)
    private String txPessoaEndLograCorresp;
    @Size(max = 12)
    @Column(name = "tx_pessoa_end_numero_corresp", length = 12)
    private String txPessoaEndNumeroCorresp;
    @Size(max = 70)
    @Column(name = "tx_pessoa_ende_compl_corresp", length = 70)
    private String txPessoaEndeComplCorresp;
    @Size(max = 100)
    @Column(name = "tx_pessoa_end_bairro_corresp", length = 100)
    private String txPessoaEndBairroCorresp;
    @Column(name = "nu_pessoa_end_cep_corresp")
    private BigInteger nuPessoaEndCepCorresp;
    @Column(name = "tx_pessoa_contato", length = 60)
    private String txPessoaContato;
    @Column(name = "tx_pessoa_tel_ddd", length = 3)
    private String txPessoaDdd;
    @Column(name = "tx_pessoa_telefone", length = 20)
    private String txPessoaTelefone;
    @Column(name = "tx_pessoa_fax_ddd", length = 3)
    private String txPessoaFaxDdd;
    @Column(name = "tx_pessoa_fax_telefone", length = 10)
    private String txPessoaFaxTelefone;    
    @Column(name = "tx_pessoa_cel_ddd", length = 3)
    private String txPessoaCelDdd;
    @Column(name = "tx_pessoa_cel_telefone", length = 10)
    private String txPessoaCelTelefone;
    @Email
    @Size(max = 100)
    @Column(name = "tx_pessoa_email", length = 100)
    private String txPessoaEmail;
    @Basic(optional = false)
    @Column(name = "dt_pessoa_cad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPessoaCad;
   
    @ManyToOne
    @JoinColumn(name = "id_operador_cadastro", referencedColumnName="id_operador")
    private Operador operadorCadastrou;
    
    @ManyToOne
    @JoinColumn(name = "id_operador_alterou", referencedColumnName="id_operador")
    private Operador operadorAlterou;
          
    @Column(name = "dt_pessoa_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPessoaAlterou;
    
    @OneToOne(mappedBy = "pessoa")
    private Operador operador;
  
    
    
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pessoa")
//    private PessoaJuridica pessoaJuridica;
    
    @OneToMany(mappedBy = "pessoa")
    private List<Contribuinte> contribuintes;

    @OneToMany(mappedBy = "pessoa")
    private List<ContribuinteEmpresa> contribuinteEmpresas;

	@Transient
    private String cpfCnpjFormatado;
	
	@Transient
	private String txTelefoneComDdd;
	
	@Transient
	private String txCelularComDdd;

	@Transient
	private String txCelularComDddFormatado;
	@Transient
	private String txCelularFormatado;
	
	@Transient
	private String txFaxComDdd;

    
    public Pessoa() {
    	//tipoLogradouro = new TipoLogradouro();
    }

    public Pessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Pessoa(Long idPessoa, String noPessoa, String tpPessoa,
            Date dtPessoaCad) {
        this.idPessoa = idPessoa;
        this.noPessoa = noPessoa;
        this.tpPessoa = tpPessoa;
        this.dtPessoaCad = dtPessoaCad;
    }
    
    @Transient
    public String getEnderecoFormatado(){
    	StringBuilder endereco = new StringBuilder();
    	if(tipoLogradouro != null){
    		endereco.append(tipoLogradouro.getNoTpLogradouro());
    		endereco.append(" ");
    	}
    	if(txPessoaEndeLogradouro != null){
    		endereco.append(txPessoaEndeLogradouro);
    	}
    	if(txPessoaEndNumero != null){
    		endereco.append(", ");
    		endereco.append(txPessoaEndNumero);
    		endereco.append(". ");
    	}
    	if(txPessoaEndBairro != null){
    		endereco.append(txPessoaEndBairro);
    		endereco.append(". ");
    	}
    	if(cidade != null){
    		endereco.append(cidade.getNoCidade());
    		endereco.append(", ");
    	}
    	if(estado != null){
    		endereco.append(estado.getNoEstado());
    		endereco.append(". ");
    	}
    	
    	if(txPessoaEndeComplemento != null){
    		endereco.append("\n");
    		endereco.append(txPessoaEndeComplemento);
    	}
    	return endereco.toString().toUpperCase();
    }
    
	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNoPessoa() {
		return noPessoa;
	}

	public void setNoPessoa(String noPessoa) {
		this.noPessoa = noPessoa;
	}

	public String getTpPessoa() {
		return tpPessoa;
	}

	public void setTpPessoa(String tpPessoa) {
		this.tpPessoa = tpPessoa;
	}

	public BigInteger getNuPessoaCpfCnpj() {
		return nuPessoaCpfCnpj;
	}

	public void setNuPessoaCpfCnpj(BigInteger nuPessoaCpfCnpj) {
		this.nuPessoaCpfCnpj = nuPessoaCpfCnpj;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getTxPessoaEndeLogradouro() {
		return txPessoaEndeLogradouro;
	}

	public void setTxPessoaEndeLogradouro(String txPessoaEndeLogradouro) {
		this.txPessoaEndeLogradouro = txPessoaEndeLogradouro;
	}

	public String getTxPessoaEndNumero() {
		return txPessoaEndNumero;
	}

	public void setTxPessoaEndNumero(String txPessoaEndNumero) {
		this.txPessoaEndNumero = txPessoaEndNumero;
	}

	public String getTxPessoaEndeComplemento() {
		return txPessoaEndeComplemento;
	}

	public void setTxPessoaEndeComplemento(String txPessoaEndeComplemento) {
		this.txPessoaEndeComplemento = txPessoaEndeComplemento;
	}

	public String getTxPessoaEndBairro() {
		return txPessoaEndBairro;
	}

	public void setTxPessoaEndBairro(String txPessoaEndBairro) {
		this.txPessoaEndBairro = txPessoaEndBairro;
	}

	public BigInteger getNuPessoaEndCep() {
		return nuPessoaEndCep;
	}

	public void setNuPessoaEndCep(BigInteger nuPessoaEndCep) {
		this.nuPessoaEndCep = nuPessoaEndCep;
	}

	public Estado getEstadoCorresp() {
		return estadoCorresp;
	}

	public void setEstadoCorresp(Estado estadoCorresp) {
		this.estadoCorresp = estadoCorresp;
	}

	public Cidade getCidadeCorresp() {
		return cidadeCorresp;
	}

	public void setCidadeCorresp(Cidade cidadeCorresp) {
		this.cidadeCorresp = cidadeCorresp;
	}

	public TipoLogradouro getTipoLogradouroCorresp() {
		return tipoLogradouroCorresp;
	}

	public void setTipoLogradouroCorresp(TipoLogradouro tipoLogradouroCorresp) {
		this.tipoLogradouroCorresp = tipoLogradouroCorresp;
	}

	public String getTxPessoaEndLograCorresp() {
		return txPessoaEndLograCorresp;
	}

	public void setTxPessoaEndLograCorresp(String txPessoaEndLograCorresp) {
		this.txPessoaEndLograCorresp = txPessoaEndLograCorresp;
	}

	public String getTxPessoaEndNumeroCorresp() {
		return txPessoaEndNumeroCorresp;
	}

	public void setTxPessoaEndNumeroCorresp(String txPessoaEndNumeroCorresp) {
		this.txPessoaEndNumeroCorresp = txPessoaEndNumeroCorresp;
	}

	public String getTxPessoaEndeComplCorresp() {
		return txPessoaEndeComplCorresp;
	}

	public void setTxPessoaEndeComplCorresp(String txPessoaEndeComplCorresp) {
		this.txPessoaEndeComplCorresp = txPessoaEndeComplCorresp;
	}

	public String getTxPessoaEndBairroCorresp() {
		return txPessoaEndBairroCorresp;
	}

	public void setTxPessoaEndBairroCorresp(String txPessoaEndBairroCorresp) {
		this.txPessoaEndBairroCorresp = txPessoaEndBairroCorresp;
	}

	public BigInteger getNuPessoaEndCepCorresp() {
		return nuPessoaEndCepCorresp;
	}

	public void setNuPessoaEndCepCorresp(BigInteger nuPessoaEndCepCorresp) {
		this.nuPessoaEndCepCorresp = nuPessoaEndCepCorresp;
	}

	public String getTxPessoaContato() {
		return txPessoaContato;
	}

	public void setTxPessoaContato(String txPessoaContato) {
		this.txPessoaContato = txPessoaContato;
	}

	public String getTxPessoaDdd() {
		return txPessoaDdd;
	}

	public void setTxPessoaDdd(String txPessoaDdd) {
		this.txPessoaDdd = txPessoaDdd;
	}

	public String getTxPessoaTelefone() {
		return txPessoaTelefone;
	}

	public void setTxPessoaTelefone(String txPessoaTelefone) {
		this.txPessoaTelefone = txPessoaTelefone;
	}

	public String getTxPessoaFaxDdd() {
		return txPessoaFaxDdd;
	}

	public void setTxPessoaFaxDdd(String txPessoaFaxDdd) {
		this.txPessoaFaxDdd = txPessoaFaxDdd;
	}

	public String getTxPessoaFaxTelefone() {
		return txPessoaFaxTelefone;
	}

	public void setTxPessoaFaxTelefone(String txPessoaFaxTelefone) {
		this.txPessoaFaxTelefone = txPessoaFaxTelefone;
	}

	public String getTxPessoaCelDdd() {
		return txPessoaCelDdd;
	}

	public void setTxPessoaCelDdd(String txPessoaCelDdd) {
		this.txPessoaCelDdd = txPessoaCelDdd;
	}

	public String getTxPessoaCelTelefone() {
		return txPessoaCelTelefone;
	}

	public void setTxPessoaCelTelefone(String txPessoaCelTelefone) {
		this.txPessoaCelTelefone = txPessoaCelTelefone;
	}

	public String getTxPessoaEmail() {
		return txPessoaEmail;
	}

	public void setTxPessoaEmail(String txPessoaEmail) {
		this.txPessoaEmail = txPessoaEmail;
	}

	public Date getDtPessoaCad() {
		return dtPessoaCad;
	}

	public void setDtPessoaCad(Date dtPessoaCad) {
		this.dtPessoaCad = dtPessoaCad;
	}

	@JsonIgnore
	public Operador getOperadorCadastrou() {
		return operadorCadastrou;
	}

	public void setOperadorCadastrou(Operador operadorCadastrou) {
		this.operadorCadastrou = operadorCadastrou;
	}

	@JsonIgnore
	public Operador getOperadorAlterou() {
		return operadorAlterou;
	}

	public void setOperadorAlterou(Operador operadorAlterou) {
		this.operadorAlterou = operadorAlterou;
	}

	public Date getDtPessoaAlterou() {
		return dtPessoaAlterou;
	}

	public void setDtPessoaAlterou(Date dtPessoaAlterou) {
		this.dtPessoaAlterou = dtPessoaAlterou;
	}

	@JsonIgnore
	public Operador getOperador() {
		return operador;
	}

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

	@JsonIgnore
	public List<Contribuinte> getContribuintes() {
		return contribuintes;
	}

	@JsonIgnore
    public List<ContribuinteEmpresa> getContribuinteEmpresas() {
		return contribuinteEmpresas;
	}

	public void setContribuinteEmpresas(
			List<ContribuinteEmpresa> contribuinteEmpresas) {
		this.contribuinteEmpresas = contribuinteEmpresas;
	}
	
//	public PessoaJuridica getPessoaJuridica() {
//		return pessoaJuridica;
//	}
//
//	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
//		this.pessoaJuridica = pessoaJuridica;
//	}
	

	public void setContribuintes(List<Contribuinte> contribuintes) {
		this.contribuintes = contribuintes;
	}

	public String getTxTelefoneComDdd() {
		if((getTxPessoaDdd()!=null) && (getTxPessoaTelefone()!=null)){
			return getTxPessoaDdd().concat(getTxPessoaTelefone());
		}else{
			return "";
		}
	}

	public void setTxTelefoneComDdd(String txTelefoneComDdd) {
		if((txTelefoneComDdd!=null) && (!txTelefoneComDdd.equals(""))){
			txPessoaDdd = txTelefoneComDdd.substring(0, 2);
			txPessoaTelefone = txTelefoneComDdd.substring(2, 10);
		}
		this.txTelefoneComDdd = txTelefoneComDdd;
	}

	public String getTxCelularComDdd() {
		if((getTxPessoaCelDdd()!=null) && (getTxPessoaCelTelefone()!=null)){
			return getTxPessoaCelDdd().concat(getTxPessoaCelTelefone());
		}else{
			return "";
		}
	}
	
	public void setTxCelularComDdd(String txCelularComDdd) {
		if((txCelularComDdd!=null) && (!txCelularComDdd.equals(""))){
			txPessoaCelDdd = txCelularComDdd.substring(0, 2);
			txPessoaCelTelefone = txCelularComDdd.substring(2, 10);
		}
		this.txCelularComDdd = txCelularComDdd;
	}
	

	public void setTxCelularFormatado(String txCelularFormatado) {
		if((txCelularComDdd!=null) && (!txCelularComDdd.equals(""))){
			txPessoaCelTelefone = txCelularComDdd.substring(2, 10);
		}
		this.txCelularFormatado = txCelularFormatado;
	}
	
	public String getTxCelularNonoDigitoComDdd() {
		if((getTxPessoaCelDdd()!=null) && (getTxPessoaCelTelefone()!=null)){
			return getTxPessoaCelDdd().concat(getTxPessoaCelTelefone());
		}else{
			return "";
		}
	}


	public String getTxFaxComDdd() {
		if((getTxPessoaFaxDdd()!=null) && (getTxPessoaFaxTelefone()!=null)){
			return getTxPessoaFaxDdd().concat(getTxPessoaFaxTelefone());
		}else{
			return "";
		}
	}

	public void setTxFaxComDdd(String txFaxComDdd) {
		if((txFaxComDdd!=null) && (!txFaxComDdd.equals(""))){
			txPessoaFaxDdd = txFaxComDdd.substring(0, 2);
			txPessoaFaxTelefone = txFaxComDdd.substring(2, 10);
		}
		this.txFaxComDdd = txFaxComDdd;
	}



}
