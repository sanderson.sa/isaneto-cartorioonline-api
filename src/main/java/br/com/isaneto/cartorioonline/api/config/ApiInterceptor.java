package br.com.isaneto.cartorioonline.api.config;

import java.nio.file.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import br.com.isaneto.cartorioonline.api.service.ContribuinteService;

public class ApiInterceptor implements HandlerInterceptor {
	
	public static final String TOKEN_HEADER = "token";
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private ContribuinteService contribuinteService;
	
	@Override
	@Transactional
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		String path = request.getRequestURI();
		if(!path.contains("swagger")) {
			String token = request.getHeader(TOKEN_HEADER);		
			if(StringUtils.isEmpty(token))
				throw new AccessDeniedException("Acesso não autorizado!");
			
			String tokenCorrente = contribuinteService.txPortalParamToken(entityManager);
			
			if(!tokenCorrente.equals(token)) {
				throw new AccessDeniedException(String.format("Token de autenticação incorreto, informe um token válido ou entre em contato com o suporte."));
			}
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception exception) throws Exception {
		if (exception != null)
			exception.printStackTrace();
	}
}