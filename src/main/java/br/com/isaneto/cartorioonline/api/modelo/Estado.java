/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "estado", schema = "public")
public class Estado implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4288523727710952892L;
    @Id
    @SequenceGenerator(name = "estado_id_estado_seq", sequenceName = "estado_id_estado_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estado_id_estado_seq")
    @Column(name = "id_estado", nullable = false)
    private Short idEstado;
    @Size(max = 20)
    @Column(name = "no_estado", length = 20)
    private String noEstado;
    @Size(max = 20)
    @Column(name = "tx_estado_sigla", length = 20)
    private String txEstadoSigla;
    @Size(max = 2)
    @Column(name = "tx_estado_cod_ibge", length = 2)
    private String txEstadoCodIbge;
    @OneToMany(mappedBy = "estado")
    private List<Cidade> cidadeList;
    @Column(name="dt_estado_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore private Date dataUltimaAtualizacao;
    

    public Estado() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public Estado(Short idEstado) {
        this.idEstado = idEstado;
    }

    public Short getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Short idEstado) {
        this.idEstado = idEstado;
    }

    public String getNoEstado() {
        return noEstado;
    }

    public void setNoEstado(String noEstado) {
        this.noEstado = noEstado;
    }

    public String getTxEstadoSigla() {
        return txEstadoSigla;
    }

    public void setTxEstadoSigla(String txEstadoSigla) {
        this.txEstadoSigla = txEstadoSigla;
    }

    public String getTxEstadoCodIbge() {
        return txEstadoCodIbge;
    }

    public void setTxEstadoCodIbge(String txEstadoCodIbge) {
        this.txEstadoCodIbge = txEstadoCodIbge;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEstado == null) ? 0 : idEstado.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (idEstado == null) {
			if (other.idEstado != null)
				return false;
		} else if (!idEstado.equals(other.idEstado))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return noEstado;
    }

    @JsonIgnore
    public List<Cidade> getCidadeList() {
        return cidadeList;
    }

    public void setCidadeList(List<Cidade> cidadeList) {
        this.cidadeList = cidadeList;
    }

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

}
