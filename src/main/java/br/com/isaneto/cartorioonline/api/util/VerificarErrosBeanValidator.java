package br.com.isaneto.cartorioonline.api.util;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


public class VerificarErrosBeanValidator<T>{

	public VerificarErrosBeanValidator() {
	}

	public void validarDados(T entidade) throws Exception{
		String listaErros = "";	
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> erros = validator.validate(entidade);
		if(erros.size()>0){
			
			for(ConstraintViolation<T> erro : erros){				
				listaErros += "Campo: "+erro.getPropertyPath()+"---- Erro: "+erro.getMessage()+"\n";
			}
			throw new Exception(listaErros);
		}	
	}
	
	public void validarDados(List<T> entidades) throws Exception{
		String listaErros = "";	
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		for(T entidade : entidades){
			Set<ConstraintViolation<T>> erros = validator.validate(entidade);
			if(erros.size()>0){
				
				for(ConstraintViolation<T> erro : erros){				
					listaErros += "Campo: "+erro.getPropertyPath()+"---- Erro: "+erro.getMessage()+"\n";
				}
				throw new Exception(listaErros);
			}	
		}
	}
}
