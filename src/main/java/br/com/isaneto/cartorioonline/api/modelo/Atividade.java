package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "atividade",  schema = "mobiliario")
public class Atividade implements Serializable { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 8585900065740902830L;
	@Id
	@SequenceGenerator(name = "atividade_id_atividade_seq", sequenceName = "mobiliario.atividade_id_atividade_seq", schema = "mobiliario", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "atividade_id_atividade_seq")
	@Column(name = "id_atividade", nullable = false)
	private Integer idAtividade;
	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "tx_atividade_cnae", nullable = false, length = 20)
	private String txAtividadeCnae;
	@Column(name = "tx_atividade_cnae_sm", length = 9)
	private String txAtividadeCnaeSm;
	@Size(max = 254)
	@Column(name = "tx_atividade_descricao", length = 254)
	private String txAtividadeDescricao;
	@Column(name = "bo_atividade_ativo")
	private Boolean boAtividadeAtivo = true;
	@Column(name = "bo_atividade_sintetica")
	private Boolean boAtividadeSintetica;
	@Column(name = "id_atividade_pai")
	private Integer idAtividadePai;
	@Column(name = "tx_atividade_versao", length = 4)
	private String txAtividadeVersao;
	@Column(name="dt_atividade_ult_atualizacao")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date dataUltimaAtualizacao;
	
	@OneToMany(mappedBy = "atividade")
	private List<ContribEmpresaAtividade> contribuinteAtividades;
	

	public Atividade() {
	}

	public Atividade(Integer idAtividade) {
		this.idAtividade = idAtividade;
	}

	public Atividade(Integer idAtividade, String txAtividadeCnae) {
		this.idAtividade = idAtividade;
		this.txAtividadeCnae = txAtividadeCnae;
	}
	
	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}

	public Integer getIdAtividade() {
		return idAtividade;
	}

	public void setIdAtividade(Integer idAtividade) {
		this.idAtividade = idAtividade;
	}

	public String getTxAtividadeCnae() {
		return txAtividadeCnae;
	}

	public void setTxAtividadeCnae(String txAtividadeCnae) {
		this.txAtividadeCnae = txAtividadeCnae;
	}

	public String getTxAtividadeDescricao() {
		return txAtividadeDescricao;
	}

	public void setTxAtividadeDescricao(String txAtividadeDescricao) {
		this.txAtividadeDescricao = txAtividadeDescricao;
	}

	public Boolean getBoAtividadeAtivo() {
		return boAtividadeAtivo;
	}

	public void setBoAtividadeAtivo(Boolean boAtividadeAtivo) {
		this.boAtividadeAtivo = boAtividadeAtivo;
	}





	/**
	 * @return the contribuinteAtividades
	 */
	public List<ContribEmpresaAtividade> getContribuinteAtividades() {
		return contribuinteAtividades;
	}

	/**
	 * @param contribuinteAtividades
	 *            the contribuinteAtividades to set
	 */
	public void setContribuinteAtividades(
			List<ContribEmpresaAtividade> contribuinteAtividades) {
		this.contribuinteAtividades = contribuinteAtividades;
	}
	
	@Override
	public String toString() {
		if (this.txAtividadeCnae == null){
			return "";
		}
		else {
		   return this.txAtividadeCnae + "  " + this.txAtividadeDescricao; 
		}
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setTxAtividadeCnaeSm(String txAtividadeCnaeSm) {
		this.txAtividadeCnaeSm = txAtividadeCnaeSm;
	}

	public String getTxAtividadeCnaeSm() {
		return txAtividadeCnaeSm;
	}

	public void setBoAtividadeSintetica(Boolean boAtividadeSintetica) {
		this.boAtividadeSintetica = boAtividadeSintetica;
	}

	public Boolean getBoAtividadeSintetica() {
		return boAtividadeSintetica;
	}

	public void setIdAtividadePai(Integer idAtividadePai) {
		this.idAtividadePai = idAtividadePai;
	}

	public Integer getIdAtividadePai() {
		return idAtividadePai;
	}

	public void setTxAtividadeVersao(String txAtividadeVersao) {
		this.txAtividadeVersao = txAtividadeVersao;
	}

	public String getTxAtividadeVersao() {
		return txAtividadeVersao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAtividade == null) ? 0 : idAtividade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (idAtividade == null) {
			if (other.idAtividade != null)
				return false;
			else if(this == other)
                return true;
			 else 
	            return false;
		} else if (!idAtividade.equals(other.idAtividade))
			return false;
		return true;
	}

/*	public List<NotaFiscalEav> getNotasFiscaisAvulsas() {
		return notasFiscaisAvulsas;
	}

	public void setNotasFiscaisAvulsas(List<NotaFiscalEav> notasFiscaisAvulsas) {
		this.notasFiscaisAvulsas = notasFiscaisAvulsas;
	}*/
    
}
