package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import br.com.isaneto.cartorioonline.api.modelo.dto.PessoaFisicaDto;



/**
 * 
 * @author Bruno
 */
@Entity
@DiscriminatorValue(value="F")
public class PessoaFisica extends Pessoa implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -304424354010545152L;
   
 
    @Size(max = 100)
    @Column(name = "tx_pessoa_fis_mae", length = 100)
    private String txPessoaFisMae;
    
    @Size(max = 100) 
    @Column(name = "tx_pessoa_fis_fantasia", length = 100)
    private String txPessoaFisFantasia;
    
    @Size(max = 100)
    @Column(name = "tx_pessoa_fis_pai", length = 100)
    private String txPessoaFisPai;
    @Column(name = "tx_pessoa_fis_sexo")
    private String txPessoaFisSexo;
    
    @Column(name = "dt_pessoa_fis_nasc")
    @Temporal(TemporalType.DATE)
    private Date dtPessoaFisNasc;
    
    @JoinColumn(name = "id_tp_identificacao_doc", referencedColumnName = "id_tp_identificacao")
    @ManyToOne(fetch=FetchType.EAGER)
    private TipoIdentificacao tipoIdentificacaoDoc;
    
    
    @JoinColumn(name = "id_estado_doc", referencedColumnName = "id_estado")
    @ManyToOne(fetch=FetchType.EAGER)
    private Estado estadoDoc;
    
    
    
    
    @Size(max = 15)
    @Column(name = "tx_pessoa_fis_doc", length = 15)
    private String txPessoaFisDoc;
    
    @Column(name = "dt_pessoa_fis_expe_doc")
    @Temporal(TemporalType.DATE)
    private Date dtPessoaFisExpeDoc;    
    @Size(max = 20)
    @Column(name = "tx_pessoa_fis_org_exp_doc", length = 20)
    private String txPessoaFisOrgExpDoc;        
    @Column(name = "bo_pessoa_fis_estrangeiro")
    private Boolean isPessoaFisEstrangeiro = false;
    
    @Transient
    private String cpf;

    public PessoaFisica() {
    	super();
    }
    
	  public PessoaFisica(PessoaFisicaDto dto) {
		  	this.setCpf(dto.getCpf());
		  	this.setNuPessoaCpfCnpj(new BigInteger(dto.getCpf()));
			this.setNoPessoa(dto.getNome());
			this.setDtPessoaFisNasc(strToDateConvert(dto.getDataNascimento()));
			this.setTxPessoaFisSexo(dto.getSexo());
			//this.setTipoIdentificacaoDoc(dto.getTipoIdentificacaoDoc());
			this.setTxPessoaFisDoc(dto.getNumeroDocumento());
			this.setDtPessoaFisExpeDoc(strToDateConvert(dto.getDataExpedicao()));
			//this.setEstadoDoc(dto.getUfDoc());
			this.setTxPessoaFisOrgExpDoc(dto.getOrgaoExpeditor());
			this.setNuPessoaEndCep(new BigInteger(dto.getCep()));
			//this.setEstado(dto.getUf());
			//this.setCidade(dto.getCidade());
			this.setTxPessoaEndeLogradouro(dto.getEnderecoLogradouro());
			this.setTxPessoaEndNumero(dto.getEnderecoNumero());
			this.setTxPessoaEndeComplemento(dto.getEnderecoComplemento());
			this.setTxPessoaEndBairro(dto.getEnderecoBairro());  
	  }
    

	@PrePersist
    public void prePersist(){
		setTpPessoa("F");
    	setDtPessoaCad(new Date());
    	setIsPessoaFisEstrangeiro(false);
    }

    public String getTxPessoaFisMae() {
        return txPessoaFisMae;
    }

    public void setTxPessoaFisMae(String txPessoaFisMae) {
        this.txPessoaFisMae = txPessoaFisMae;
    }

    public String getTxPessoaFisPai() {
        return txPessoaFisPai;
    }

    public void setTxPessoaFisPai(String txPessoaFisPai) {
        this.txPessoaFisPai = txPessoaFisPai;
    }

    public Date getDtPessoaFisNasc() {
        return dtPessoaFisNasc;
    }

    public void setDtPessoaFisNasc(Date dtPessoaFisNasc) {
        this.dtPessoaFisNasc = dtPessoaFisNasc;
    }

    public String getTxPessoaFisSexo() {
        return txPessoaFisSexo;
    }

    public void setTxPessoaFisSexo(String txPessoaFisSexo) {
        this.txPessoaFisSexo = txPessoaFisSexo;
    }

    public Boolean getIsPessoaFisEstrangeiro() {
        return isPessoaFisEstrangeiro;
    }

    public void setIsPessoaFisEstrangeiro(Boolean isPessoaFisEstrangeiro) {
        this.isPessoaFisEstrangeiro = isPessoaFisEstrangeiro;
    }

    public String getTxPessoaFisDoc() {
        return txPessoaFisDoc;
    }

    public void setTxPessoaFisDoc(String txPessoaFisDoc) {
        this.txPessoaFisDoc = txPessoaFisDoc;
    }

    public Date getDtPessoaFisExpeDoc() {
        return dtPessoaFisExpeDoc;
    }

    public void setDtPessoaFisExpeDoc(Date dtPessoaFisExpeDoc) {
        this.dtPessoaFisExpeDoc = dtPessoaFisExpeDoc;
    }

    public String getTxPessoaFisOrgExpDoc() {
        return txPessoaFisOrgExpDoc;
    }

    public void setTxPessoaFisOrgExpDoc(String txPessoaFisOrgExpDoc) {
        this.txPessoaFisOrgExpDoc = txPessoaFisOrgExpDoc;
    }

   

    public Estado getEstadoDoc() {
        return estadoDoc;
    }

    public void setEstadoDoc(Estado estadoDoc) {
        this.estadoDoc = estadoDoc;
    }

    
  

	public void setCpf(String cpf) {
		setNuPessoaCpfCnpj(new BigInteger(cpf));
		this.cpf = cpf;
	}
	
	@Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdPessoa() != null ? getIdPessoa().hashCode() : 0);
        return hash;
    }
	
  @Override
    public boolean equals(Object object) {
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (!(object instanceof PessoaFisica)) 
			return false;
		
		PessoaFisica other = (PessoaFisica) object;
		if (getIdPessoa() == null) {
			if (other.getIdPessoa() != null)
				return false;
		} else if (!getIdPessoa().equals(other.getIdPessoa()))
			return false;
		return true;
	}

	public String getTxPessoaFisFantasia() {
		return txPessoaFisFantasia;
	}
	
	public void setTxPessoaFisFantasia(String txPessoaFisFantasia) {
		this.txPessoaFisFantasia = txPessoaFisFantasia;
	}
	
	public BigInteger getNuPessoaCpfCnpj() {
		return super.getNuPessoaCpfCnpj();
	}

	public void setNuPessoaCpfCnpj(BigInteger nuPessoaCpfCnpj) {
		super.setNuPessoaCpfCnpj(nuPessoaCpfCnpj);
	}




	public TipoIdentificacao getTipoIdentificacaoDoc() {
		return tipoIdentificacaoDoc;
	}

	private Date strToDateConvert(String str) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date = sdf.parse(str);
			return date;
		} catch (Exception e) {
			return null;
		}
	}


	public void setTipoIdentificacaoDoc(TipoIdentificacao tipoIdentificacaoDoc) {
		this.tipoIdentificacaoDoc = tipoIdentificacaoDoc;
	}
}

