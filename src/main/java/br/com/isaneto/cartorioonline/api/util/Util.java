package br.com.isaneto.cartorioonline.api.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Util {

	
	public static String dateToStr(Date dt) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate date = dt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return date.format(formatter);
	}
	
	public static Date strToDateConvert(String str) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date = sdf.parse(str);
			return date;
		} catch (Exception e) {
			return null;
		}
	}
	
}
