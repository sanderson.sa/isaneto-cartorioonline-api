package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tipo_contribuinte_empresa", schema = "mobiliario")
public class TipoContribuinteEmpresa implements Serializable {

	/**
	 * 
	 */
    private static final long serialVersionUID = -7693668809861918700L;
    @Id
    @SequenceGenerator(name = "tipo_contribuinte_empresa_seq", sequenceName = "mobiliario.tipo_contribuinte_empresa_seq", schema = "mobiliario", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_contribuinte_empresa_seq")
    @Column(name = "id_tp_contrib_empresa", nullable = false)
    private Integer idTipoContribuinteEmpresa;
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "tp_contrib", nullable = false, length = 1)
    private String tipoContribuinteEmpresa;    
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no_tp_contrib", nullable = false, length = 100)
    private String noTipoContribuinteEmpresa;
    @Basic(optional = false)
    @Column(name = "bo_tp_contrib_ativo")
    private boolean boTpContribuinteAtivo;
    @OneToMany(mappedBy = "tipoContribuinte") 
    private List<ContribuinteEmpresa> contribuinteEmpresas;

    
	public TipoContribuinteEmpresa() {
		super();
	}

	public TipoContribuinteEmpresa(TipoContribuinteEmpresa editTipoContribEmpresa) {
		super();
		this.boTpContribuinteAtivo = editTipoContribEmpresa.boTpContribuinteAtivo;
		this.idTipoContribuinteEmpresa = editTipoContribEmpresa.idTipoContribuinteEmpresa;
		this.noTipoContribuinteEmpresa = editTipoContribEmpresa.noTipoContribuinteEmpresa;
		this.tipoContribuinteEmpresa = editTipoContribEmpresa.tipoContribuinteEmpresa;
	}

	public Integer getIdTipoContribuinteEmpresa() {
		return idTipoContribuinteEmpresa;
	}

	public void setIdTipoContribuinteEmpresa(Integer idTipoContribuinteEmpresa) {
		this.idTipoContribuinteEmpresa = idTipoContribuinteEmpresa;
	}

	public String getTipoPessoa() {
		return tipoContribuinteEmpresa;
	}

	public void setTipoPessoa(String tipoContribuinteEmpresa) {
		this.tipoContribuinteEmpresa = tipoContribuinteEmpresa;
	}

	public List<ContribuinteEmpresa> getContribuinteEmpresas() {
		return contribuinteEmpresas;
	}

	public void setContribuinteEmpresas(
			List<ContribuinteEmpresa> contribuinteEmpresas) {
		this.contribuinteEmpresas = contribuinteEmpresas;
	}
	

	public boolean isBoTpContribuinteAtivo() {
		return boTpContribuinteAtivo;
	}

	public void setBoTpContribuinteAtivo(boolean boTpContribuinteAtivo) {
		this.boTpContribuinteAtivo = boTpContribuinteAtivo;
	}
	

	public String getNoTipoContribuinteEmpresa() {
		return noTipoContribuinteEmpresa;
	}

	public void setNoTipoContribuinteEmpresa(String noTipoContribuinteEmpresa) {
		this.noTipoContribuinteEmpresa = noTipoContribuinteEmpresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((idTipoContribuinteEmpresa == null) ? 0	: idTipoContribuinteEmpresa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoContribuinteEmpresa other = (TipoContribuinteEmpresa) obj;
		if (idTipoContribuinteEmpresa == null) {
			if (other.idTipoContribuinteEmpresa != null)
				return false;
		} else if (!idTipoContribuinteEmpresa.equals(other.idTipoContribuinteEmpresa))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.noTipoContribuinteEmpresa;
	}

	public String getTipoContribuinteEmpresa() {
		return tipoContribuinteEmpresa;
	}

	public void setTipoContribuinteEmpresa(String tipoContribuinteEmpresa) {
		this.tipoContribuinteEmpresa = tipoContribuinteEmpresa;
	}
        
}
