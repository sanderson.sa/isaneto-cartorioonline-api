package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


/**
 * 
 * @author Bruno
 */
@Entity
@Table(name = "contrib_empresa_regime_tributacao", schema = "mobiliario")
public class ContribEmpresaRegimeTributacao implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 200508621566781859L;
    @Id
    @SequenceGenerator(name = "contrib_empresa_regime_tributacao_id_contrib_empresa_regime_seq", sequenceName = "mobiliario.contrib_empresa_regime_tributacao_id_contrib_empresa_regime_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contrib_empresa_regime_tributacao_id_contrib_empresa_regime_seq")
    @Column(name = "id_contrib_empresa_regime", nullable = false)
    private Long idContribEmpresaRegime;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_operador_incluiu", referencedColumnName = "id_operador", nullable = false)
    private Operador operador;
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "dt_contrib_empresa_regime_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicio;
    @Basic(optional = false)
    @Column(name = "dt_contrib_empresa_regime_receita")
    @Temporal(TemporalType.DATE)
    private Date dataInicioReceita;
    @Column(name = "dt_contrib_empresa_regime_fim")
    @Temporal(TemporalType.DATE)
    private Date dataTermino;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dt_contrib_empresa_regime_incluiu")
    @Temporal(TemporalType.DATE)
    private Date dataIncluiu;
    @JoinColumn(name = "id_regime_tributacao", referencedColumnName = "id_regime_tributacao", nullable = false)
    @ManyToOne(optional = false)
    private RegimeTributacao regimeTributacao;
    @JoinColumn(name = "id_contribuinte", referencedColumnName = "id_contribuinte", nullable = false)
    @ManyToOne(optional = false)
    private ContribuinteEmpresa contribuinteEmpresa;
    

    public ContribEmpresaRegimeTributacao() {
    }

    public ContribEmpresaRegimeTributacao(Long idContribEmpresaRegime) {
        this.idContribEmpresaRegime = idContribEmpresaRegime;
    }

    public ContribEmpresaRegimeTributacao(Long idContribEmpresaRegime,
            Date dataInicio, Operador operador,
            Date dataIncluiu) {
        this.idContribEmpresaRegime = idContribEmpresaRegime;
        this.dataInicio = dataInicio;
        this.operador = operador;
        this.dataIncluiu = dataIncluiu;
    }

    public ContribEmpresaRegimeTributacao(ContribEmpresaRegimeTributacao regime) {
		this.operador = regime.getOperador();
		this.idContribEmpresaRegime = regime.getIdContribEmpresaRegime();
		this.dataIncluiu = regime.getDataIncluiu();
		this.dataTermino = regime.getDataTermino();
		this.dataInicio = regime.getDataInicio();
		this.contribuinteEmpresa = regime.getContribuinteEmpresa();
		this.regimeTributacao = regime.getRegimeTributacao();
	}

	public Long getIdContribEmpresaRegime() {
		return idContribEmpresaRegime;
	}

	public void setIdContribEmpresaRegime(Long idContribEmpresaRegime) {
		this.idContribEmpresaRegime = idContribEmpresaRegime;
	}

	public Operador getOperador() {
		return operador;
	}

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public Date getDataInicioReceita() {
		return dataInicioReceita;
	}

	public void setDataInicioReceita(Date dataInicioReceita) {
		this.dataInicioReceita = dataInicioReceita;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

	public Date getDataIncluiu() {
		return dataIncluiu;
	}

	public void setDataIncluiu(Date dataIncluiu) {
		this.dataIncluiu = dataIncluiu;
	}

	public RegimeTributacao getRegimeTributacao() {
		return regimeTributacao;
	}

	public void setRegimeTributacao(RegimeTributacao regimeTributacao) {
		this.regimeTributacao = regimeTributacao;
	}

	public ContribuinteEmpresa getContribuinteEmpresa() {
		return contribuinteEmpresa;
	}

	public void setContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa) {
		this.contribuinteEmpresa = contribuinteEmpresa;
	}

    @Override
    public String toString() {
        return "ContribEmpresaRegimeTributacao[ idContribEmpresaRegime="
                + idContribEmpresaRegime + " ]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idContribEmpresaRegime == null) ? 0 : idContribEmpresaRegime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContribEmpresaRegimeTributacao other = (ContribEmpresaRegimeTributacao) obj;
		if (idContribEmpresaRegime == null) {
			if (other.idContribEmpresaRegime != null)
				return false;
			else if(this == other)
                return true;
			 else 
	             return false;
		} else if (!idContribEmpresaRegime.equals(other.idContribEmpresaRegime))
			return false;
		return true;
	}

}
