package br.com.isaneto.cartorioonline.api.repository;

import br.com.isaneto.cartorioonline.api.modelo.PessoaJuridica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaJuridicaRepository extends JpaRepository<PessoaJuridica, Long> {

}
