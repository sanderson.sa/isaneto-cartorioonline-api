/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "bairro", schema = "public")
public class Bairro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8358891568162550821L;
    @Id
    @SequenceGenerator(name = "bairro_id_bairro_seq", sequenceName = "bairro_id_bairro_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bairro_id_bairro_seq")
    @Column(name = "id_bairro", nullable = false)
    private Integer idBairro;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no_bairro", nullable = false, length = 100)
    private String noBairro;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade cidade;

    public Bairro() {
    }

    public Bairro(Integer idBairro) {
        this.idBairro = idBairro;
    }

    public Bairro(Integer idBairro, String noBairro) {
        this.idBairro = idBairro;
        this.noBairro = noBairro;
    }

    public Integer getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(Integer idBairro) {
        this.idBairro = idBairro;
    }

    public String getNoBairro() {
        return noBairro;
    }

    public void setNoBairro(String noBairro) {
        this.noBairro = noBairro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade idCidade) {
        this.cidade = idCidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBairro != null ? idBairro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
    	//Flavio Viegas
    	if (this == object)
			return true;
		if (object == null)
			return false;
		 if (object.getClass().getName().toLowerCase().contains("$")) {
			int index = object.getClass().getName().indexOf("_$$_");
			String outherNameClazz = object.getClass().getName().substring(0, index);
			if (!this.getClass().getName().equals(outherNameClazz)) {
				return false;
			}
		 }
        if (!(object instanceof Bairro)) {
            return false;
        }
        Bairro other = (Bairro) object;
        //Mudan�a de idBairro para getIdBairro
        if ((this.getIdBairro() == null && other.getIdBairro() != null) || (this.getIdBairro() != null && !this.getIdBairro().equals(other.getIdBairro()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "publico.Bairro[ idBairro=" + idBairro + " ]";
    }
}
