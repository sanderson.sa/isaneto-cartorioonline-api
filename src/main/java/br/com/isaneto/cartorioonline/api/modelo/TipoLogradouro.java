/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "tipo_logradouro", schema = "imobiliario")
public class TipoLogradouro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1075730500637719649L;
    @Id
    @SequenceGenerator(name = "tipo_logradouro_id_tp_logradouro_seq", sequenceName="imobiliario.tipo_logradouro_id_tp_logradouro_seq", schema = "imobiliario", allocationSize = 1 )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_logradouro_id_tp_logradouro_seq")	   
    @Column(name = "id_tp_logradouro")
    private Integer idTpLogradouro;
    //@NotNull
    //@Size(min = 1, max = 20)
    @Column(name = "no_tp_logradouro", length = 20)
    private String noTpLogradouro;
   
    @Column(name="dt_tp_logra_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date dataUltimaAtualizacao;

    public TipoLogradouro() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public TipoLogradouro(Integer idTpLogradouro) {
        this.idTpLogradouro = idTpLogradouro;
    }

    public TipoLogradouro(Integer idTpLogradouro, String noTpLogradouro, Date dataUltimaAtualizacao) {
        this.idTpLogradouro = idTpLogradouro;
        this.noTpLogradouro = noTpLogradouro;
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
    }

    public Integer getIdTpLogradouro() {
        return idTpLogradouro;
    }

    public void setIdTpLogradouro(Integer idTpLogradouro) {
        this.idTpLogradouro = idTpLogradouro;
    }

    public String getNoTpLogradouro() {
        return noTpLogradouro;
    }

    public void setNoTpLogradouro(String noTpLogradouro) {
        this.noTpLogradouro = noTpLogradouro;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTpLogradouro == null) ? 0 : idTpLogradouro.hashCode());
		return result;
	}

  /*  @Override
    public boolean equals(Object object) {
    	if (this == object)
			return true;
		if (object == null)
			return false;
		 if (object.getClass().getName().toLowerCase().contains("$")) {
			int index = object.getClass().getName().indexOf("_$$_");
			String outherNameClazz = object.getClass().getName().substring(0, index);
			if (!this.getClass().getName().equals(outherNameClazz)) {
				return false;
			}
		 }
 			
        if (!(object instanceof TipoLogradouro)) {
            return false;
        }
        TipoLogradouro other = (TipoLogradouro) object;
        if ((this.getIdTpLogradouro() == null && other.getIdTpLogradouro() != null) || (this.getIdTpLogradouro() != null && !this.getIdTpLogradouro().equals(other.getIdTpLogradouro()))) {
            return false;
        }
		
        return true;
    }*/

    @Override
    public String toString() {
        return "br.com.isaneto.prefeitura.modelo.publico.TipoLogradouro[ idTpLogradouro=" + idTpLogradouro + " ]";
    }

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoLogradouro other = (TipoLogradouro) obj;
		if (idTpLogradouro == null) {
			if (other.idTpLogradouro != null)
				return false;
		} else if (!idTpLogradouro.equals(other.idTpLogradouro))
			return false;
		return true;
	}
}
