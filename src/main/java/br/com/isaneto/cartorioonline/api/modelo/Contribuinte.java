/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
//import br.com.isaneto.prefeitura.modelo.transito.ContribuinteTransito;

/**
 * 
 * @author Simeao
 */
@Entity
@Table(name = "contribuinte", schema = "mobiliario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_pessoa", "tp_contribuinte", "tx_contrib_insc_municipal_sem_masc"})})
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contribuinte implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8992406298830342988L;
    @Id
    @SequenceGenerator(name = "contribuinte_id_contribuinte_seq", sequenceName = "mobiliario.contribuinte_id_contribuinte_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contribuinte_id_contribuinte_seq")
    @Column(name = "id_contribuinte", nullable = false)
    private Long idContribuinte;
    @NotNull
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id_pessoa")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Pessoa pessoa;    
    @NotNull
    @Size(max = 1)
    @Column(name = "tp_contribuinte", nullable = false)
    private String tipoContribuinte;
   // @NotNull
    @Size(max = 20)
    @Column(name = "tx_contrib_insc_municipal_sem_masc")
    private String inscricaoMunicipalSemMascara;
    //@NotNull
    @Size(max = 30)
    @Column(name = "tx_contrib_insc_municipal_com_masc")
    private String inscricaoMunicipalComMascara;   
    
    @NotNull
    @JoinColumn(name = "id_operador_incluiu", referencedColumnName = "id_operador", nullable=false)
    @ManyToOne
    private Operador idOperadorIncluiu;
    @Column(name = "dt_contrib_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataContribCadastro;        
    
    @JoinColumn(name = "id_operador_alterou", referencedColumnName = "id_operador")
    @ManyToOne
    private Operador operadorAlterou;    
    @Column(name = "dt_contrib_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataContribUltimaAtualizacao;
    @OneToOne    
    @JoinColumn(name="id_contribuinte")   
    private ContribuinteEmpresa contribuinteEmpresa;
    
    @PrePersist
    public void prePersist(){
    	dataContribCadastro = new Date();
    }
    
    public Contribuinte(Long idContribuinte, Pessoa idPessoa, String tipoContribuinte,
            String inscricaoMunicipalSemMascara, String inscricaoMunicipalComMascara) {
        this.idContribuinte = idContribuinte;
        this.pessoa = idPessoa;
        this.tipoContribuinte = tipoContribuinte;
        this.inscricaoMunicipalSemMascara = inscricaoMunicipalSemMascara;
        this.inscricaoMunicipalComMascara = inscricaoMunicipalComMascara;
    }
    
      
	public Long getIdContribuinte() {
		return idContribuinte;
	}

	public void setIdContribuinte(Long idContribuinte) {
		this.idContribuinte = idContribuinte;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getTipoContribuinte() {
		return tipoContribuinte;
	}

	public void setTipoContribuinte(String tipoContribuinte) {
		this.tipoContribuinte = tipoContribuinte;
	}

	public String getInscricaoMunicipalSemMascara() {
		return inscricaoMunicipalSemMascara;
	}

	public void setInscricaoMunicipalSemMascara(String inscricaoMunicipalSemMascara) {
		this.inscricaoMunicipalSemMascara = inscricaoMunicipalSemMascara;
	}

	public String getInscricaoMunicipalComMascara() {
		return inscricaoMunicipalComMascara;
	}

	public void setInscricaoMunicipalComMascara(String inscricaoMunicipalComMascara) {
		this.inscricaoMunicipalComMascara = inscricaoMunicipalComMascara;
	}

	@JsonIgnore
	public Operador getIdOperadorIncluiu() {
		return idOperadorIncluiu;
	}

	public void setIdOperadorIncluiu(Operador idOperadorIncluiu) {
		this.idOperadorIncluiu = idOperadorIncluiu;
	}

	public Date getDataContribCadastro() {
		return dataContribCadastro;
	}

	public void setDataContribCadastro(Date dataContribCadastro) {
		this.dataContribCadastro = dataContribCadastro;
	}

	@JsonIgnore
	public Operador getOperadorAlterou() {
		return operadorAlterou;
	}

	public void setOperadorAlterou(Operador idOperadorAlterou) {
		this.operadorAlterou = idOperadorAlterou;
	}

	public Date getDataContribUltimaAtualizacao() {
		return dataContribUltimaAtualizacao;
	}

	public void setDataContribUltimaAtualizacao(Date dataContribUltimaAtualizacao) {
		this.dataContribUltimaAtualizacao = dataContribUltimaAtualizacao;
	}

	public Contribuinte() {
    }

    public Contribuinte(Long idContribuinte) {
        this.idContribuinte = idContribuinte;
    }
    
    public Contribuinte(Long inscMunicipal, String tipoContribuinte, PessoaJuridica pj, Operador operador) {
    	this.setIdOperadorIncluiu(operador);
    	this.setDataContribCadastro(new Date());
    	this.setDataContribUltimaAtualizacao(new Date());
    	this.setInscricaoMunicipalComMascara(new DecimalFormat("000000").format(inscMunicipal));
    	this.setInscricaoMunicipalSemMascara(new DecimalFormat("000000").format(inscMunicipal));
    	this.setPessoa(pj);
    	this.setTipoContribuinte(tipoContribuinte);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idContribuinte == null) ? 0 : idContribuinte.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contribuinte other = (Contribuinte) obj;
		if (idContribuinte == null) {
			if (other.idContribuinte != null)
				return false;
		} else if (!idContribuinte.equals(other.idContribuinte))
			return false;
		return true;
	}

	public ContribuinteEmpresa getContribuinteEmpresa() {
		return contribuinteEmpresa;
	}

	public void setContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa) {
		this.contribuinteEmpresa = contribuinteEmpresa;
	}



	
        
}
