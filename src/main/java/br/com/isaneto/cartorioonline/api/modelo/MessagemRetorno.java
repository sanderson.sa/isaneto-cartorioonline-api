package br.com.isaneto.cartorioonline.api.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessagemRetorno {
	private Date data = new Date();
	private boolean sucesso = false;
	private String menssagem;
	private ListaMensagemRetorno listaMensagemRetorno;

	public static class ListaMensagemRetorno {
		private List<TcMensagemRetorno> mensagensRetorno = new ArrayList<TcMensagemRetorno>();

		public List<TcMensagemRetorno> getMensagensRetorno() {
			return mensagensRetorno;
		}

		public void setMensagensRetorno(List<TcMensagemRetorno> mensagensRetorno) {
			this.mensagensRetorno = mensagensRetorno;
		}

	}

	public static class TcMensagemRetorno {
		private String campo;
		private String mensagem;

		public String getCampo() {
			return campo;
		}

		public void setCampo(String campo) {
			this.campo = campo;
		}

		public String getMensagem() {
			return mensagem;
		}

		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public boolean isSucesso() {
		return sucesso;
	}

	public void setSucesso(boolean sucesso) {
		this.sucesso = sucesso;
	}

	public ListaMensagemRetorno getListaMensagemRetorno() {
		return listaMensagemRetorno;
	}

	public void setListaMensagemRetorno(ListaMensagemRetorno listaMensagemRetorno) {
		this.listaMensagemRetorno = listaMensagemRetorno;
	}

	public String getMenssagem() {
		return menssagem;
	}

	public void setMenssagem(String menssagem) {
		this.menssagem = menssagem;
	}

}
