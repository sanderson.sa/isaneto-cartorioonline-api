package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.TipoIdentificacao;

@Repository
public interface TipoIdentificacaoRepository extends JpaRepository<TipoIdentificacao, Short> {

}
