package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 * 
 * @author Bruno
 */
@Entity
@Table(name = "cidade", schema = "public")
public class Cidade implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4293138463260050525L;
    @Id
    @SequenceGenerator(name = "cidade_id_cidade_seq", sequenceName = "cidade_id_cidade_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cidade_id_cidade_seq")
    @Column(name = "id_cidade", nullable = false)
    private Integer idCidade;
    @Size(max = 100)
    @Column(name = "no_cidade", length = 100)
    private String noCidade;
    @Size(max = 7)
    @Column(name = "tx_cidade_cod_ibge", length = 7)
    private String txCidadeCodIbge;
    @Size(max = 7)
    @Column(name = "tx_cidade_cod_siafi", length = 5)
    private String codigoSiafi;
    @Size(max = 5)
    @Column(name = "tx_cidade_cod_sinpas", length = 5)
    private String codigoSinpas;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_estado", referencedColumnName="id_estado")
    private Estado estado;
    
    @OneToMany(mappedBy = "cidade")
    private List<Bairro> bairroList;
    @Column(name="dt_cidade_ulti_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date dataUltimaAtualizacao;

    public Cidade() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public Cidade(Integer idCidade) {
        this.idCidade = idCidade;
    }

    public Integer getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Integer idCidade) {
        this.idCidade = idCidade;
    }

    public String getNoCidade() {
        return noCidade;
    }

    public void setNoCidade(String noCidade) {
        this.noCidade = noCidade;
    }

    public String getTxCidadeCodIbge() {
        return txCidadeCodIbge;
    }

    public void setTxCidadeCodIbge(String txCidadeCodIbge) {
        this.txCidadeCodIbge = txCidadeCodIbge;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Bairro> getBairroList() {
        return bairroList;
    }

    public void setBairroList(List<Bairro> bairroList) {
        this.bairroList = bairroList;
    }
    
    public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public String getCodigoSiafi() {
		return codigoSiafi;
	}

	public void setCodigoSiafi(String codigoSiafi) {
		this.codigoSiafi = codigoSiafi;
	}

	public String getCodigoSinpas() {
		return codigoSinpas;
	}

	public void setCodigoSinpas(String codigoSinpas) {
		this.codigoSinpas = codigoSinpas;
	}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCidade != null ? idCidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Cidade)) {
            return false;
        }
        Cidade other = (Cidade) object;
        if ((this.idCidade == null && other.idCidade != null)
                || (this.idCidade != null && !this.idCidade.equals(other.idCidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.isaneto.prefeitura.modelo.publico.Cidade[ idCidade=" + idCidade + " ]";
    }

	
}
