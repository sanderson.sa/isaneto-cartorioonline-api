package br.com.isaneto.cartorioonline.api.exception;

public final class ApiException extends RuntimeException {

    public ApiException() {
        super();
    }

    public ApiException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ApiException(final String message) {
        super(message);
    }

    public ApiException(final Throwable cause) {
        super(cause);
    }

}