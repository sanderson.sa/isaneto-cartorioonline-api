package br.com.isaneto.cartorioonline.api.modelo.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ContribuinteDTO {

	@NotNull(message="CNPJ é um campo obrigatório")
	@Size(min=11, max=14, message="CNPJ é um campo obrigatório de tamanho 11-14, sem formatação, apenas numero")
	private String cpfCnpj;
	
	@NotNull(message="INSCRICAO é um campo obrigatório")
	@Size(max=10, message="INSCRICAO é um campo obrigatório com tamanho máximo de 10 caracteres")
	private String inscricao;
	
	//@NotNull(message="TIPO ESTABELECIMENTO é um campo obrigatório")
	//@Size(min=1, max=1, message="TIPO ESTABELECIMENTO é um campo obrigatório com tamanho 1")
	private String tipoEstabelecimento; // apenas para PJ
	
	@NotNull(message="ATIVO é um campo obrigatório")
	@Size(min=1, max=1, message="ATIVO é um campo obrigatório com tamanho 1")
	private String ativo;
	
	@Pattern(regexp = "\\d{2}-\\d{2}-\\d{4}", message = "Formato de data dd-mm-aaaa")
	@NotNull(message="DATA DE CADASTRO é um campo obrigatório")
	private String dataCadastro;
	
	@NotNull(message="UF é um campo obrigatório")
	@Size(min=2, max=2, message="UF é um campo obrigatório com tamanho 2")
	private String uf;
	
	@NotNull(message="CIDADE é um campo obrigatório")
	private String cidade;
	
	@NotNull(message="CEP é um campo obrigatório")
	@Size(min=8, max=8, message="CEP é um campo obrigatório com tamanho 8 sem mascará")
	private String cep;
	
	@NotEmpty(message="ENDEREÇO LOGRADOURO é um campo obrigatório")
	private String enderecoLogradouro;
	
	@NotEmpty(message="NUMERO LOGRADOURO é um campo obrigatório")
	private String enderecoNumero;
	
	private String enderecoComplemento;
	
	@NotEmpty(message="BAIRRO LOGRADOURO é um campo obrigatório")
	private String enderecoBairro;
	
	@NotNull(message="NATUREZA JURIDICA é um campo obrigatório")
	@Size(max=5, message="NATUREZA JURIDICA é um campo obrigatório. Máx. 5 caracteres sem mascaras")
	private String naturezaJuridica;
	
	
	private String ddd;
	private String telefone;
	private String dddCelular;
	private String celular;
	private String email;
	
	@NotNull(message="ATIVIDADES CNAE é um campo obrigatório")
	private List<AtividadeCnaeDTO> atividadeCnae;
	
	@NotNull(message="REGIMES DE TRIBUTAÇÃO é um campo obrigatório")
	private List<RegimeTributacaoDTO> regimeTributacao;
	
	@NotNull(message="SITUAÇÃO TRIBUTÁVEL é um campo obrigatório")
	private List<SituacaoTributariaDTO> situacaoTributaria;
	
	public ContribuinteDTO() {

	}
}
