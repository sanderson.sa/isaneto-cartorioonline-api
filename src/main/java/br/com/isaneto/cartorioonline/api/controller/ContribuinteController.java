package br.com.isaneto.cartorioonline.api.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import br.com.isaneto.cartorioonline.api.converter.ContribuinteConverter;
import br.com.isaneto.cartorioonline.api.exception.ApiException;
import br.com.isaneto.cartorioonline.api.repository.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.isaneto.cartorioonline.api.modelo.ContribEmpresaAtividade;
import br.com.isaneto.cartorioonline.api.modelo.ContribEmpresaRegimeTributacao;
import br.com.isaneto.cartorioonline.api.modelo.ContribEmpresaSituacaoTributacao;
import br.com.isaneto.cartorioonline.api.modelo.Contribuinte;
import br.com.isaneto.cartorioonline.api.modelo.ContribuinteEmpresa;
import br.com.isaneto.cartorioonline.api.modelo.MessagemRetorno;
import br.com.isaneto.cartorioonline.api.modelo.Pessoa;
import br.com.isaneto.cartorioonline.api.modelo.PessoaFisica;
import br.com.isaneto.cartorioonline.api.modelo.PessoaJuridica;
import br.com.isaneto.cartorioonline.api.modelo.dto.AtividadeCnaeDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.ContribuinteDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.PessoaFisicaDto;
import br.com.isaneto.cartorioonline.api.modelo.dto.RegimeTributacaoDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.RepresentanteLegalCpfDto;
import br.com.isaneto.cartorioonline.api.modelo.dto.SituacaoTributariaDTO;
import br.com.isaneto.cartorioonline.api.service.ContribuinteService;
import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping("/contribuinte")
@Slf4j
public class ContribuinteController {
	
	@Autowired
	private ContribuinteService contribuinteService;
		
	@Autowired
	private ContribuinteEmpresaRepository contribuinteEmpresaRepository;

	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;
		
	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private ContribEmpresaAtividadeRepository contribEmpresaAtividadeRepository;
	
	@Autowired
	private ContribEmpresaRegimeTributacaoRepository contribEmpresaRegimeTributacaoRepository;
	
	@Autowired
	private ContribEmpresaSituacaoTributacaoRepository contribEmpresaSituacaoTributacaoRepository;

	@Autowired
	private ContribuinteConverter contribuinteConverter;
	
	
	@RequestMapping(path="/pj/{cnpj}/{inscricao}",method=RequestMethod.GET)
	public ResponseEntity<?> getContribuintePJ(
			@PathVariable String cnpj,
			@PathVariable String inscricao) {

		Optional<Pessoa> p1 = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cnpj.replaceAll("\\D", "")));
		if(!p1.isPresent())
			throw new EntityNotFoundException(String.format("Pessoa não localizada CNPJ:: %s ", cnpj));

		Optional<Contribuinte> contribuinte = contribuinteService.pegarPorCpfCnpjInsc(inscricao,new BigInteger(cnpj.replaceAll("\\D", "")));
		if(!contribuinte.isPresent())
			throw new EntityNotFoundException(String.format("Contribuinte não localizado CNPJ :: %s Inscrição:: %s ", cnpj, inscricao));

		ContribuinteDTO retorno = contribuinteConverter.paraDTO(contribuinte.get());

		return new ResponseEntity<>(retorno, HttpStatus.OK);
	}
	
	
	@RequestMapping(path="/pf/{cpf}/{inscricao}",method=RequestMethod.GET)
	public ResponseEntity<?> getContribuintePF(
			@PathVariable String cpf,
			@PathVariable String inscricao) {
		
		Optional<Pessoa> p1 = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cpf.replaceAll("\\D", "")));
		if(!p1.isPresent()) {
			throw new EntityNotFoundException(String.format("Pessoa não localizada CPF:: %s ", cpf));
		}
		
		Optional<Contribuinte> contribuinte = contribuinteService.pegarPorCpfCnpjInsc(inscricao,new BigInteger(cpf.replaceAll("\\D", "")));
		if(!contribuinte.isPresent())
			throw new EntityNotFoundException(String.format("Contribuinte não localizado CPF :: %s Inscrição:: %s ", cpf, inscricao));

		ContribuinteDTO retorno = contribuinteConverter.paraDTO(contribuinte.get());

		return new ResponseEntity<>(retorno, HttpStatus.OK);
	}

	@PostMapping("/pj")
	ResponseEntity<?> salvarPJ(
			@Valid @RequestBody ContribuinteDTO dto,
			BindingResult result) {

		//Valida Estrutura DTO
		if(result.hasErrors()) {
			contribuinteService.getErrosValidacaoRequest(result);
		}

		Contribuinte contribuinte = contribuinteService.convertDtoParaContribuintePJ(dto);
		return new ResponseEntity<>(contribuinte, HttpStatus.CREATED);
	}
	
	@PostMapping("/pf")
	ResponseEntity<?> salvarPF(
			@Valid @RequestBody ContribuinteDTO dto,
			BindingResult result) {

		//Valida Estrutura DTO
		if(result.hasErrors()) {
			contribuinteService.getErrosValidacaoRequest(result);
		}
		
		//Salvar Registro Contribuinte
		Contribuinte contribuinte = contribuinteService.convertDtoParaContribuintePF(dto);
		return new ResponseEntity<>(contribuinte, HttpStatus.CREATED);
		
	}
	
	@RequestMapping(path="/contribuinte/{cnpj}",method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteById(@PathVariable String cnpj) {
		try {
			Optional<Pessoa> p1 = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cnpj.replaceAll("\\D", "")));
			if(p1.isPresent()) {
				Pessoa pessoa = p1.get();
				Optional<Contribuinte> contribuinte = contribuinteService.pegarPorPessoa(pessoa);
				if(contribuinte.isPresent()) {
					Optional<ContribuinteEmpresa> contribEmpresa = contribuinteEmpresaRepository.findByContribuinte(contribuinte.get());
					//List ContribEmpresaAtividade
					List<ContribEmpresaAtividade> listaContribEmpresaAtividade = contribEmpresaAtividadeRepository.findByContribuinteEmpresa(contribEmpresa.get());
					if(!listaContribEmpresaAtividade.isEmpty()) {
						for (ContribEmpresaAtividade item : listaContribEmpresaAtividade) {
							contribEmpresaAtividadeRepository.delete(item);
						}
					}
					//List ContribEmpresaRegimeTributacao
					List<ContribEmpresaRegimeTributacao> listaContribEmpresaRegimeTributacao = contribEmpresaRegimeTributacaoRepository.findByContribuinteEmpresa(contribEmpresa.get());
					if(!listaContribEmpresaRegimeTributacao.isEmpty()) {
						for (ContribEmpresaRegimeTributacao item : listaContribEmpresaRegimeTributacao) {
							contribEmpresaRegimeTributacaoRepository.delete(item);
						}
					}					
					//List ContribEmpresaSituacaoTributacao
					List<ContribEmpresaSituacaoTributacao> listaContribEmpresaSituacaoTributacao = contribEmpresaSituacaoTributacaoRepository.findByContribuinteEmpresa(contribEmpresa.get());
					if(!listaContribEmpresaSituacaoTributacao.isEmpty()) {
						for (ContribEmpresaSituacaoTributacao item : listaContribEmpresaSituacaoTributacao) {
							contribEmpresaSituacaoTributacaoRepository.delete(item);
						}	
					}
					contribuinteEmpresaRepository.delete(contribEmpresa.get());
					contribuinteService.remove(contribuinte.get());
					pessoaRepository.delete(pessoa);
					return ResponseEntity.noContent().build();
				}else {
					return ResponseEntity.notFound().build();
				}			
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
}
