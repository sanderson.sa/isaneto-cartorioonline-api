package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Bruno
 */
@Entity
@Table(name = "situcao_tributaria", schema = "mobiliario")
@NamedQueries({
    @NamedQuery(name = "SituacaoTributaria.findAll", query = "SELECT r FROM SituacaoTributaria r"),
    @NamedQuery(name = "SituacaoTributaria.findByIdSitTributacao", query = "SELECT r FROM SituacaoTributaria r WHERE r.idSitTributacao = :idSitTributacao"),
    @NamedQuery(name = "SituacaoTributaria.findByAllAtiva", query = "SELECT r FROM SituacaoTributaria r WHERE r.boSitTributacaoAtivo = true"),
    @NamedQuery(name = "SituacaoTributaria.findSituacaiTributariaVigenteContribuinte", query = "SELECT r FROM SituacaoTributaria r JOIN r.contribSituacaoTributacoes s WHERE r.boSitTributacaoAtivo = true and s.contribuinteEmpresa = :contribuinte and s.dataInicioSituacaoTributacao <= :data and (case when s.dataFimSituacaoTributacao IS NULL then current_date else s.dataFimSituacaoTributacao end) >= :data "),
    @NamedQuery(name = "SituacaoTributaria.findSituacaoTributariaPorRegimeTributacao", query = "SELECT s FROM SituacaoTributaria s JOIN s.listRegimeTributacao r WHERE r = :regimeTributacao and s.boSitTributacaoAtivo = true  ")})    
public class SituacaoTributaria implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4593512429763177701L;
    @Id
    @Column(name = "id_sit_tributacao", nullable = false)
    private Integer idSitTributacao;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tx_sit_tributacao", nullable = false, length = 100)
    private String txSitTributacao;
    @NotNull
    @Column(name = "bo_sit_tributacao_ativo", nullable = false)
    private boolean boSitTributacaoAtivo;
    @NotNull
    @Column(name = "bo_tributa_iss", nullable = false)
    private Boolean tributaIss;
    @Column(name="dt_sit_trib_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date dataUltimaAtualizacao;
    @OneToMany(mappedBy = "situacaoTributacao")
    private List<ContribEmpresaSituacaoTributacao> contribSituacaoTributacoes;
   
    @ManyToMany
	@JoinTable(name="regime_trib_sit_tributaria", schema = "mobiliario", 
			   joinColumns= @JoinColumn(name="id_sit_tributacao", referencedColumnName="id_sit_tributacao"), 
			   inverseJoinColumns = @JoinColumn(name="id_regime_tributacao", referencedColumnName="id_regime_tributacao"))
	private List<RegimeTributacao> listRegimeTributacao; 
    
    public SituacaoTributaria() {
    }

	@PrePersist
	@PreUpdate
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
    public SituacaoTributaria(Integer idSitTributacao) {
        this.idSitTributacao = idSitTributacao;
    }

    public SituacaoTributaria(Integer idSitTributacao, String txSitTributacao,
            boolean boSitTributacaoAtivo, Boolean tributaIss) {
        this.idSitTributacao = idSitTributacao;
        this.txSitTributacao = txSitTributacao;
        this.boSitTributacaoAtivo = boSitTributacaoAtivo;
        this.tributaIss = tributaIss;
    }

    public Integer getIdSitTributacao() {
        return idSitTributacao;
    }

    public void setIdSitTributacao(Integer idSitTributacao) {
        this.idSitTributacao = idSitTributacao;
    }

    public String getTxSitTributacao() {
        return txSitTributacao;
    }

    public void setTxSitTributacao(String txSitTributacao) {
        this.txSitTributacao = txSitTributacao;
    }

    public boolean getBoSitTributacaoAtivo() {
        return boSitTributacaoAtivo;
    }

    public void setBoSitTributacaoAtivo(boolean boSitTributacaoAtivo) {
        this.boSitTributacaoAtivo = boSitTributacaoAtivo;
    }

    public Boolean getTributaIss() {
        return tributaIss;
    }

    public void setTributaIss(Boolean boSitTributacao) {
        this.tributaIss = boSitTributacao;
    }

    public List<ContribEmpresaSituacaoTributacao> getContribSituacaoTributacoes() {
        return contribSituacaoTributacoes;
    }

    public void setContribSituacaoTributacoes(
            List<ContribEmpresaSituacaoTributacao> contribSituacaoTributacoes) {
        this.contribSituacaoTributacoes = contribSituacaoTributacoes;
    }

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setListRegimeTributacao(List<RegimeTributacao> listRegimeTributacao) {
		this.listRegimeTributacao = listRegimeTributacao;
	}

	public List<RegimeTributacao> getListRegimeTributacao() {
		return listRegimeTributacao;
	}

	@Override
    public String toString() {
        return "teste.SitucaoTributaria[ idSitTributacao=" + idSitTributacao
                + " ]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSitTributacao == null) ? 0 : idSitTributacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SituacaoTributaria other = (SituacaoTributaria) obj;
		if (idSitTributacao == null) {
			if (other.idSitTributacao != null)
				return false;
			else if(this == other)
                return true;
			 else 
	             return false;
		} else if (!idSitTributacao.equals(other.idSitTributacao))
			return false;
		return true;
	}
	
}
