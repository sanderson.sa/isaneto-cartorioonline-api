/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

/**
 * 
 * @author Bruno
 */
@Entity
@Table(name = "operador", schema = "seguranca")
public class Operador  implements Serializable {
	
    /**
     * 
     */
    private static final long serialVersionUID = 3867281014931287185L;
    
    @Id
    @SequenceGenerator(name = "operador_id_operador_seq", sequenceName = "seguranca.operador_id_operador_seq", schema = "seguranca", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operador_id_operador_seq")
    @Column(name = "id_operador", nullable = false)
    private Long idOperador;
    
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id_pessoa")
    @OneToOne(fetch = FetchType.EAGER)
    private Pessoa pessoa;   
    
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "tx_operador_login", nullable = false, length = 15)
    private String txOperadorLogin;
    
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "tx_operador_senha", nullable = false, length = 256)
    private String txOperadorSenha;
    
    @NotNull
    @Column(name = "bo_operador_status", nullable = false)
    private Boolean boOperadorStatus;
    
    @Column(name = "id_unidade_lotacao")
    private Integer idUnidadeLotacao;
    
    @Size(max = 20)
    @Column(name = "tx_operador_matricula", length = 20)
    private String txOperadorMatricula;
   
    @Size(max = 100)
    @Column(name = "tx_operador_frase_seguranca", length = 100)
    private String txOperadorFraseSeguranca;    

    @Column(name = "bo_operador_recebe_email_nfe")
    private Boolean boOperadorRecebeEmailNfe;
    
    @Column(name = "tx_operador_ddd", length = 3)
    private String txOperadorDdd;
    
    @Column(name = "tx_operador_telefone", length = 9)
    private String txOperadorTelefone;
    
    @Column(name = "tx_operador_fax_ddd", length = 3)
    private String txOperadorFaxDdd;
    
    @Column(name = "tx_operador_fax_telefone", length = 9)
    private String txOperadorFaxTelefone;
    
    @Column(name = "tx_operador_cel_ddd", length = 3)
    private String txOperadorCelDdd;
    
    @Column(name = "tx_operador_cel_telefone", length = 9)
    private String txOperadorCelTelefone;
    
    //@NotEmpty
    ///@NotNull
    @Email
    @Size(max = 100)
    @Column(name = "tx_operador_email", length = 100)
    private String txOperadorEmail;    
    
    @Column(name = "dt_operador_validade")
    @Temporal(TemporalType.DATE)
    private Date dtOperadorValidade;

    @Column(name = "dt_operador_incluiu")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtOperadorIncluiu;
   /* @NotAudited
    @OneToMany(mappedBy = "idOperadorIncluiu")
    private List<Operador> operadorList;*/
    
    @JoinColumn(name = "id_operador_incluiu", referencedColumnName = "id_operador")
    @ManyToOne
    private Operador idOperadorIncluiu;   
    
    @Transient
	private String txTelefoneComDdd;
	@Transient
	private String txCelularComDdd;
	@Transient
	private String txFaxComDdd;
    
	@Transient
	private boolean administrativo;

    public Operador() {
    }

    public Operador(Long idOperador) {
        this.idOperador = idOperador;
    }

    public Operador(Long idOperador, String txOperadorLogin,
            String txOperadorSenha, Boolean boOperadorStatus,
            Date dtOperadorValidade) {
        this.idOperador = idOperador;
        this.txOperadorLogin = txOperadorLogin;
        this.txOperadorSenha = txOperadorSenha;
        this.boOperadorStatus = boOperadorStatus;
        this.dtOperadorValidade = dtOperadorValidade;
    }
    
   

	public Long getIdOperador() {
		return idOperador;
	}

	public void setIdOperador(Long idOperador) {
		this.idOperador = idOperador;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getTxOperadorLogin() {
		return txOperadorLogin;
	}

	public void setTxOperadorLogin(String txOperadorLogin) {
		this.txOperadorLogin = txOperadorLogin;
	}

	public String getTxOperadorSenha() {
		return txOperadorSenha;
	}

	public void setTxOperadorSenha(String txOperadorSenha) {
		this.txOperadorSenha = txOperadorSenha;
	}

	public Boolean getBoOperadorStatus() {
		return boOperadorStatus;
	}

	public void setBoOperadorStatus(Boolean boOperadorStatus) {
		this.boOperadorStatus = boOperadorStatus;
	}

	public Integer getIdUnidadeLotacao() {
		return idUnidadeLotacao;
	}

	public void setIdUnidadeLotacao(Integer idUnidadeLotacao) {
		this.idUnidadeLotacao = idUnidadeLotacao;
	}

	public String getTxOperadorMatricula() {
		return txOperadorMatricula;
	}

	public void setTxOperadorMatricula(String txOperadorMatricula) {
		this.txOperadorMatricula = txOperadorMatricula;
	}

	public String getTxOperadorFraseSeguranca() {
		return txOperadorFraseSeguranca;
	}

	public void setTxOperadorFraseSeguranca(String txOperadorFraseSeguranca) {
		this.txOperadorFraseSeguranca = txOperadorFraseSeguranca;
	}

	public Boolean getBoOperadorRecebeEmailNfe() {
		return boOperadorRecebeEmailNfe;
	}

	public void setBoOperadorRecebeEmailNfe(Boolean boOperadorRecebeEmailNfe) {
		this.boOperadorRecebeEmailNfe = boOperadorRecebeEmailNfe;
	}

	public String getTxOperadorDdd() {
		return txOperadorDdd;
	}

	public void setTxOperadorDdd(String txOperadorDdd) {
		this.txOperadorDdd = txOperadorDdd;
	}

	public String getTxOperadorTelefone() {
		return txOperadorTelefone;
	}

	public void setTxOperadorTelefone(String txOperadorTelefone) {
		this.txOperadorTelefone = txOperadorTelefone;
	}

	public String getTxOperadorFaxDdd() {
		return txOperadorFaxDdd;
	}

	public void setTxOperadorFaxDdd(String txOperadorFaxDdd) {
		this.txOperadorFaxDdd = txOperadorFaxDdd;
	}

	public String getTxOperadorFaxTelefone() {
		return txOperadorFaxTelefone;
	}

	public void setTxOperadorFaxTelefone(String txOperadorFaxTelefone) {
		this.txOperadorFaxTelefone = txOperadorFaxTelefone;
	}

	public String getTxOperadorCelDdd() {
		return txOperadorCelDdd;
	}

	public void setTxOperadorCelDdd(String txOperadorCelDdd) {
		this.txOperadorCelDdd = txOperadorCelDdd;
	}

	public String getTxOperadorCelTelefone() {
		return txOperadorCelTelefone;
	}

	public void setTxOperadorCelTelefone(String txOperadorCelTelefone) {
		this.txOperadorCelTelefone = txOperadorCelTelefone;
	}

	public String getTxOperadorEmail() {
		return txOperadorEmail;
	}

	public void setTxOperadorEmail(String txOperadorEmail) {
		this.txOperadorEmail = txOperadorEmail;
	}

	public Date getDtOperadorValidade() {
		return dtOperadorValidade;
	}

	public void setDtOperadorValidade(Date dtOperadorValidade) {
		this.dtOperadorValidade = dtOperadorValidade;
	}

	public Date getDtOperadorIncluiu() {
		return dtOperadorIncluiu;
	}

	public void setDtOperadorIncluiu(Date dtOperadorIncluiu) {
		this.dtOperadorIncluiu = dtOperadorIncluiu;
	}

	public Operador getIdOperadorIncluiu() {
		return idOperadorIncluiu;
	}

	public void setIdOperadorIncluiu(Operador idOperadorIncluiu) {
		this.idOperadorIncluiu = idOperadorIncluiu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idOperador == null) ? 0 : idOperador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operador other = (Operador) obj;
		if (idOperador == null) {
			if (other.idOperador != null)
				return false;
		} else if (!idOperador.equals(other.idOperador))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Operador [idOperador=" + idOperador + "]";
	}

	

	public String getTxTelefoneComDdd() {
		if((getTxOperadorDdd()!=null) && (getTxOperadorTelefone()!=null)){
			return getTxOperadorDdd().concat(getTxOperadorTelefone());
		}else{
			return "";
		}
	}

	public void setTxTelefoneComDdd(String txTelefoneComDdd) {
		if((txTelefoneComDdd!=null) && (!txTelefoneComDdd.equals(""))){
			txOperadorDdd = txTelefoneComDdd.substring(0, 2);
			txOperadorTelefone = txTelefoneComDdd.substring(2, 10);
		}
		this.txTelefoneComDdd = txTelefoneComDdd;
	}

	public String getTxCelularComDdd() {
		if((getTxOperadorCelDdd()!=null) && (getTxOperadorCelTelefone()!=null)){
			return getTxOperadorCelDdd().concat(getTxOperadorCelTelefone());
		}else{
			return "";
		}
	}

	public void setTxCelularComDdd(String txCelularComDdd) {
		if((txCelularComDdd!=null) && (!txCelularComDdd.equals(""))){
			txOperadorCelDdd = txCelularComDdd.substring(0, 2);
			txOperadorCelTelefone = txCelularComDdd.substring(2, 10);
		}
		this.txCelularComDdd = txCelularComDdd;
	}

	public String getTxFaxComDdd() {
		if((getTxOperadorFaxDdd()!=null) && (getTxOperadorFaxTelefone()!=null)){
			return getTxOperadorFaxDdd().concat(getTxOperadorFaxTelefone());
		}else{
			return "";
		}
	}

	public void setTxFaxComDdd(String txFaxComDdd) {
		if((txFaxComDdd!=null) && (!txFaxComDdd.equals(""))){
			txOperadorFaxDdd = txFaxComDdd.substring(0, 2);
			txOperadorFaxTelefone = txFaxComDdd.substring(2, 10);
		}
		this.txFaxComDdd = txFaxComDdd;
	}

	public boolean isAdministrativo() {
		return administrativo;
	}

	public void setAdministrativo(boolean administrativo) {
		this.administrativo = administrativo;
	}
	
   
}
