package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "contrib_empresa_situacao_tributacao", schema = "mobiliario")
public class ContribEmpresaSituacaoTributacao implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5305532388223194298L;
    @Id
    @SequenceGenerator(name = "contrib_empresa_situacao_tribu_id_contrib_empresa_sit_tribu_seq", sequenceName = "mobiliario.contrib_empresa_situacao_tribu_id_contrib_empresa_sit_tribu_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contrib_empresa_situacao_tribu_id_contrib_empresa_sit_tribu_seq")
    @Column(name = "id_contrib_empresa_sit_tribu", nullable = false)
    private Long idContribSitTribu;
    @JoinColumn(name = "id_contribuinte", referencedColumnName = "id_contribuinte", nullable = false)
    @ManyToOne(optional = false)
    private ContribuinteEmpresa contribuinteEmpresa;
    @JoinColumn(name = "id_sit_tributacao", referencedColumnName = "id_sit_tributacao", nullable = false)
    @ManyToOne(optional = false)
    private SituacaoTributaria situacaoTributacao;    
    @Basic(optional = false)
    @NotNull
    @Column(name = "dt_contrib_empresa_sit_tribu_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicioSituacaoTributacao;
    @Column(name = "dt_contrib_empresa_sit_tribu_fim")
    @Temporal(TemporalType.DATE)
    private Date dataFimSituacaoTributacao;
    @Column(name = "dt_contrib_empresa_sit_trib_ult_atualiz")
    @Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore private Date dataUltimaAtualizacao;
    @JoinColumn(name = "id_operador", referencedColumnName = "id_operador")
    @OneToOne
    private Operador operador;   
    
	@PrePersist
	public void beforaPersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
	
	@PreUpdate
	public void beforeUpdate(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
	    
    public ContribEmpresaSituacaoTributacao() {
    }

    public ContribEmpresaSituacaoTributacao(Long idContribSitTribu) {
        this.idContribSitTribu = idContribSitTribu;
    }

    public ContribEmpresaSituacaoTributacao(Long idContribSitTribu, Date dataIniciSituacaoTributacao) {
        this.idContribSitTribu = idContribSitTribu;
        this.dataInicioSituacaoTributacao = dataIniciSituacaoTributacao;
    }

    public ContribEmpresaSituacaoTributacao(ContribEmpresaSituacaoTributacao situacao) {
		this.contribuinteEmpresa = situacao.getContribuinteEmpresa();
		this.idContribSitTribu = situacao.getIdContribSitTribu();
		this.dataUltimaAtualizacao = situacao.getDataUltimaAtualizacao();
		this.dataFimSituacaoTributacao = situacao.getDataFimSituacaoTributacao();
		this.dataInicioSituacaoTributacao = situacao.getDataInicioSituacaoTributacao();
		this.situacaoTributacao = situacao.getSituacaoTributacao();
	}

	public Long getIdContribSitTribu() {
		return idContribSitTribu;
	}

	public void setIdContribSitTribu(Long idContribSitTribu) {
		this.idContribSitTribu = idContribSitTribu;
	}

	public ContribuinteEmpresa getContribuinteEmpresa() {
		return contribuinteEmpresa;
	}

	public void setContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa) {
		this.contribuinteEmpresa = contribuinteEmpresa;
	}

	public SituacaoTributaria getSituacaoTributacao() {
		return situacaoTributacao;
	}

	public void setSituacaoTributacao(SituacaoTributaria situacaoTributacao) {
		this.situacaoTributacao = situacaoTributacao;
	}

	public Date getDataInicioSituacaoTributacao() {
		return dataInicioSituacaoTributacao;
	}

	public void setDataInicioSituacaoTributacao(Date dataInicioSituacaoTributacao) {
		this.dataInicioSituacaoTributacao = dataInicioSituacaoTributacao;
	}

	public Date getDataFimSituacaoTributacao() {
		return dataFimSituacaoTributacao;
	}

	public void setDataFimSituacaoTributacao(Date dataFimSituacaoTributacao) {
		this.dataFimSituacaoTributacao = dataFimSituacaoTributacao;
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public Operador getOperador() {
		return operador;
	}

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

    @Override
    public String toString() {
    	if (this.situacaoTributacao != null){
    		return this.idContribSitTribu+ " - " + this.situacaoTributacao.getTxSitTributacao();
    	} 
    	else {
    		return "ContribEmpresaSituacaoTributacao[ idContribSitTribu=" + idContribSitTribu + " ]";
    	}	
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idContribSitTribu == null) ? 0 : idContribSitTribu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContribEmpresaSituacaoTributacao other = (ContribEmpresaSituacaoTributacao) obj;
		if (idContribSitTribu == null) {
			if (other.idContribSitTribu != null)
				return false;
			else if(this == other)
                return true;
			 else 
	             return false;
		} else if (!idContribSitTribu.equals(other.idContribSitTribu))
			return false;
		return true;
	}

}

