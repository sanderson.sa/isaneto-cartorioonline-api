package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Operador;

@Repository
//public interface OperadorRepository extends CrudRepository<Operador, Long> {
////	@Query(value = "SELECT * FROM seguranca.operador p WHERE p.id_operador=?1", nativeQuery = true)
//	@Query(value = "SELECT o FROM Operador o WHERE o.idOperador=?1")
//	Operador findByIdOperador(Long id);
//}

public interface OperadorRepository extends CrudRepository<Operador, Long> {

  @Query("select u from Operador as u where u.idOperador = ?1 ")
  	Operador findByIdOperador(Long id);
}
