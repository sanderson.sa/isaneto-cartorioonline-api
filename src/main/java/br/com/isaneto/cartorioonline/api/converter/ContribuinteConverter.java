package br.com.isaneto.cartorioonline.api.converter;

import br.com.isaneto.cartorioonline.api.modelo.*;
import br.com.isaneto.cartorioonline.api.modelo.dto.*;
import br.com.isaneto.cartorioonline.api.repository.ContribEmpresaAtividadeRepository;
import br.com.isaneto.cartorioonline.api.repository.ContribEmpresaRegimeTributacaoRepository;
import br.com.isaneto.cartorioonline.api.repository.ContribEmpresaSituacaoTributacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContribuinteConverter {

    @Autowired
    private ContribEmpresaAtividadeRepository contribEmpresaAtividadeRepository;

    @Autowired
    private ContribEmpresaRegimeTributacaoRepository contribEmpresaRegimeTributacaoRepository;

    @Autowired
    private ContribEmpresaSituacaoTributacaoRepository contribEmpresaSituacaoTributacaoRepository;

    public ContribuinteDTO paraDTO(Contribuinte contribuinte){
        ContribuinteDTO contribuinteDTO = new ContribuinteDTO();

        List<ContribEmpresaAtividade> listaContribEmpresaAtividade = contribEmpresaAtividadeRepository.findByContribuinteEmpresa(contribuinte.getContribuinteEmpresa());
        List<ContribEmpresaRegimeTributacao> listaContribEmpresaRegimeTributacao = contribEmpresaRegimeTributacaoRepository.findByContribuinteEmpresa(contribuinte.getContribuinteEmpresa());
        List<ContribEmpresaSituacaoTributacao> listaContribEmpresaSituacaoTributacao = contribEmpresaSituacaoTributacaoRepository.findByContribuinteEmpresa(contribuinte.getContribuinteEmpresa());

        // Preencher DTO
        contribuinteDTO.setCpfCnpj(contribuinte.getPessoa().getNuPessoaCpfCnpj().toString());
        contribuinteDTO.setInscricao(contribuinte.getContribuinteEmpresa().getInscricaoMunicipal().toString().replaceAll("\\D", ""));
        contribuinteDTO
                .setTipoEstabelecimento(contribuinte.getContribuinteEmpresa().getTipoContribuinte().getIdTipoContribuinteEmpresa().toString());
        contribuinteDTO.setDataCadastro(contribuinte.getContribuinteEmpresa().getDataContribEmpresaCadastro().toString());
        contribuinteDTO.setUf(contribuinte.getContribuinteEmpresa().getPessoa().getEstado().getNoEstado());
        contribuinteDTO.setCidade(contribuinte.getContribuinteEmpresa().getPessoa().getCidade().getNoCidade());
        contribuinteDTO.setCep(contribuinte.getContribuinteEmpresa().getPessoa().getNuPessoaEndCep().toString());
        contribuinteDTO.setEnderecoLogradouro(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEndeLogradouro());
        contribuinteDTO.setEnderecoNumero(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEndNumero());
        contribuinteDTO.setEnderecoBairro(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEndBairro());
        contribuinteDTO.setEnderecoComplemento(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEndeComplemento());
        if(contribuinte.getContribuinteEmpresa().getPessoa() instanceof  PessoaJuridica)
            contribuinteDTO.setNaturezaJuridica(((PessoaJuridica) contribuinte.getContribuinteEmpresa().getPessoa()).getIdNatJuridica().getNuNatJuridicaCod());
        RepresentanteLegalCpfDto representante = new RepresentanteLegalCpfDto();

        PessoaFisica respLegal = null;

        if(contribuinte.getContribuinteEmpresa().getPessoa() instanceof  PessoaJuridica)
            respLegal = ((PessoaJuridica) contribuinte.getContribuinteEmpresa().getPessoa()).getIdRepresentanteLegal();

        if (respLegal != null) {
            representante.setCpf(respLegal.getNuPessoaCpfCnpj().toString());
            representante.setNome(respLegal.getNoPessoa());
            representante.setDataNascimento(respLegal.getDtPessoaFisNasc().toString());
            representante.setSexo(respLegal.getTxPessoaFisSexo());
            representante.setTipoIdentificacaoDoc(respLegal.getTipoIdentificacaoDoc().getIdTpIdentificacao().toString());
            representante.setNumeroDocumento(respLegal.getTxPessoaFisDoc());
            representante.setDataExpedicao(respLegal.getDtPessoaFisExpeDoc() != null ? respLegal.getDtPessoaFisExpeDoc().toString() : "");
            representante.setUfExpedicaoDocumento(respLegal.getEstadoDoc() != null ? respLegal.getEstadoDoc().getTxEstadoSigla() : "");
            representante.setOrgaoExpeditor(respLegal.getTxPessoaFisOrgExpDoc());
            representante.setCep(respLegal.getNuPessoaEndCep() != null ? respLegal.getNuPessoaEndCep().toString() : "");
            representante.setUfResidencia(respLegal.getEstado() != null ? respLegal.getEstado().getTxEstadoSigla() : "");
            representante.setCidade(respLegal.getCidade() != null ? respLegal.getCidade().getNoCidade() : "");
            representante.setEnderecoLogradouro(respLegal.getTxPessoaEndeLogradouro());
            representante.setEnderecoNumero(respLegal.getTxPessoaEndNumero());
            representante.setEnderecoBairro(respLegal.getTxPessoaEndBairro());
            representante.setEnderecoComplemento(respLegal.getTxPessoaEndeComplemento());
        }
        contribuinteDTO.setDdd(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaDdd() != null ? contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaDdd() : "");
        contribuinteDTO.setTelefone(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaTelefone() != null ? contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaTelefone(): "");
        contribuinteDTO.setDddCelular(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaCelDdd() != null ? contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaCelDdd(): "");
        contribuinteDTO.setCelular(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaCelTelefone() != null? contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaCelTelefone(): "");
        contribuinteDTO.setEmail(contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEmail() != null ? contribuinte.getContribuinteEmpresa().getPessoa().getTxPessoaEmail() : "");
        //
        List<AtividadeCnaeDTO> listaAtividadeCnae = new ArrayList<AtividadeCnaeDTO>();
        if (!listaContribEmpresaAtividade.isEmpty()) {
            for (ContribEmpresaAtividade i : listaContribEmpresaAtividade) {
                AtividadeCnaeDTO atividade = new AtividadeCnaeDTO();
                atividade.setCodigo(i.getAtividade().getTxAtividadeCnaeSm());
                atividade.setPrincipal(i.getAtividadePrincipal() ? "S" : "N");
                atividade.setDataInicio(i.getDataInicioAtividade().toString());
                atividade.setDataFim(i.getDataTerminoAtividade() != null ? i.getDataTerminoAtividade().toString() : "");
                listaAtividadeCnae.add(atividade);
            }
            contribuinteDTO.setAtividadeCnae(listaAtividadeCnae);
        }
        //
        List<RegimeTributacaoDTO> listaRegimeTributacao = new ArrayList<RegimeTributacaoDTO>();
        if (!listaContribEmpresaRegimeTributacao.isEmpty()) {
            for (ContribEmpresaRegimeTributacao i : listaContribEmpresaRegimeTributacao) {
                RegimeTributacaoDTO regimeTrib = new RegimeTributacaoDTO();
                regimeTrib.setCodigo(i.getRegimeTributacao().getIdRegimeTributacao().toString());
                regimeTrib.setDataInicio(i.getDataInicio().toString());
                regimeTrib.setDataFim(i.getDataTermino() != null ? i.getDataTermino().toString() : "");
                listaRegimeTributacao.add(regimeTrib);
            }
            contribuinteDTO.setRegimeTributacao(listaRegimeTributacao);
        }
        //
        List<SituacaoTributariaDTO> listaSituacaoTributaria = new ArrayList<SituacaoTributariaDTO>();
        if (!listaContribEmpresaSituacaoTributacao.isEmpty()) {
            for (ContribEmpresaSituacaoTributacao i : listaContribEmpresaSituacaoTributacao) {
                SituacaoTributariaDTO situacaoTributariaDTO = new SituacaoTributariaDTO();
                situacaoTributariaDTO.setCodigo(i.getSituacaoTributacao().getIdSitTributacao().toString());
                situacaoTributariaDTO.setDataInicio(i.getDataInicioSituacaoTributacao().toString());
                situacaoTributariaDTO.setDataFim(
                        i.getDataFimSituacaoTributacao() != null ? i.getDataFimSituacaoTributacao().toString() : "");
                listaSituacaoTributaria.add(situacaoTributariaDTO);
            }
            contribuinteDTO.setSituacaoTributaria(listaSituacaoTributaria);
        }
        return contribuinteDTO;
    }

    public Contribuinte paraEntidade(ContribuinteDTO dto){
        Contribuinte contribuinte = new Contribuinte();

        return contribuinte;
    }
}
