package br.com.isaneto.cartorioonline.api.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity 
@Table(name = "contrib_empresa_atividade", schema = "mobiliario") 
@NamedQueries({
    @NamedQuery(name = "ContribEmpresaAtividade.findAll", query = "SELECT c FROM ContribEmpresaAtividade c"),
    @NamedQuery(name = "ContribEmpresaAtividade.findByPessoaJurAtividade", query = "SELECT c FROM ContribEmpresaAtividade c JOIN FETCH c.atividade WHERE c.contribuinteEmpresa = :contribuinteEmpresa"),
    @NamedQuery(name = "ContribEmpresaAtividade.findByIdPessoaJurAtividade", query = "SELECT c FROM ContribEmpresaAtividade c WHERE c.idContribEmpresaAtividade = :idContribEmpresaAtividade"),    
    @NamedQuery(name = "ContribEmpresaAtividade.findByBoPessoaJurAtivPrincipal", query = "SELECT c FROM ContribEmpresaAtividade c WHERE c.atividadePrincipal = :atividadePrincipal"),
    @NamedQuery(name = "ContribEmpresaAtividade.findByDtPessoaJurAtivInicio", query = "SELECT c FROM ContribEmpresaAtividade c WHERE c.dataInicioAtividade = :dataInicioAtividade"),
    @NamedQuery(name = "ContribEmpresaAtividade.findByDtPessoaJurAtivTermino", query = "SELECT c FROM ContribEmpresaAtividade c WHERE c.dataTerminoAtividade = :dataTerminoAtividade")})
       
public class ContribEmpresaAtividade implements Serializable {

    /** 
     * 
     */
    private static final long serialVersionUID = -6216213850769402496L;
    @Id
    @SequenceGenerator(name = "contrib_empresa_atividade_id_contrib_emp_atividade_seq", sequenceName = "mobiliario.contrib_empresa_atividade_id_contrib_emp_atividade_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contrib_empresa_atividade_id_contrib_emp_atividade_seq")
    @Column(name = "id_contrib_emp_atividade", nullable = false)
    private Long idContribEmpresaAtividade;
    @NotNull
    @Column(name = "bo_contrib_emp_ativ_principal", nullable = false)
    private boolean atividadePrincipal;
    @Column(name = "dt_contrib_emp_ativ_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicioAtividade;
    @Column(name = "dt_contrib_emp_atv_termino")
    @Temporal(TemporalType.DATE)
    private Date dataTerminoAtividade;
    @JoinColumn(name = "id_contribuinte", referencedColumnName = "id_contribuinte", nullable = false)
    @ManyToOne(optional = false)
    private ContribuinteEmpresa contribuinteEmpresa;
    @JoinColumn(name = "id_atividade", referencedColumnName = "id_atividade", nullable = false)
    @ManyToOne(optional = false)
    private Atividade atividade;
    
	@ManyToOne
    @JoinColumn(name = "id_operador_incluiu", referencedColumnName="id_operador")
    private Operador operadorIncluiu;
    
	@ManyToOne
    @JoinColumn(name = "id_operador_alterou", referencedColumnName="id_operador")
    private Operador operadorAlterou;
          
    @Column(name = "dt_contrib_emp_incluiu")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInclusao;
    
	@Column(name = "dt_contrib_emp_alterou")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAlteracao;
	
	@Column(name = "nu_contrib_emp_atv_ordem")
    private Integer nuContribEmpAtvOrdem;
	
	
    
    public ContribEmpresaAtividade() {
    }

    public ContribEmpresaAtividade(Long idContribEmpresaAtividade) {
        this.idContribEmpresaAtividade = idContribEmpresaAtividade;
    }
    
    public ContribEmpresaAtividade(ContribEmpresaAtividade atividade){
    	this.idContribEmpresaAtividade = atividade.getIdContribEmpresaAtividade();
    	this.atividadePrincipal = atividade.getAtividadePrincipal();
    	this.dataInicioAtividade = atividade.getDataInicioAtividade();
    	this.dataTerminoAtividade = atividade.getDataTerminoAtividade();
    	this.operadorIncluiu = atividade.getOperadorIncluiu(); 
    	this.operadorAlterou = atividade.getOperadorAlterou();
    	this.dataInclusao = atividade.getDataInclusao();
    	this.dataAlteracao= atividade.getDataAlteracao();
    	this.atividade = atividade.getAtividade();
    	this.contribuinteEmpresa = atividade.getContribuinteEmpresa();
    }

    public ContribEmpresaAtividade(Long idContribEmpresaAtividade,
            boolean atividadePrincipal) {
        this.idContribEmpresaAtividade = idContribEmpresaAtividade;
        this.atividadePrincipal = atividadePrincipal;
    }

  	public Long getIdContribEmpresaAtividade() {
		return idContribEmpresaAtividade;
	}

	public void setIdContribEmpresaAtividade(Long idContribEmpresaAtividade) {
		this.idContribEmpresaAtividade = idContribEmpresaAtividade;
	}

	public boolean getAtividadePrincipal() {
		return atividadePrincipal;
	}

	public void setAtividadePrincipal(boolean atividadePrincipal) {
		this.atividadePrincipal = atividadePrincipal;
	}

	public Date getDataInicioAtividade() {
		return dataInicioAtividade;
	}

	public void setDataInicioAtividade(Date dataInicioAtividade) {
		this.dataInicioAtividade = dataInicioAtividade;
	}

	public Date getDataTerminoAtividade() {
		return dataTerminoAtividade;
	}

	public void setDataTerminoAtividade(Date dataTerminoAtividade) {
		this.dataTerminoAtividade = dataTerminoAtividade;
	}

	public ContribuinteEmpresa getContribuinteEmpresa() {
		return contribuinteEmpresa;
	}

	public void setContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa) {
		this.contribuinteEmpresa = contribuinteEmpresa;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

    public Operador getOperadorIncluiu() {
		return operadorIncluiu;
	}

	public void setOperadorIncluiu(Operador operadorIncluiu) {
		this.operadorIncluiu = operadorIncluiu;
	}

	public Operador getOperadorAlterou() {
		return operadorAlterou;
	}

	public void setOperadorAlterou(Operador operadorAlterou) {
		this.operadorAlterou = operadorAlterou;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

   

    @Override
    public String toString() { 
        return atividade.getTxAtividadeCnae(); 
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idContribEmpresaAtividade == null) ? 0 : idContribEmpresaAtividade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContribEmpresaAtividade other = (ContribEmpresaAtividade) obj;
		if (idContribEmpresaAtividade == null) {
			if (other.idContribEmpresaAtividade != null)
				return false;
			else if(this == other)
                return true;
			 else 
	             return false;
		} else if (!idContribEmpresaAtividade.equals(other.idContribEmpresaAtividade))
			return false;
		return true;
	}

	public Integer getNuContribEmpAtvOrdem() {
		return nuContribEmpAtvOrdem;
	}

	public void setNuContribEmpAtvOrdem(Integer nuContribEmpAtvOrdem) {
		this.nuContribEmpAtvOrdem = nuContribEmpAtvOrdem;
	}
}
