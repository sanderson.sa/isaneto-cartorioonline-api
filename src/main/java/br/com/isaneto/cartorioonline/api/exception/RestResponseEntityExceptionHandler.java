package br.com.isaneto.cartorioonline.api.exception;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

import br.com.isaneto.cartorioonline.api.util.ErrorDetails;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    public RestResponseEntityExceptionHandler() {
        super();
    }

//    @ExceptionHandler(Exception.class)
//    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
//        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
//        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//
//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
//                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
//        ErrorDetails errorDetails = new ErrorDetails(new Date(), "Validation Failed", ex.getBindingResult().toString());
//        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
//    }

    // API

    // 400



//    @ExceptionHandler({ ConstraintViolationException.class })
//    public ResponseEntity<Object> handleBadRequest(final DataIntegrityViolationException ex, final WebRequest request) {
//        final String bodyOfResponse = "This should be application specific";
//        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
//    }
//
//    @Override
//    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
//        final String bodyOfResponse = "This should be application specific";
//        // ex.getCause() instanceof JsonMappingException, JsonParseException // for additional information later on
//        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
//    }
//
//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
//        final String bodyOfResponse = "This should be application specific";
//        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
//    }


    // 404



    // 409

//    @ExceptionHandler({ DataIntegrityViolationException.class, DataAccessException.class })
//    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
//        final String bodyOfResponse = "This should be application specific";
//        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
//    }

    // 412

    // 500

    @ExceptionHandler(value = { EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(final EntityNotFoundException ex, final HttpServletRequest request) {
        logger.error("404 Status Code - NO CONTENT");
        ex.printStackTrace();
        String tituloErro = "Recurso Não Localizado";
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body( new ErrorDetails(HttpStatus.NO_CONTENT, tituloErro , Arrays.asList(ex.getMessage()), request.getRequestURI()));
    }

    @ExceptionHandler({ NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class })
    /*500*/public ResponseEntity<Object> handleInternal(final RuntimeException e, HttpServletRequest servletRequest) {
        logger.error("500 Status Code - INTERNAL_SERVER_ERROR");
        e.printStackTrace();
        String tituloErro = "Erro Interno de Aplicação";
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body( new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, tituloErro , Arrays.asList(e.getMessage()), servletRequest.getRequestURI()));
    }

    @ExceptionHandler({ InvalidDataAccessApiUsageException.class })
    public ResponseEntity<Object> handleBadRequest(final InvalidDataAccessApiUsageException ex, final HttpServletRequest request) {
        logger.error("409 Status Code - CONFLICT");
        ex.printStackTrace();
        String tituloErro = "CONFLICT - Dados já existentes.";
        return ResponseEntity.status(HttpStatus.CONFLICT).body( new ErrorDetails(HttpStatus.CONFLICT, tituloErro , Arrays.asList(ex.getMessage()), request.getRequestURI()));
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<Object> handleApiException(ApiException ex, HttpServletRequest request) {
        logger.error("404 Status Code - BAD_REQUEST");
        ex.printStackTrace();
        String tituloErro = "Erro de API";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body( new ErrorDetails(HttpStatus.BAD_REQUEST, tituloErro , Arrays.asList(ex.getMessage()), request.getRequestURI()));
    }

}
