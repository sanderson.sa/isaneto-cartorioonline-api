package br.com.isaneto.cartorioonline.api.modelo.dto;

public class AtividadeCnaeDTO {
	private String codigo;
	private String dataInicio;
	private String dataFim;
	private String principal;
	
	public AtividadeCnaeDTO(String codigo, String dataInicio, String dataFim, String principal) {
		super();
		this.codigo = codigo;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.principal = principal;
	}
	public AtividadeCnaeDTO() {

	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	
	
}
