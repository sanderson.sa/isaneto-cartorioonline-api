package br.com.isaneto.cartorioonline.api.modelo;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "contrib_empresa_situacao", schema = "mobiliario")
public class ContribEmpresaSituacao implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3753865786648966109L;
    @Id
    @SequenceGenerator(name = "contrib_empresa_situacao_id_contrib_emp_sit_seq", sequenceName = "mobiliario.contrib_empresa_situacao_id_contrib_emp_sit_seq", schema = "mobiliario", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contrib_empresa_situacao_id_contrib_emp_sit_seq")
    @Column(name = "id_contrib_emp_sit", nullable = false)
    private Long idContribEmpresaSituacao;
    @JoinColumn(name = "id_contribuinte", referencedColumnName = "id_contribuinte", nullable = false)
    @ManyToOne(optional = false)
    private ContribuinteEmpresa contribuinteEmpresa;    
    @JoinColumn(name = "id_sit_contribuinte", referencedColumnName = "id_sit_contribuinte", nullable = false)
    @ManyToOne(optional = false)
    private SituacaoContribuinte situacaoContribuinte;           
    @Basic(optional = false)
    @NotNull
    @Column(name = "dt_contrib_emp_sit_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicioSituacaoContribuinte;
    @Column(name = "dt_contrib_emp_sit_encerramento")
    @Temporal(TemporalType.DATE)
    private Date dataEncerramentoSituacaoContribuinte;
    @Column(name = "dt_contrib_emp_sit_ult_atualiz")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
	private Date dataUltimaAtualizacao;
    @JoinColumn(name = "id_operador", referencedColumnName = "id_operador")
    @OneToOne
    private Operador operador;

    
    public ContribEmpresaSituacao(ContribEmpresaSituacao editContribEmpresaSituacao) {
    	super();
		this.idContribEmpresaSituacao = editContribEmpresaSituacao.idContribEmpresaSituacao;
		this.contribuinteEmpresa = editContribEmpresaSituacao.contribuinteEmpresa;
		this.situacaoContribuinte = editContribEmpresaSituacao.situacaoContribuinte;
		this.dataInicioSituacaoContribuinte = editContribEmpresaSituacao.dataInicioSituacaoContribuinte;
		this.dataEncerramentoSituacaoContribuinte = editContribEmpresaSituacao.dataEncerramentoSituacaoContribuinte;
		this.dataUltimaAtualizacao = editContribEmpresaSituacao.dataUltimaAtualizacao;
		this.operador = editContribEmpresaSituacao.operador;
	}

	public ContribEmpresaSituacao() {
		
	}

	public ContribEmpresaSituacao(SituacaoContribuinte sitContribAtiva, ContribuinteEmpresa c,
			Operador o) {
		definirDados(sitContribAtiva, c);
		this.setOperador(o);
		this.setDataUltimaAtualizacao(new Date());
	}
	
	public void definirDados(SituacaoContribuinte sitContribAtiva, ContribuinteEmpresa c){
		this.setContribuinteEmpresa(c);
		this.setDataInicioSituacaoContribuinte(new Date());
		this.setSituacaoContribuinte(sitContribAtiva);
	}

	@PrePersist
	@PreUpdate
	public void beforePersiste(){
		this.dataUltimaAtualizacao = new GregorianCalendar().getTime();
	}
    
	public Long getIdContribEmpresaSituacao() {
		return idContribEmpresaSituacao;	
	}
	
	public void setIdContribEmpresaSituacao(Long idContribEmpresaSituacao) {
		this.idContribEmpresaSituacao = idContribEmpresaSituacao;
	}
	
	public ContribuinteEmpresa getContribuinteEmpresa() {
		return contribuinteEmpresa;
	}
	
	public void setContribuinteEmpresa(ContribuinteEmpresa contribuinteEmpresa) {
		this.contribuinteEmpresa = contribuinteEmpresa;
	}
	
	public SituacaoContribuinte getSituacaoContribuinte() {
		return situacaoContribuinte;
	}
	
	public void setSituacaoContribuinte(SituacaoContribuinte situacaoContribuinte) {
		this.situacaoContribuinte = situacaoContribuinte;
	}
	
	public Date getDataInicioSituacaoContribuinte() {
		return dataInicioSituacaoContribuinte;
	}
	
	public void setDataInicioSituacaoContribuinte(
			Date dataInicioSituacaoContribuinte) {
		this.dataInicioSituacaoContribuinte = dataInicioSituacaoContribuinte;
	}
	
	public Date getDataEncerramentoSituacaoContribuinte() {
		return dataEncerramentoSituacaoContribuinte;
	}
	
	public void setDataEncerramentoSituacaoContribuinte(
			Date dataEncerramentoSituacaoContribuinte) {
		this.dataEncerramentoSituacaoContribuinte = dataEncerramentoSituacaoContribuinte;
	}
	
	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}
	
	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}
	
	public Operador getOperador() {
		return operador;
	}
	
	public void setOperador(Operador operador) {
		this.operador = operador;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idContribEmpresaSituacao == null) ? 0
						: idContribEmpresaSituacao.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContribEmpresaSituacao other = (ContribEmpresaSituacao) obj;
		if (idContribEmpresaSituacao == null) {
			if (other.idContribEmpresaSituacao != null)
				return false;
		} else if (!idContribEmpresaSituacao
				.equals(other.idContribEmpresaSituacao))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ContribEmpresaSituacao [idContribEmpresaSituacao="
				+ idContribEmpresaSituacao + "]";
	}

	   
       
    
}
