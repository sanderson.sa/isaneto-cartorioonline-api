//package br.com.isaneto.cartorioonline.api.modelo;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//
///**
// *
// * @author Walmir Portal
// */
//@Entity
//@Table(name = "portal_parametro", schema = "seguranca")
//public class PortalParametro implements Serializable {
//	
//    private static final long serialVersionUID = 1L;
//    
//    @Id
//    @SequenceGenerator(name = "portal_parametro_id_portal_param_seq", sequenceName = "seguranca.portal_parametro_id_portal_param_seq", schema = "seguranca", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "portal_parametro_id_portal_param_seq")
//    @Column(name = "id_portal_param", nullable = false)
//    private Integer idPortalParam;
//    
//    @NotNull
//    @JoinColumn(name = "id_portal", referencedColumnName = "id_portal")
//    @ManyToOne
//    private Portal portal;
//    
//    @NotNull
//    @Column(name = "tx_portal_param_codigo")
//    private String txPortalParamCodigo;
//    
//    @NotNull
//    @Column(name = "no_portal_param")
//    private String noPortalParam;
//    
//    @Column(name = "tx_portal_param_descricao")
//    private String txPortalParamDescricao;
//    
//    @NotNull
//    @Column(name = "tx_portal_param_valor")
//    private String txPortalParamValor;
//    
//    @Column(name = "nu_portal_param_tempo_maximo")
//    private Integer nuPortalParamTempoMaximo;
//    
//    @Column(name = "nu_portal_param_tipo")
//    private Integer nuPortalParamTipo;
//    
//    @Column(name = "tx_portal_param_padrao")
//    private String txPortalParamPadrao;
//    
//    @Column(name = "vl_portal_param_valor_minimo", precision = 10, scale = 2)
//    private BigDecimal vlPortalParamValorMinimo;
//    
//    @Column(name = "vl_portal_param_valor_valor_maximo", precision = 10, scale = 2)
//    private BigDecimal vlPortalParamValorValorMaximo;
//    
//
//    public PortalParametro() {
//    	
//    }
//
//    public Integer getIdPortalParam() {
//        return idPortalParam;
//    }
//
//    public void setIdPortalParam(Integer idPortalParam) {
//        this.idPortalParam = idPortalParam;
//    }
//
//    public String getTxPortalParamCodigo() {
//        return txPortalParamCodigo;
//    }
//
//    public void setTxPortalParamCodigo(String txPortalParamCodigo) {
//        this.txPortalParamCodigo = txPortalParamCodigo;
//    }
//
//    public String getNoPortalParam() {
//        return noPortalParam;
//    }
//
//    public void setNoPortalParam(String noPortalParam) {
//        this.noPortalParam = noPortalParam;
//    }
//
//    public String getTxPortalParamDescricao() {
//        return txPortalParamDescricao;
//    }
//
//    public void setTxPortalParamDescricao(String txPortalParamDescricao) {
//        this.txPortalParamDescricao = txPortalParamDescricao;
//    }
//
//    public String getTxPortalParamValor() {
//        return txPortalParamValor;
//    }
//
//    public void setTxPortalParamValor(String txPortalParamValor) {
//        this.txPortalParamValor = txPortalParamValor;
//    }
//
//    public Integer getNuPortalParamTempoMaximo() {
//        return nuPortalParamTempoMaximo;
//    }
//
//    public void setNuPortalParamTempoMaximo(Integer nuPortalParamTempoMaximo) {
//        this.nuPortalParamTempoMaximo = nuPortalParamTempoMaximo;
//    }
//
//    public Integer getNuPortalParamTipo() {
//        return nuPortalParamTipo;
//    }
//
//    public void setNuPortalParamTipo(Integer nuPortalParamTipo) {
//        this.nuPortalParamTipo = nuPortalParamTipo;
//    }
//
//    public String getTxPortalParamPadrao() {
//        return txPortalParamPadrao;
//    }
//
//    public void setTxPortalParamPadrao(String txPortalParamPadrao) {
//        this.txPortalParamPadrao = txPortalParamPadrao;
//    }
//
//    public BigDecimal getVlPortalParamValorMinimo() {
//        return vlPortalParamValorMinimo;
//    }
//
//    public void setVlPortalParamValorMinimo(BigDecimal vlPortalParamValorMinimo) {
//        this.vlPortalParamValorMinimo = vlPortalParamValorMinimo;
//    }
//
//    public BigDecimal getVlPortalParamValorValorMaximo() {
//        return vlPortalParamValorValorMaximo;
//    }
//
//    public void setVlPortalParamValorValorMaximo(BigDecimal vlPortalParamValorValorMaximo) {
//        this.vlPortalParamValorValorMaximo = vlPortalParamValorValorMaximo;
//    }
//
//    public Portal getPortal() {
//		return portal;
//	}
//
//	public void setPortal(Portal portal) {
//		this.portal = portal;
//	}
//	
//
//	@Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idPortalParam != null ? idPortalParam.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        if (!(object instanceof PortalParametro)) {
//            return false;
//        }
//        PortalParametro other = (PortalParametro) object;
//        if ((this.idPortalParam == null && other.idPortalParam != null) || (this.idPortalParam != null && !this.idPortalParam.equals(other.idPortalParam))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "com.isaneto.seguranca.PortalParametro[ idPortalParam=" + idPortalParam + " ]";
//    }
//    
//}
//
