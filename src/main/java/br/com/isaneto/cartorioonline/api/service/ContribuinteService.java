package br.com.isaneto.cartorioonline.api.service;

import br.com.isaneto.cartorioonline.api.exception.ApiException;
import br.com.isaneto.cartorioonline.api.modelo.*;
import br.com.isaneto.cartorioonline.api.modelo.dto.AtividadeCnaeDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.ContribuinteDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.RegimeTributacaoDTO;
import br.com.isaneto.cartorioonline.api.modelo.dto.SituacaoTributariaDTO;
import br.com.isaneto.cartorioonline.api.repository.*;
import br.com.isaneto.cartorioonline.api.util.Util;
import br.com.isaneto.cartorioonline.api.util.VerificarErrosBeanValidator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
//@Transactional(rollbackFor = {Exception.class})
@Transactional(rollbackFor = {RuntimeException.class})
public class ContribuinteService {

    @Autowired
    private ContribuinteRepository contribuinteRepo;

    @Autowired
    private ContribuinteEmpresaRepository contribuinteEmpresaRepository;


    @Autowired
    private PessoaRepository pessoaRepository;


    @Autowired
    private ContribEmpresaSituacaoRepository contribEmpresaSituacaoRepo;

    @Autowired
    private AtividadeEmpresaRepository contribEmpresaAtividadeRepository;

    @Autowired
    private ContribEmpresaRegTributacao contribEmpresaRegimeTributacaoRepository;

    @Autowired
    private ContribEmpresaSitTributariaRepository contribEmpresaSitTributariaRepository;


    @Autowired
    private OperadorRepository operadorRepository;

    @Autowired
    private SituacaoContribuinteRepository situacaoContribuinteRepository;


    @Autowired
    private TipoEstabelecimentoRepository tipoEstabelecimentoRepository;

    @Autowired
    private CepRepository cepRepository;

    @Autowired
    private AtividadeRepository atividadeRepository;

    @Autowired
    private TipoContribuinteEmpresaRepository tipoContribuinteEmpresaRepository;

    @Autowired
    private RegimeTributacaoRepository regimeTributacaoRepository;

    @Autowired
    private SituacaoTributariaRepository situacaoTributariaRepository;

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;

    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;


    private VerificarErrosBeanValidator<ContribuinteEmpresa> verificarErroContribuinteEmpresa = new VerificarErrosBeanValidator<>();


    public List<Contribuinte> listarTodos() {
        return contribuinteRepo.findAll();
    }

    public Optional<Contribuinte> pegarPorPessoa(Pessoa pessoa) {
        return contribuinteRepo.findByPessoa(pessoa);
    }

    public Optional<Contribuinte> pegarPorCpfCnpjInsc(String insc, BigInteger cpfCnpj) {
        return contribuinteRepo.findByPessoaAndInsc(insc, cpfCnpj);
    }

    public Contribuinte findOne(Long id) {
        return contribuinteRepo.findById(id).get();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Contribuinte save(Contribuinte obj) {
        return contribuinteRepo.saveAndFlush(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public ContribEmpresaSituacao save(ContribEmpresaSituacao obj) {
        return contribEmpresaSituacaoRepo.saveAndFlush(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public Pessoa save(Pessoa obj) {
        return pessoaRepository.save(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public Pessoa save(PessoaFisica obj) {
        return pessoaRepository.save(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public ContribuinteEmpresa save(ContribuinteEmpresa obj) {
        return contribuinteEmpresaRepository.saveAndFlush(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public ContribEmpresaAtividade save(ContribEmpresaAtividade obj) {
        return contribEmpresaAtividadeRepository.saveAndFlush(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public ContribEmpresaRegimeTributacao save(ContribEmpresaRegimeTributacao obj) {
        return contribEmpresaRegimeTributacaoRepository.saveAndFlush(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public ContribEmpresaSituacaoTributacao save(ContribEmpresaSituacaoTributacao obj) {
        return contribEmpresaSitTributariaRepository.saveAndFlush(obj);
    }

    //remove

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(Contribuinte obj) {
        contribuinteRepo.delete(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(Pessoa obj) {
        pessoaRepository.delete(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(ContribuinteEmpresa obj) {
        contribuinteEmpresaRepository.delete(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(ContribEmpresaAtividade obj) {
        contribEmpresaAtividadeRepository.delete(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(ContribEmpresaRegimeTributacao obj) {
        contribEmpresaRegimeTributacaoRepository.delete(obj);
    }

    //	@Transactional(propagation = Propagation.REQUIRED)
    public void remove(ContribEmpresaSituacaoTributacao obj) {
        contribEmpresaSitTributariaRepository.delete(obj);
    }


    public String txPortalParamToken(EntityManager entityManager) {
        String token = "";
        StringBuilder sql = new StringBuilder();
        sql.append(
                " select tx_portal_param_valor from seguranca.portal_parametro p where p.tx_portal_param_codigo = '04_000057' and p.no_portal_param = 'TOKEN_REST'");

        try {
            Query query = entityManager.createNativeQuery(sql.toString());
            List<String> results = query.getResultList();
            if (!results.isEmpty()) {
                for (String a : results) {
                    token = (a != null) ? ((String) a) : "";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    public String insertPoint(String str) {
        StringBuilder builder = new StringBuilder(str);
        int x = 2;
        builder.insert(builder.length() - x, ".");
        return builder.toString();
    }

    //Valida apenas para PJ
    private void validaTipoEstabelecimento(String tipoEstab) {
        Optional<TipoEstabelecimento> tipoEstabelecimento = tipoEstabelecimentoRepository.findById(new Integer(tipoEstab));
        if (!tipoEstabelecimento.isPresent()) {
            throw new ApiException(String.format("Não existe tipo de estabelecimento para o código informado :: %s ", tipoEstab));
        }
    }

    private void validaCEP(String cep) {
        Optional<Cep> cepObj = cepRepository.findByCep(cep.replaceAll("\\D", ""));
        if (!cepObj.isPresent()) {
            throw new ApiException(String.format("CEP Não localizado :: %s ", cep));
        }
    }

    private void validaInscricaoExistente(String inscricao) {
        if (!Objects.isNull(inscricao))
            if (contribuinteEmpresaRepository.findByInscricaoMunicipal(new Long(inscricao)).isPresent())
                throw new InvalidDataAccessApiUsageException(String.format("Inscrição municipal já existente:: %s ", inscricao));
    }

    private void validaAtividadesCNAE(List<AtividadeCnaeDTO> cnaes) {
        if (Objects.isNull(cnaes) || cnaes.isEmpty())
            throw new ApiException("Lista atividades CNAE não pode ser vazia");

        cnaes.forEach(cnae -> {
            Optional<Atividade> a = atividadeRepository.findByTxAtividadeCnaeSm(cnae.getCodigo());
            if (!a.isPresent()) {
                throw new ApiException(String.format("Atividade CNAE não localizada :: %s ", cnae.getCodigo()));
            }
        });
    }

    private void validaRegimeTributacao(List<RegimeTributacaoDTO> listaRegimeTributacao) {
        if (Objects.isNull(listaRegimeTributacao) || listaRegimeTributacao.isEmpty())
            throw new ApiException("Lista Regime Tributaçãonão pode ser vazia");

        listaRegimeTributacao.forEach(regimeTributacao -> {
            Optional<RegimeTributacao> rt = regimeTributacaoRepository.findById(new Integer(regimeTributacao.getCodigo()));
            if (!rt.isPresent()) {
                throw new ApiException(String.format("Regime de Tributação não localizado :: %s ", regimeTributacao.getCodigo()));
            }
        });
    }

    private void validaSituacaoTributaria(List<SituacaoTributariaDTO> listaSituacaoTributaria) {
        if (Objects.isNull(listaSituacaoTributaria) || listaSituacaoTributaria.isEmpty())
            throw new ApiException("Lista Regime Tributaçãonão pode ser vazia");

        listaSituacaoTributaria.forEach(st -> {
            Optional<SituacaoTributaria> situacaoTributaria = situacaoTributariaRepository.findById(new Integer(st.getCodigo()));
            if (!situacaoTributaria.isPresent()) {
                throw new ApiException(String.format("Situação Tributária não localizada :: %s ", st.getCodigo()));
            }
        });
    }

    private Pessoa getPessoa(String cnpj) {
        Optional<Pessoa> pessoa = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cnpj));
        if (pessoa.isPresent())
            return pessoa.get();
        else
            return null;
    }

    public ResponseEntity<?> getErrosValidacaoRequest(BindingResult result) {
        StringBuilder erros = new StringBuilder();
        erros.append("Erros de Validação :: ");

        for (ObjectError erro : result.getAllErrors()) {
            FieldError err = (FieldError) erro;
            MessagemRetorno.TcMensagemRetorno msg = new MessagemRetorno.TcMensagemRetorno();
            erros.append(String.format("CAMPO: %s", err.getField()));
            erros.append(String.format(" - ERRO: %s", err.getDefaultMessage()));
            erros.append(" || ");
        }

        //Remove último ||
        throw new ApiException(erros.toString().substring(0, erros.toString().length() - 4));
    }

    public Contribuinte convertDtoParaContribuintePF(ContribuinteDTO contribuinte) {
        if (Objects.isNull(contribuinte))
            throw new ApiException("Erro ao processar requisição verifique o JSON enviado está de acordo com o modelo da documentaçaõ");

        //Consultar pessoa juridica
        Pessoa pessoa = getPessoa((contribuinte.getCpfCnpj()));
        if (Objects.isNull(pessoa))
            throw new EntityNotFoundException(String.format("Pessoa não localizada CpfCnpj :: %s ", contribuinte.getCpfCnpj()));

        //Salvar Registro Contribuinte
        Optional<PessoaFisica> pj = pessoaFisicaRepository.findById(pessoa.getIdPessoa());
        if(!pj.isPresent())
            throw new ApiException(String.format("Pessoa Fisica não localizada CpfCnpj :: %s ", contribuinte.getCpfCnpj()));

        PessoaFisica pessoaFisica = pj.get();

        Contribuinte c = new Contribuinte();
        try {

            //Validar DTO
            validaTipoEstabelecimento(contribuinte.getTipoEstabelecimento());
            validaCEP(contribuinte.getCep());
            validaInscricaoExistente(contribuinte.getInscricao());
            validaAtividadesCNAE(contribuinte.getAtividadeCnae());
            validaRegimeTributacao(contribuinte.getRegimeTributacao());
            validaSituacaoTributaria(contribuinte.getSituacaoTributaria());

            //Negócio
            save(pessoaFisica);
            c.setPessoa(pessoaFisica);
            c.setInscricaoMunicipalSemMascara(contribuinte.getInscricao());
            c.setInscricaoMunicipalComMascara(contribuinte.getInscricao());
            //tipo contribuinte
            c.setTipoContribuinte("M");

            //idOperadorIncluiu
            Operador idOperadorIncluiu = operadorRepository.findByIdOperador(1L);
            c.setIdOperadorIncluiu(idOperadorIncluiu);

            //Salvar Contribuinte
            Contribuinte contrib = save(c);

            //ContribuinteEmpresa
            ContribuinteEmpresa contribuinteEmpresa = new ContribuinteEmpresa();
            contribuinteEmpresa.setIdContribuinte(contrib.getIdContribuinte());
            contribuinteEmpresa.setContribuinte(contrib);
            contribuinteEmpresa.setPessoa((Pessoa) pessoaFisica);
            contribuinteEmpresa.setInscricaoMunicipal(new Long(contribuinte.getInscricao()));
            contribuinteEmpresa.setTipoEmpresa('J');
            contribuinteEmpresa.setTipoContribuinte(tipoContribuinteEmpresaRepository.findById(1).get());
            contribuinteEmpresa.setDataContribEmpresaCadastro(Util.strToDateConvert(contribuinte.getDataCadastro()));
            contribuinteEmpresa.setOperadorIncluiu(idOperadorIncluiu);


            //Validação realizada em validaTipoEstabelecimento();
            Optional<TipoEstabelecimento> tipoEstabelecimento;
            tipoEstabelecimento = tipoEstabelecimentoRepository.findById(new Integer(contribuinte.getTipoEstabelecimento()));
            contribuinteEmpresa.setTipoEstabelecimento(tipoEstabelecimento.get());

            Optional<Cep> cep = cepRepository.findByCep(contribuinte.getCep().replaceAll("\\D", ""));
            contribuinteEmpresa.setEstado(cep.get().getEstado());
            contribuinteEmpresa.setCidade(cep.get().getCidade());
            contribuinteEmpresa.setTipoLogradouro(cep.get().getTipoLogradouro());
            contribuinteEmpresa.setTxContribuinteEnderecoBairro(cep.get().getBairro());
            contribuinteEmpresa.setTxContribuinteLogradouro(contribuinte.getEnderecoLogradouro());
            contribuinteEmpresa.setTxContribuinteEnderecoNumero(contribuinte.getEnderecoNumero());
            contribuinteEmpresa.setTxContribuinteEnderecoComplemento(contribuinte.getEnderecoComplemento());
            contribuinteEmpresa.setNuContribuinteEnderecoCep(cep.get().getNuCep());

            try {
                verificarErroContribuinteEmpresa.validarDados(contribuinteEmpresa);
                save(contribuinteEmpresa);
            } catch (Exception e) {
                throw new ApiException(String.format("Ocorreu um erro ao salvar contribuinte, tente novamente, se o problema persistir entre em contato com o suporte :: %s", e.getMessage()));
            }

            ContribEmpresaSituacao contribEmpresaSituacao = new ContribEmpresaSituacao();
            contribEmpresaSituacao.setSituacaoContribuinte(situacaoContribuinteRepository.findById(1).get());
            contribEmpresaSituacao.setDataInicioSituacaoContribuinte(new Date());
            contribEmpresaSituacao.setContribuinteEmpresa(contribuinteEmpresa);
            contribEmpresaSituacao.setOperador(idOperadorIncluiu);
            contribEmpresaSituacao.setIdContribEmpresaSituacao(contribEmpresaSituacao.getIdContribEmpresaSituacao());
            save(contribEmpresaSituacao);

            //Atividades CNAE
            for (AtividadeCnaeDTO atv : contribuinte.getAtividadeCnae()) {
                Optional<Atividade> a = atividadeRepository.findByTxAtividadeCnaeSm(atv.getCodigo());
                ContribEmpresaAtividade contribEmpresaAtividade = new ContribEmpresaAtividade();
                contribEmpresaAtividade.setAtividade(a.get());
                contribEmpresaAtividade.setAtividadePrincipal(atv.getPrincipal().equals("S") ? true : false);
                contribEmpresaAtividade.setOperadorIncluiu(idOperadorIncluiu);
                contribEmpresaAtividade.setDataInicioAtividade(Util.strToDateConvert(atv.getDataInicio()));
                contribEmpresaAtividade.setDataTerminoAtividade(Util.strToDateConvert(atv.getDataFim()));
                contribEmpresaAtividade.setContribuinteEmpresa(contribuinteEmpresa);
                contribEmpresaAtividade.setDataInclusao(new Date());
                save(contribEmpresaAtividade);
            }

            //regimeTributacao
            for (RegimeTributacaoDTO regTrib : contribuinte.getRegimeTributacao()) {
                Optional<RegimeTributacao> regimeTributacao = regimeTributacaoRepository.findById(new Integer(regTrib.getCodigo()));
                ContribEmpresaRegimeTributacao contribEmpresaRegimeTributacao = new ContribEmpresaRegimeTributacao();
                contribEmpresaRegimeTributacao.setRegimeTributacao(regimeTributacao.get());
                contribEmpresaRegimeTributacao.setContribuinteEmpresa(contribuinteEmpresa);
                contribEmpresaRegimeTributacao.setDataInicio(Util.strToDateConvert(regTrib.getDataInicio()));
                contribEmpresaRegimeTributacao.setDataTermino(Util.strToDateConvert(regTrib.getDataFim()));
                contribEmpresaRegimeTributacao.setOperador(idOperadorIncluiu);
                contribEmpresaRegimeTributacao.setDataIncluiu(new Date());
                save(contribEmpresaRegimeTributacao);
            }

            //situacaoTributaria
            for (SituacaoTributariaDTO i : contribuinte.getSituacaoTributaria()) {
                Optional<SituacaoTributaria> SituacaoTributaria = situacaoTributariaRepository.findById(new Integer(i.getCodigo()));
                ContribEmpresaSituacaoTributacao contribEmpresaSituacaoTributacao = new ContribEmpresaSituacaoTributacao();
                contribEmpresaSituacaoTributacao.setContribuinteEmpresa(contribuinteEmpresa);
                contribEmpresaSituacaoTributacao.setDataInicioSituacaoTributacao(Util.strToDateConvert(i.getDataInicio()));
                contribEmpresaSituacaoTributacao.setDataFimSituacaoTributacao(Util.strToDateConvert(i.getDataFim()));
                contribEmpresaSituacaoTributacao.setSituacaoTributacao(SituacaoTributaria.get());
                contribEmpresaSituacaoTributacao.setOperador(idOperadorIncluiu);
                save(contribEmpresaSituacaoTributacao);
            }
            return contrib;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        }
    }

    public Contribuinte convertDtoParaContribuintePJ(ContribuinteDTO contribuinte) {
        if (Objects.isNull(contribuinte))
            throw new ApiException("Erro ao processar requisição verifique o JSON enviado está de acordo com o modelo da documentaçaõ");

        //Consultar pessoa juridica
        Pessoa pessoa = getPessoa((contribuinte.getCpfCnpj()));
        if (Objects.isNull(pessoa))
            throw new EntityNotFoundException(String.format("Pessoa não localizada CpfCnpj :: %s ", contribuinte.getCpfCnpj()));

        //Salvar Registro Contribuinte
        Optional<PessoaJuridica> pj = pessoaJuridicaRepository.findById(pessoa.getIdPessoa());
        if(!pj.isPresent())
            throw new ApiException(String.format("Pessoa Jurídica não localizada CpfCnpj :: %s ", contribuinte.getCpfCnpj()));

        PessoaJuridica pessoaJuridica = pj.get();

        Contribuinte c = new Contribuinte();


        //Validar DTO
        validaTipoEstabelecimento(contribuinte.getTipoEstabelecimento());
        validaCEP(contribuinte.getCep());
        validaInscricaoExistente(contribuinte.getInscricao());
        validaAtividadesCNAE(contribuinte.getAtividadeCnae());
        validaRegimeTributacao(contribuinte.getRegimeTributacao());
        validaSituacaoTributaria(contribuinte.getSituacaoTributaria());

        //Negócio
        save(pessoaJuridica);
        c.setPessoa(pessoaJuridica);
        c.setInscricaoMunicipalSemMascara(contribuinte.getInscricao());
        c.setInscricaoMunicipalComMascara(contribuinte.getInscricao());
        //tipo contribuinte
        c.setTipoContribuinte("M");

        //idOperadorIncluiu
        Operador idOperadorIncluiu = operadorRepository.findByIdOperador(1L);
        c.setIdOperadorIncluiu(idOperadorIncluiu);

        //Salvar Contribuinte
        Contribuinte contrib = save(c);

        //ContribuinteEmpresa
        ContribuinteEmpresa contribuinteEmpresa = new ContribuinteEmpresa();
        contribuinteEmpresa.setIdContribuinte(contrib.getIdContribuinte());
        contribuinteEmpresa.setContribuinte(contrib);
        contribuinteEmpresa.setPessoa((Pessoa) pessoaJuridica);
        contribuinteEmpresa.setInscricaoMunicipal(new Long(contribuinte.getInscricao()));
        contribuinteEmpresa.setTipoEmpresa('J');
        contribuinteEmpresa.setTipoContribuinte(tipoContribuinteEmpresaRepository.findById(1).get());
        contribuinteEmpresa.setDataContribEmpresaCadastro(Util.strToDateConvert(contribuinte.getDataCadastro()));
        contribuinteEmpresa.setOperadorIncluiu(idOperadorIncluiu);


        //Validação realizada em validaTipoEstabelecimento();
        Optional<TipoEstabelecimento> tipoEstabelecimento;
        tipoEstabelecimento = tipoEstabelecimentoRepository.findById(new Integer(contribuinte.getTipoEstabelecimento()));
        contribuinteEmpresa.setTipoEstabelecimento(tipoEstabelecimento.get());

        Optional<Cep> cep = cepRepository.findByCep(contribuinte.getCep().replaceAll("\\D", ""));
        contribuinteEmpresa.setEstado(cep.get().getEstado());
        contribuinteEmpresa.setCidade(cep.get().getCidade());
        contribuinteEmpresa.setTipoLogradouro(cep.get().getTipoLogradouro());
        contribuinteEmpresa.setTxContribuinteEnderecoBairro(cep.get().getBairro());
        contribuinteEmpresa.setTxContribuinteLogradouro(contribuinte.getEnderecoLogradouro());
        contribuinteEmpresa.setTxContribuinteEnderecoNumero(contribuinte.getEnderecoNumero());
        contribuinteEmpresa.setTxContribuinteEnderecoComplemento(contribuinte.getEnderecoComplemento());
        contribuinteEmpresa.setNuContribuinteEnderecoCep(cep.get().getNuCep());

        try {
            verificarErroContribuinteEmpresa.validarDados(contribuinteEmpresa);
            save(contribuinteEmpresa);
        } catch (Exception e) {
            throw new ApiException(String.format("Ocorreu um erro ao salvar contribuinte, tente novamente, se o problema persistir entre em contato com o suporte :: %s", e.getMessage()));
        }

        ContribEmpresaSituacao contribEmpresaSituacao = new ContribEmpresaSituacao();
        contribEmpresaSituacao.setSituacaoContribuinte(situacaoContribuinteRepository.findById(1).get());
        contribEmpresaSituacao.setDataInicioSituacaoContribuinte(new Date());
        contribEmpresaSituacao.setContribuinteEmpresa(contribuinteEmpresa);
        contribEmpresaSituacao.setOperador(idOperadorIncluiu);
        contribEmpresaSituacao.setIdContribEmpresaSituacao(contribEmpresaSituacao.getIdContribEmpresaSituacao());
        save(contribEmpresaSituacao);

        //Atividades CNAE
        for (AtividadeCnaeDTO atv : contribuinte.getAtividadeCnae()) {
            Optional<Atividade> a = atividadeRepository.findByTxAtividadeCnaeSm(atv.getCodigo());
            ContribEmpresaAtividade contribEmpresaAtividade = new ContribEmpresaAtividade();
            contribEmpresaAtividade.setAtividade(a.get());
            contribEmpresaAtividade.setAtividadePrincipal(atv.getPrincipal().equals("S") ? true : false);
            contribEmpresaAtividade.setOperadorIncluiu(idOperadorIncluiu);
            contribEmpresaAtividade.setDataInicioAtividade(Util.strToDateConvert(atv.getDataInicio()));
            contribEmpresaAtividade.setDataTerminoAtividade(Util.strToDateConvert(atv.getDataFim()));
            contribEmpresaAtividade.setContribuinteEmpresa(contribuinteEmpresa);
            contribEmpresaAtividade.setDataInclusao(new Date());
            save(contribEmpresaAtividade);
        }

        //regimeTributacao
        for (RegimeTributacaoDTO regTrib : contribuinte.getRegimeTributacao()) {
            Optional<RegimeTributacao> regimeTributacao = regimeTributacaoRepository.findById(new Integer(regTrib.getCodigo()));
            ContribEmpresaRegimeTributacao contribEmpresaRegimeTributacao = new ContribEmpresaRegimeTributacao();
            contribEmpresaRegimeTributacao.setRegimeTributacao(regimeTributacao.get());
            contribEmpresaRegimeTributacao.setContribuinteEmpresa(contribuinteEmpresa);
            contribEmpresaRegimeTributacao.setDataInicio(Util.strToDateConvert(regTrib.getDataInicio()));
            contribEmpresaRegimeTributacao.setDataTermino(Util.strToDateConvert(regTrib.getDataFim()));
            contribEmpresaRegimeTributacao.setOperador(idOperadorIncluiu);
            contribEmpresaRegimeTributacao.setDataIncluiu(new Date());
            save(contribEmpresaRegimeTributacao);
        }

        //situacaoTributaria
        for (SituacaoTributariaDTO i : contribuinte.getSituacaoTributaria()) {
            Optional<SituacaoTributaria> SituacaoTributaria = situacaoTributariaRepository.findById(new Integer(i.getCodigo()));
            ContribEmpresaSituacaoTributacao contribEmpresaSituacaoTributacao = new ContribEmpresaSituacaoTributacao();
            contribEmpresaSituacaoTributacao.setContribuinteEmpresa(contribuinteEmpresa);
            contribEmpresaSituacaoTributacao.setDataInicioSituacaoTributacao(Util.strToDateConvert(i.getDataInicio()));
            contribEmpresaSituacaoTributacao.setDataFimSituacaoTributacao(Util.strToDateConvert(i.getDataFim()));
            contribEmpresaSituacaoTributacao.setSituacaoTributacao(SituacaoTributaria.get());
            contribEmpresaSituacaoTributacao.setOperador(idOperadorIncluiu);
            save(contribEmpresaSituacaoTributacao);
        }
        return contrib;

    }
}
