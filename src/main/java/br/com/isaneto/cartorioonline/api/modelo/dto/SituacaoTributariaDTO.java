package br.com.isaneto.cartorioonline.api.modelo.dto;

public class SituacaoTributariaDTO {

	private String codigo;
	private String dataInicio;
	private String dataFim;
	

	public SituacaoTributariaDTO() {
		
	}
	
	public SituacaoTributariaDTO(String codigo, String dataInicio, String dataFim) {
		super();
		this.codigo = codigo;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
}
