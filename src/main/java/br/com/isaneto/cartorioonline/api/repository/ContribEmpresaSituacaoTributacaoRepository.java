package br.com.isaneto.cartorioonline.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.ContribEmpresaSituacaoTributacao;
import br.com.isaneto.cartorioonline.api.modelo.ContribuinteEmpresa;

@Repository
public interface ContribEmpresaSituacaoTributacaoRepository extends JpaRepository<ContribEmpresaSituacaoTributacao, Long>{
	List<ContribEmpresaSituacaoTributacao> findByContribuinteEmpresa(ContribuinteEmpresa contribEmpresa);
}
