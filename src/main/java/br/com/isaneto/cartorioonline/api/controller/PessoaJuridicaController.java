package br.com.isaneto.cartorioonline.api.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import br.com.isaneto.cartorioonline.api.exception.ApiException;
import br.com.isaneto.cartorioonline.api.modelo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.isaneto.cartorioonline.api.modelo.dto.PessoaJuridicaDTO;
import br.com.isaneto.cartorioonline.api.repository.CidadeRepository;
import br.com.isaneto.cartorioonline.api.repository.EstadoRepository;
import br.com.isaneto.cartorioonline.api.repository.NaturezaJuridicaRepository;
import br.com.isaneto.cartorioonline.api.repository.PessoaRepository;
import br.com.isaneto.cartorioonline.api.service.ContribuinteService;
import br.com.isaneto.cartorioonline.api.util.Util;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("pessoa-juridica")
@Slf4j
public class PessoaJuridicaController {

	@Autowired
	private ContribuinteService contribuinteService;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private NaturezaJuridicaRepository naturezaJuridicaRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@GetMapping("/{cnpj}")
	public ResponseEntity<?> getPessoaJuridica(
			@PathVariable("cnpj") String cnpj){
		
		Optional<Pessoa> pessoaJuridica = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cnpj));
		if(!pessoaJuridica.isPresent()) {
			throw new EntityNotFoundException(String.format("Pessoa não localizada CNPJ :: %s ", cnpj));
		}
		//Preencher DTO
		PessoaJuridicaDTO dto = new PessoaJuridicaDTO((PessoaJuridica) pessoaJuridica.get());
		return new ResponseEntity<Object>(dto, HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<?> postPessoaJuridica(
			@Valid @RequestBody PessoaJuridicaDTO dto,
			BindingResult result){

		if(result.hasErrors())
			contribuinteService.getErrosValidacaoRequest(result);
		
		//Verifica se já existe
		Optional<Pessoa> p = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getCnpj()));
		if(p.isPresent()) {
			throw new InvalidDataAccessApiUsageException(String.format("Já existe um registro para o CNPJ :: ", dto.getCnpj()));
		}
			
		//Salvar Pessoa Juridica
		PessoaJuridica pj = new PessoaJuridica(dto);
		pj.setIdNatJuridica(naturezaJuridicaRepository.getOne(Integer.parseInt(dto.getNaturezaJuridica())));
//		this.tipoEstabelecimento = pj.get; ContribuinteEmpresa
//		this.dataCadastro = dataContribEmpresaCadastro; ContribuinteEmpresa
		pj.setEstado(estadoRepository.findByTxEstadoSigla(dto.getUf()));
		pj.setCidade(cidadeRepository.findByTxCidadeCodIbge(dto.getCidade()));
		Optional<NaturezaJuridica> naturezaJuridica = naturezaJuridicaRepository.findByNuNatJuridicaCod(dto.getNaturezaJuridica());
		if(naturezaJuridica.isPresent())
			pj.setIdNatJuridica(naturezaJuridica.get());
		else
			throw new ApiException(String.format("Natureza Juridica não localizada :: ", dto.getNaturezaJuridica()));
		if(StringUtils.isEmpty(dto.getRepresentanteLegalCnpj())) {			
			//FIXME Verificar com o Simeão essa situação
		}
		if(!StringUtils.isEmpty(dto.getRepresentanteLegalCpf())) {
			Optional<Pessoa> pjExist = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getRepresentanteLegalCpf()));
			if(pjExist.isPresent()) {
				pj.setIdRepresentanteLegal((PessoaFisica) pjExist.get());
			}
		}
		
		PessoaJuridica pessJur = pessoaRepository.save(pj);
		return new ResponseEntity<>(new PessoaJuridicaDTO(pessJur), HttpStatus.CREATED);
	}
	
	@PutMapping("")
	public ResponseEntity<?> putPessoaJuridica(
			@Valid @RequestBody PessoaJuridicaDTO dto,
			BindingResult result){

		if(result.hasErrors())
			contribuinteService.getErrosValidacaoRequest(result);
		
		//Verifica se já existe
		Optional<Pessoa> p = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getCnpj()));
		if(!p.isPresent()) {
			throw new EntityNotFoundException(String.format("Não existe uma pessoa juridica para o CNPJ :: %s ", dto.getCnpj()));
		}
			
		//Salvar Pessoa Juridica
		PessoaJuridica pj = (PessoaJuridica) p.get();
		pj.setTxPessoaJurFantasia(dto.getNomeFantasia());
        pj.setVlPessoaJurCapital(new BigDecimal(dto.getCapitalSocial()));
        pj.setDtPessoaJurConstituicao(Util.strToDateConvert(dto.getDataConstituicao()));
        pj.setCnpj(dto.getCnpj());
        pj.setNuPessoaCpfCnpj(new BigInteger(dto.getCnpj()));
		pj.setNoPessoa(dto.getRazaoSocial());
		pj.setTxPessoaJurFantasia(dto.getNomeFantasia());
		pj.setDtPessoaCad(Util.strToDateConvert(dto.getDataCadastro()));
		pj.setVlPessoaJurCapital(new BigDecimal(dto.getCapitalSocial()));
		pj.setNuPessoaEndCep(new BigInteger(dto.getCep()));
		pj.setTxPessoaEndeLogradouro(dto.getEnderecoLogradouro());
		pj.setTxPessoaEndNumero(dto.getEnderecoNumero());
		pj.setTxPessoaEndeComplemento(dto.getEnderecoComplemento());
		pj.setTxPessoaEndBairro(dto.getEnderecoBairro());
		pj.setTxPessoaDdd(dto.getDdd());
		pj.setTxPessoaTelefone(dto.getTelefone());
		pj.setTxPessoaCelDdd(dto.getDddCelular());
		pj.setTxCelularComDdd(dto.getCelular());
		pj.setTxPessoaEmail(dto.getCelular());
		pj.setIdNatJuridica(naturezaJuridicaRepository.getOne(Integer.parseInt(dto.getNaturezaJuridica())));
		pj.setEstado(estadoRepository.findByTxEstadoSigla(dto.getUf()));
		pj.setCidade(cidadeRepository.findByTxCidadeCodIbge(dto.getCidade()));
		pj.setIdNatJuridica(naturezaJuridicaRepository.findByNuNatJuridicaCod(dto.getNaturezaJuridica()).get());		
		if(StringUtils.isEmpty(dto.getRepresentanteLegalCnpj())) {			
			//FIXME Verificar com o Simeão essa situação
		}
		if(!StringUtils.isEmpty(dto.getRepresentanteLegalCpf())) {
			Optional<Pessoa> pjExist = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getRepresentanteLegalCpf()));
			if(pjExist.isPresent()) {
				pj.setIdRepresentanteLegal((PessoaFisica) pjExist.get());
			}
		}
		
		PessoaJuridica pessJur = pessoaRepository.save(pj);
		return new ResponseEntity<Object>(new PessoaJuridicaDTO(pessJur), HttpStatus.OK);
	}
	
}
