package br.com.isaneto.cartorioonline.api.repository;

import br.com.isaneto.cartorioonline.api.modelo.PessoaFisica;
import br.com.isaneto.cartorioonline.api.modelo.PessoaJuridica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaFisicaRepository extends JpaRepository<PessoaFisica, Long> {

}
