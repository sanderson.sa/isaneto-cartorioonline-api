package br.com.isaneto.cartorioonline.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.SituacaoContribuinte;

@Repository
public interface SituacaoContribuinteRepository extends JpaRepository<SituacaoContribuinte, Integer>{
	
}
