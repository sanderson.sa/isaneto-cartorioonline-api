/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.isaneto.cartorioonline.api.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author Bruno
 */
@Entity
@Table(name = "cep", schema = "public")  		            		        
public class Cep implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5725095098246327581L;
    @Id 
    @SequenceGenerator(name = "cep_id_cep_seq", sequenceName = "cep_id_cep_seq", schema = "public", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cep_id_cep_seq")
    @Column(name = "id_cep", nullable = false)
    private Long idCep;
    @Size(min = 8, max = 8)
    @Column(name = "cep", length = 8)
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro", length = 20)
    private String noTipoLogradouro;
    @Size(max = 100)
    @Column(name = "no_logradouro", length = 100)
    private String logradouro;
    @Size(max = 100)
    @Column(name = "no_bairro", length = 100)
    private String bairro;
    @Size(max = 100)
    @Column(name = "no_cidade", length = 100)
    private String noCidade;
    @Size(max = 2)
    @Column(name = "tx_uf", length = 2)
    private String uf;    
    @ManyToOne
	@JoinColumn(name="id_estado",referencedColumnName="id_estado")
    private Estado estado;
    @ManyToOne
	@JoinColumn(name="id_cidade",referencedColumnName="id_cidade")
    private Cidade cidade;
    @ManyToOne
	@JoinColumn(name="id_tp_logradouro",referencedColumnName="id_tp_logradouro")
    private TipoLogradouro tipoLogradouro;
    @Column(name="dt_cep_ult_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
	private Date dataUltimaAtualizacao;
    @Transient
    private BigInteger nuCep;
	
	public Long getIdCep() {
		return idCep;
	}
	public void setIdCep(Long idCep) {
		this.idCep = idCep;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}
	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}
	public void setNuCep(BigInteger nuCep) {
		this.nuCep = nuCep;
	}
	public BigInteger getNuCep() {
		if(cep != null && !cep.equals("")){
			return new BigInteger(cep);
		}
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCep == null) ? 0 : idCep.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cep other = (Cep) obj;
		if (idCep == null) {
			if (other.idCep != null)
				return false;
		} else if (!idCep.equals(other.idCep))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Cep [idCep=" + idCep + ", cep=" + cep + ", nuCep=" + nuCep
				+ ", tipoLogradouro=" + tipoLogradouro + ", logradouro="
				+ logradouro + ", bairro=" + bairro + ", noCidade=" + noCidade
				+ ", uf=" + uf + ", estado=" + estado + ", cidade="
				+ cidade + ", tipoLogradouro=" + tipoLogradouro
				+ ", dataUltimaAtualizacao=" + dataUltimaAtualizacao + "]";
	}
	public String getNoTipoLogradouro() {
		return noTipoLogradouro;
	}
	public void setNoTipoLogradouro(String noTipoLogradouro) {
		this.noTipoLogradouro = noTipoLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNoCidade() {
		return noCidade;
	}
	public void setNoCidade(String noCidade) {
		this.noCidade = noCidade;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}
	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
    
}
