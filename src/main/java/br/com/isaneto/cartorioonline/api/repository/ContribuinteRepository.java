package br.com.isaneto.cartorioonline.api.repository;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Contribuinte;
import br.com.isaneto.cartorioonline.api.modelo.Pessoa;


@Repository
public interface ContribuinteRepository extends JpaRepository<Contribuinte, Long> {
	Optional<Contribuinte> findByPessoa(Pessoa pessoa);
	
	@Query("SELECT c FROM Contribuinte c JOIN c.pessoa p WHERE c.inscricaoMunicipalSemMascara = :inscricao AND p.nuPessoaCpfCnpj = :cpfCnpj")
	Optional<Contribuinte> findByPessoaAndInsc(@Param("inscricao") String insc, @Param("cpfCnpj") BigInteger cpfCnpj);
	
}

