package br.com.isaneto.cartorioonline.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.isaneto.cartorioonline.api.modelo.Atividade;

@Repository
public interface AtividadeRepository extends JpaRepository<Atividade, Long> {
	Optional<Atividade> findByTxAtividadeCnaeSm(String codigo);
}
