package br.com.isaneto.cartorioonline.api.controller;

import java.math.BigInteger;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import br.com.isaneto.cartorioonline.api.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.isaneto.cartorioonline.api.modelo.MessagemRetorno;
import br.com.isaneto.cartorioonline.api.modelo.Pessoa;
import br.com.isaneto.cartorioonline.api.modelo.PessoaFisica;
import br.com.isaneto.cartorioonline.api.modelo.dto.PessoaFisicaDto;
import br.com.isaneto.cartorioonline.api.repository.CidadeRepository;
import br.com.isaneto.cartorioonline.api.repository.EstadoRepository;
import br.com.isaneto.cartorioonline.api.repository.PessoaRepository;
import br.com.isaneto.cartorioonline.api.repository.TipoIdentificacaoRepository;
import br.com.isaneto.cartorioonline.api.service.ContribuinteService;
import br.com.isaneto.cartorioonline.api.util.Util;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("pessoa-fisica")
public class PessoaFisicaController {

	
	@Autowired
	private ContribuinteService contribuinteService;
	
	@Autowired
	private TipoIdentificacaoRepository tipoIdentificacaoRepository;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@GetMapping("/{cpf}")
	@ApiOperation(value = "Recuperar Pessoa Fisíca por CPF", notes = "Recuperar Pessoa Fisíca por CPF")
	public ResponseEntity<Object> getPessoa(
			@PathVariable("cpf") String cpf) {
		Optional<Pessoa> pessoa = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(cpf));
		if(pessoa.isPresent()) {
			PessoaFisicaDto dto = new PessoaFisicaDto(pessoa.get());
			return new ResponseEntity<Object>(dto, HttpStatus.OK);
		}else {
			throw new EntityNotFoundException(String.format("Pessoa não localizada CPF :: %s ", cpf));
		}		
	}
	
	@PostMapping("")
	public ResponseEntity<?> postPessoa(
			@Valid @RequestBody PessoaFisicaDto dto,
			BindingResult result) {

		if(result.hasErrors())
			contribuinteService.getErrosValidacaoRequest(result);
		
		//Verificar ser existe
		Optional<Pessoa> pessoa = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getCpf()));
		if(pessoa.isPresent()) {
			throw new InvalidDataAccessApiUsageException(String.format("Já existe um registro para o CPF :: %s", dto.getCpf()));
		}else {
			PessoaFisica pf = new PessoaFisica(dto);
			pf.setTipoIdentificacaoDoc(tipoIdentificacaoRepository.findById(new Short(dto.getTipoIdentificacaoDoc())).get());
			pf.setEstadoDoc(estadoRepository.findByTxEstadoSigla(dto.getUfDoc()));
			pf.setEstado(estadoRepository.findByTxEstadoSigla(dto.getUf()));
			pf.setCidade(cidadeRepository.findByTxCidadeCodIbge(dto.getCidade()));
			
			PessoaFisica pessoaFisica = pessoaRepository.save(pf);
			
			return new ResponseEntity<Object>(new PessoaFisicaDto(pessoaFisica), HttpStatus.CREATED);
		}
	}
	
	@PutMapping("")
	public ResponseEntity<?> putPessoa(
			@Valid @RequestBody PessoaFisicaDto dto,
			BindingResult result) {

		if(result.hasErrors())
			contribuinteService.getErrosValidacaoRequest(result);
		
		//Verificar ser existe
		Optional<Pessoa> pessoa = pessoaRepository.findByNuPessoaCpfCnpj(new BigInteger(dto.getCpf()));
		if(!pessoa.isPresent()) {
			return new ResponseEntity<Object>("Registro não localizado", HttpStatus.NOT_FOUND);
		}else {
			PessoaFisica pf = (PessoaFisica) pessoa.get();
			
			pf.setTipoIdentificacaoDoc(tipoIdentificacaoRepository.findById(new Short(dto.getTipoIdentificacaoDoc())).get());
			pf.setEstadoDoc(estadoRepository.findByTxEstadoSigla(dto.getUfDoc()));
			pf.setEstado(estadoRepository.findByTxEstadoSigla(dto.getUf()));
			pf.setCidade(cidadeRepository.findByTxCidadeCodIbge(dto.getCidade()));
			pf.setCpf(dto.getCpf());
			pf.setNuPessoaCpfCnpj(new BigInteger(dto.getCpf()));
			pf.setNoPessoa(dto.getNome());
			pf.setDtPessoaFisNasc(Util.strToDateConvert(dto.getDataNascimento()));
			pf.setTxPessoaFisSexo(dto.getSexo());
			pf.setTxPessoaFisDoc(dto.getNumeroDocumento());
			pf.setDtPessoaFisExpeDoc(Util.strToDateConvert(dto.getDataExpedicao()));
			pf.setTxPessoaFisOrgExpDoc(dto.getOrgaoExpeditor());
			pf.setNuPessoaEndCep(new BigInteger(dto.getCep()));
			pf.setTxPessoaEndeLogradouro(dto.getEnderecoLogradouro());
			pf.setTxPessoaEndNumero(dto.getEnderecoNumero());
			pf.setTxPessoaEndeComplemento(dto.getEnderecoComplemento());
			pf.setTxPessoaEndBairro(dto.getEnderecoBairro());  
			PessoaFisica pessoaFisica = pessoaRepository.save(pf);
			
			return new ResponseEntity<Object>(new PessoaFisicaDto(pessoaFisica), HttpStatus.OK);
		}
	}
	
}
